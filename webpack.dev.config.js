var webpack = require('webpack');
var config = require('./server.config')
 
module.exports = {
 
    entry: [
        './browser/index.js',
        'webpack-dev-server/client?http://0.0.0.0:' + config.server.devPort,
        'webpack/hot/only-dev-server'
    ],
 
    output: {
        path: '/',
        filename: 'bundle.js'
    },
 
    devServer: {
        compress: true,
        disableHostCheck: true,
        hot: true,
        filename: 'bundle.js',
        publicPath: '/',
        historyApiFallback: true,
        contentBase: './public',
        proxy: {
            "*": "http://[::1]:" + config.server.port
        }
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            Promise: "bluebird"
        }),
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,  
                loaders: ['react-hot-loader', 'babel-loader'],
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
            },
        ]   
    }
};