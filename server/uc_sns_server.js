'use strict';

import express from 'express';
import path from 'path';
import config from '../server.config';
import session from 'express-session';
import connect_redis from 'connect-redis';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import db from './api/db/db';
import { Admin, Account, User, Article, Comment, Team } from './api/v1';
import localPassport from './middleware/localPassport';
import auth from './middleware/auth';
import http from 'http';
import wst from './wst';

let RedisStore = connect_redis(session);
const app = express();
const httpServer = http.createServer(app);
const port = config.server.port;
const devPort = config.server.devPort;

/**
 * database
 */
db.connect(config.dbType,
            config[config.dbType].url,
            config[config.dbType].name,
            config[config.dbType].poolsize);
localPassport.setup();

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(cookieParser(config.auth.secret));
app.use(session({
  secret: config.auth.secret,
  resave: false,
  saveUninitialized: true,
  store : new RedisStore(
      {url : config.redisdb.url})
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, '../public')));

/**
 * CORS
 */
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
   res.setHeader('Access-Control-Allow-Headers', true);
  next();
});

app.use('/api/v1/admin', Admin);
app.use('/api/v1/account', Account);
app.use('/api/v1/article', [auth.isAuthenticated], Article);
app.use('/api/v1/user', [auth.isAuthenticated], User);
app.use('/api/v1/comment', [auth.isAuthenticated], Comment);
app.use('/api/v1/team', [auth.isAuthenticated], Team);


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'));
    
});

app.use(function(err, req, res, next) {
    let json = {
        status: 'fail',
        desc: err.message.toString()
    };
    wst.e(req, err);
    res.json(json);
});

wst.i('#WINSTON TEST# webpack-dev-server is listening');
if (process.env.NODE_ENV === 'development') {

    const webpack = require('webpack');
    const WebpackDevServer = require('webpack-dev-server');

    httpServer.listen(port, ()=> {
        console.log('http is listening on port', port);
    })

    console.log('Server is running on development mode');
    const config = require('../webpack.dev.config');
    const compiler = webpack(config);
    const devServer = new WebpackDevServer(compiler, config.devServer);
    devServer.listen(
        devPort, () => {
            console.log('## webpack-dev-server is listening on port', devPort);
        }
    );
} else if (process.env.NODE_ENV === 'production') {
    httpServer.listen(port, ()=> {
        console.log('[UC-SNS]server is listening on port', port);
    });
}
