import winston from 'winston';
import winstonDaily from 'winston-daily-rotate-file';
import moment from 'moment';
import path from 'path';
import fs from 'fs';
import pe from 'pretty-error';

const wst = {};

let logPath = path.join(__dirname, '/log');
!fs.existsSync(logPath) && fs.mkdirSync(logPath);

function timeStampFormat() {
    return moment().format('YYYY-MM-DD HH:mm:ss.SSS ZZ');
}

wst.logger = new (winston.Logger)({
    transports: [
        new (winstonDaily)(
            {
                name: 'info-file',
                filename: (path.join(__dirname, '/log/')) + 'server',
                datePattern: '_yyyy_MM_dd.log',
                colorize: false,
                maxSize: 5000000,
                maxFiles: 1000,
                level: 'error',
                showLevel: true,
                json: false,
                timestamp: timeStampFormat
            }),
        new (winston.transports.Console)({
            name: 'debug-console',
            colorize: true,
            level: 'debug',
            showLevel: true,
            json: false,
            timestamp: timeStampFormat
        })
    ],
    exceptionHandlers: [
        new (winstonDaily)({
            name: 'exception-file',
            filename: (path.join(__dirname, '/log/')) + 'exception',
            datePattern: '_yyyy_MM_dd.log',
            colorize: false,
            maxSize: 5000000,
            maxFiles: 1000,
            level: 'info',
            showLevel: false,
            json: true,
            timestamp: timeStampFormat
        }),
        new (winston.transports.Console)({
            name: 'exception-console',
            colorize: true,
            level: 'info',
            showLevel: false,
            json: true,
            timestamp: timeStampFormat
        })
    ]
});

wst.e = function(req, log) {
    if (req) {
        let userinfo = '';
        if (req.session && req.session.signinInfo) {
            userinfo = `[USER]:${req.session.signinInfo.user.email}(${req.session.signinInfo.user.username})\n`;
        }
        let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        let useragent = req.headers['user-agent'];
        let url = req.originalUrl;

        this.logger.error(`[URL]:${url}, ${userinfo}[IP]:${ip}, [AGENT]:${useragent}\n${log}`);
    } 
}

wst.i = function(log) {
    this.logger.info(log);
}

wst.stackPretty = function(e) {
    return  pe.render(e);
}

export default wst
