'use strict';

export class JsonResponse {
    constructor(status = 'success', desc = null, data = null) {
        this.status = status;
        this.desc = desc;
        this.data = data;
    }
}
