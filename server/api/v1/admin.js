'use strict';

import express from 'express';
import { JsonResponse } from '../api';
import db from '../db/db';
import orgMigration from '../db/orgMigration';
import wst from '../../wst';

let Admin = express.Router();


Admin.post('/syncorgusers', (req, res, next) => {

    let json = new JsonResponse();

    let q = `
        select 
        user_id,
        user_name,
        user_paycl_name,
        user_group_code,
        user_group_name,
        user_tel_company,
        user_tel_mobile,
        user_email,
        user_field2,
        user_field3,
        user_field5,
        '5B39C8B553C821E7CDDC6DA64B5BD2EE' as user_default_rule
        from tbl_users
    `;

    orgMigration.query(q, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        } 
        db.database().userMigrations(doc, (result, err) => {
            if (err) {
                next(err, req, res);
                return;
            } 
            json.data = result;
            res.json(json);
        });
    });
});


Admin.post('/syncorggroup', (req, res, next) => {

    let json = new JsonResponse();

    let q = `
        select 
        org_group_code,
        org_group_name,
        org_caption
        from tbl_org_group
    `;
    orgMigration.query(q, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        } 
        json.data = doc;
        res.json(json);
    });
});

Admin.post('/syncorgclass', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let q = `
        SELECT
        class_id,
        class_kind,
        class_upper_class_id,
        class_org_group_code,
        class_group_code, 
        class_group_name, 
        class_user_id,
        user_writetime
        FROM TBL_CLASS
        WHERE CLASS_KIND = '2' AND CLASS_ORG_GROUP_CODE = 'ORG0000002'
        ORDER BY CLASS_ORDER_NO
    `;
    
    orgMigration.query(q, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        } 
        db.database().orgMigrations(doc, (result, err) => {
            if (err) {
                next(err, req, res);
                return;
            } 
            json.data = result;
            res.json(json);
        });
    });
});


export default Admin