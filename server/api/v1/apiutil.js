'use strict';

import wst from '../../wst';
import db from '../db/db';
import multer from 'multer';
import Q from 'q';
import fs from 'fs';
import mkdirp from 'mkdirp';
import gm from 'gm'; //graphicsmagick 설치해야함.
import videoThumb from 'video-thumb'; //ffmpeg 설치해야함. (readme. 문서 참조)


const uc = {};
uc.apiutil = {};


uc.apiutil.saveAttachments = function(attList, articleid, teamid, isComment, cb) {

    if (!attList || attList.length <= 0) {
        cb && cb(null);
        return;
    }

    let savedAttList = [];

    const createPromise = (att, arr) => {
        return new Promise((rs, rj) => {
            db.database().saveAttachment(att, (result, err) => {

                if (err) {
                    rj();
                    next(err, req, res);
                    return;
                }
                arr.push(result._id);
                rs();
            });
        });
    };

    let promiseArr = [];
    attList.forEach((v, i) => {
        !isComment ? v['article'] = articleid : v['comment'] = articleid;
        v['team'] = teamid;

        promiseArr.push(createPromise(v, savedAttList))
    });

    Promise.all(promiseArr).then(() => {
        if (cb) {
            cb(savedAttList);
        }
    });
} 

uc.apiutil.fileupload = (req, res) => {

    let fileObjects = [];
    let deferred = Q.defer();
    let savePath = 'public/uploads/'
    let destDir = savePath;
    let filePrefix = 'ETC';
    let type = 'file';

    let storage = multer.diskStorage({
        destination: (req, file, cb) => {

            savePath = 'public/uploads/'
            destDir = savePath;

            if ((/image/).test(file.mimetype)) {
                destDir = savePath += "images";
                filePrefix = 'IMG';
                type = 'image';

            } else if ((/video/).test(file.mimetype)) {
                destDir = savePath += "videos";
                filePrefix = 'MOV';
                type = 'video';

            } else {
                destDir = savePath += "files";
                filePrefix = 'ETC';
                type = 'file';
            }

            let author = JSON.parse(req.body.author);
            let email = author.email.replace(/[@.]/gi, '_');
            destDir = `${savePath}/${email}`;
            if (fs.existsSync(destDir)) {
                cb(null, destDir);
            } else {
                mkdirp(destDir, (err) => {
                    if (err) {
                        wst.e(req, err);
                        cb(null, savePath);
                    } else {
                        cb(null, destDir);
                    }
                });
            }
        },

        filename: (req, file, cb) => {
            //공백 치환
            let filename = file.originalname.replace(/\s/gi, '_');
            //.제외 특수문제 치환
            filename = filename.replace(/[\{\}\[\]\/?,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi, '');
            filename = `${filePrefix}_${Date.now()}_${filename}`;

            let author = JSON.parse(req.body.author);
            let fileObj = {
                filename: filename,
                path: destDir,
                originalname: file.originalname,
                mime: file.mimetype,
                user: author,
                type: type
            };
            fileObjects.push(fileObj);
            cb(null, filename);
        }
    });

    //let upload = multer({storage: storage, limits: { fileSize: 20 * (1024 * 1024)}}).array('files', 5);
    let upload = multer({storage: storage}).array('files');
    upload(req, res,  (err) => {
        if (err) {
            wst.e(req, err);
            deferred.reject(err);
        } else  {
            const getFileSize = (path) => {
                let stats = fs.statSync(path);
                let fileSizeInBytes = stats.size;
                return fileSizeInBytes;
            };

            fileObjects.forEach((v, i) => {
                let filepath = `${v.path}/${v.filename}`;
                v.size = getFileSize(filepath);
            });
            deferred.resolve(fileObjects);
        }
    });
    return deferred.promise;
};

uc.apiutil.createThumbnail = (fileObjects, cb) => {

	const THUMB_SIZE = 240;

    let promiseList = [];

    fileObjects.forEach( (v, i) => {

        let originalPath = `${v.path}/${v.filename}`;
        let filepath = originalPath;

        let isrename = false;
        if ((/image/).test(v.mime)) {

            let renamedFilename = encodeURI(v.filename);

            let comp = v.filename.split('.');
            let ext = comp[comp.length - 1];

            let fileRenamePath = v.path + '/tmp_' + Date.now().toString() + ext;
            let thumbfilename = v.filename.replace(new RegExp('.' + ext + '\$'), '_thumb.' + ext);

            //한글이 섞여있을경우 centos gm에서 에러발생.
            //gm 호출전 파일이름 변경 후 완료되면 다시 원복.
            if (v.filename.length !== renamedFilename.length) {

                fs.renameSync(originalPath, fileRenamePath);

                filepath = fileRenamePath;
                thumbfilename = 'tmp_' + Date.now().toString() + '_thumb' + ext;

                isrename = true;
            }

            let destFilePath = `${v.path}/${thumbfilename}`;
            let p = new Promise((rs, rj) => {
                gm(filepath)
                    .noProfile()
                    .resize(THUMB_SIZE + '', THUMB_SIZE + '', '^')
                    .gravity('Center')
                    .crop(THUMB_SIZE, THUMB_SIZE)
                    .quality(80)
                    .write(destFilePath, (e) => {
                        if (e) {
                            wst.e(null, e);
                            rj();
                        } else {
                            if (isrename) {

                                //원본이름 변경
                                fs.renameSync(filepath, originalPath);

                                //섬네일 이름 변경
                                thumbfilename = v.filename.replace(new RegExp('.' + ext + '\$'), '_thumb.' + ext);
                                let originalFileThumbPath = `${v.path}/${thumbfilename}`;

                                fs.renameSync(destFilePath, originalFileThumbPath);
                            }
                            v['thumb'] = thumbfilename;
                            rs();
                        }
                    });
            });
            promiseList.push(p);

        } else if ((/video/).test(v.mime)) {

            let comp = v.filename.split('.');
            let ext = comp[comp.length - 1];
            let thumbfilename = v.filename.replace(new RegExp('.' + ext + '\$'), '_thumb.png');
            let dest = `${v.path}/${thumbfilename}`;

            let p = new Promise((rs, rj) => {
                videoThumb.extract(originalPath, dest, '00:00:02', `${THUMB_SIZE}x${THUMB_SIZE}`, function() {
                    v['thumb'] = thumbfilename;
                    rs();
                });
            });
            promiseList.push(p);
        }
    });

    if (promiseList.length > 0) {
        Promise.all(promiseList).then(()=>{
            cb && cb();
        });
    } else {
        cb && cb();
    }
};

export default uc;