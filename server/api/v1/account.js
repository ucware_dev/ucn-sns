'use strict';

import express from 'express';
let Account = express.Router();
import { JsonResponse } from '../api';
import passport from 'passport'; 
import auth from '../../middleware/auth';
import db from '../db/db';
import wst from '../../wst';


//SIGN UP
Account.post('/signup', (req, res, next) => {

    let json = new JsonResponse();
    let email = req.body.email;

    db.database().findUser({ email: email }, (doc, err) => {
        if (doc) {
            json.status = 'fail';
            json.desc = 'already exist user';
            wst.e(req, JSON.stringify(json));
            res.json(json);
            return;
        }
        next();
    });
});

Account.post('/signup', function (req, res, next) {

    var newuser = {
        email: req.body.email,
        passwd: req.body.passwd,           //암호화 추가되어야함
        username: req.body.username,
        profileImage: {
            path:'public/images',
            filanem: 'user_default.png',
            thumb: 'user_default.png'
        }
    };

    let json = new JsonResponse();

    db.database().saveUser(newuser, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        } 
        json.data = doc;
        res.json(json);
    });
});


//SIGN IN
Account.post('/signin', (req, res, next) => {

    let json = new JsonResponse();
    passport.authenticate('local',(err, user, info) => {

        if (!user) {
            json.status = 'fail';
            json.desc = 'invalid username or password';
            res.json(json);
            return;
        }

        auth.signToken(user.email, (err, token) => {
            if (err) {
                next(err, req, res);
                return;
            }
            json.data = {
                user: user,
                token: token
            };
            req.session.signinInfo = json.data;
            res.json(json);
        });

    })(req, res, next);
});

Account.post('/signout', (req, res, next) => {

    if (req.body.email) {
        auth.revokeToken(req.body.email);
    }
    req.session.destroy();

    let json = new JsonResponse();
    res.json(json);
});

Account.post('/session', (req, res, next) => {
    if (typeof req.session.signinInfo === 'undefined') {
        return res.json({session: null});
    }
    res.json({session: req.session.signinInfo});
});

export default Account
