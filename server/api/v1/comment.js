'use strict';

import express from 'express';
let Comment = express.Router();
import { JsonResponse } from '../api';
import db from '../db/db';
import wst from '../../wst';
import uc from './apiutil';

Comment.post('/post', (req, res, next) => {

    let json = new JsonResponse("fail", "", null);
    
    if (!req.body.articleid) {
        json.desc = 'articleid param empty';
        res.json(json);
        return;
    }    

    if (!req.body.authorid) {
        json.desc = 'author param empty';
        res.json(json);
        return;
    }

    if (!req.body.content && !req.body.imgInfo) {
        json.desc = 'content or img param empty';
        res.json(json);
        return;
    }
    next();
});

Comment.post('/post', (req, res, next) => {
    
    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let article = req.body.articleid;
    let author = req.body.authorid;
    let content = req.body.content;
    
    let imgInfo = JSON.parse(req.body.imgInfo);

    let comment = {
        article: article,
        author: author,
        content: content,
    };

    db.database().saveComment(comment, (doc, err) => {
        
        if (err) {
            next(err, req, res);
            return;
        }

        if (!imgInfo) {
            json.data = doc;
            res.json(json);
            return;
        }

        let commentCount = doc.commentCount;
        let cmmtId = doc._id.toString();
        uc.apiutil.saveAttachments([imgInfo], cmmtId, teamid, true, (attList) => {

            let saveImgInfo = attList[0];
            db.database().updateComment(cmmtId, content, saveImgInfo, (doc, err) => {
                if (err) {
                    next(err, req, res);
                    return;
                }

                let copyDoc = JSON.parse(JSON.stringify(doc));
                copyDoc.commentCount = totalCommentCount;

                json.data = copyDoc;
                res.json(json);
            });
        });
    });
});

Comment.post('/update', (req, res, next) => {

    // let json = new JsonResponse("success", "", null);
    
    // let commentId = req.body.commentId;
    // let content = req.body.content;
    // let imgInfo = JSON.parse(req.body.imgInfo);

    // if (!commentId) {
    //     json.status = "fail";
    //     json.desc = "Comment Id is null";
    //     res.json(json);
    //     return;
    // }

    // if (!content && !imgInfo) {
    //     json.status = "fail";
    //     json.desc = "content, file are null";
    //     res.json(json);
    //     return;
    // }

    // db.database().updateComment(commentId, content, imgInfo, (doc, err) => {
    //     if (err) {
    //         next(err, req, res);
    //         return;
    //     }
    //     json.data = doc;
    //     res.json(json);
    // });


    let json = new JsonResponse("success", "", null);
    
    let commentid = req.body.commentid;
    let teamid = req.body.teamid;
    let content = req.body.content;
    let imgInfo = JSON.parse(req.body.imgInfo);

    if (!commentid) {
        json.status = "fail";
        json.desc = "Comment Id is null";
        res.json(json);
        return;
    }

    if (!content && !imgInfo) {
        json.status = "fail";
        json.desc = "content, file are null";
        res.json(json);
        return;
    }

    let isDeleteImg = !imgInfo ? true : false;
    let isUpdateImg = (imgInfo && !imgInfo._id) ? true : false;
    let saveImgInfo = isUpdateImg ? imgInfo : null;

    console.log(isDeleteImg, isUpdateImg);

    db.database().findComment(commentid, (doc, err) => {

        if (err) {
            next(err, req, res);
            return;
        }

        let delCommentId = null;
        if (isDeleteImg) {
            delCommentId = doc._id.toString();
        }

        db.database().removeAttachmentsByCommentId(delCommentId, (doc, err) => {

            if (err) {
                next(err, req, res);
                return;
            }

            uc.apiutil.saveAttachments( saveImgInfo ? [saveImgInfo] : null, commentid, teamid, true, (attList) => {

                let saveImgInfo = (attList && attList.length > 0) ? attList[0] : null;
                if (!isDeleteImg && !isUpdateImg) {
                    saveImgInfo = imgInfo._id;
                }
                db.database().updateComment(commentid, content, saveImgInfo, (doc, err) => {
                    if (err) {
                        next(err, req, res);
                        return;
                    }
                    json.data = doc;
                    res.json(json);
                }); 
            });
        });
    });
});

Comment.post('/delete', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let commentid = req.body.commentid;

    if (!articleid || !commentid ){
        json.desc = 'invalid params';
        res.json(json);
        return;
    }

    db.database().deleteComment(articleid, commentid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});


Comment.post('/list', (req, res, next) => {

    let json = new JsonResponse("success", "", null);
    
    let articleid = req.body.articleid;    
    let limit = parseInt(req.body.limit);
    if (!limit) {
        limit = 2;
    }

    if (!articleid) {
        json.desc = 'invalid params';
        res.json(json);
        return;
    }

    db.database().fetchComments(articleid, limit, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Comment.post('/newlist', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let commentid = req.body.commentid;

    db.database().fetchNewComments(articleid, commentid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });    
});

Comment.post('/oldlist', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let commentid = req.body.commentid;
    let count = 10;
    try {
        count = parseInt(req.body.count);
    } catch(e) {
        count = 10;
    }

    db.database().fetchOldComments(articleid, commentid, count, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

export default Comment