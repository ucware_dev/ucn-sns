'use strict';

import express from 'express';
import { JsonResponse } from '../api';
import db from '../db/db';
let User = express.Router();

import wst from '../../wst';

User.post('/addteam', (req, res, next) => {

    let json = new JsonResponse("success", "", null);
    
    if (!req.body.userid || !req.body.teamid) {
        json.status = "fail";
        json.desc = "invalid params";
        req.json(json);
        return;
    }

    db.database().addTeamToUser(req.body.userid, req.body.teamid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }        
        json.data = doc;
        res.json(json);
    });
});

User.post('/userinfo', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let userid = req.body.userid;
    if (!userid) {
        json.status = "fail";
        json.desc = "invalid params";
        res.json(json);
        return;
    }

    db.database().userInfo(userid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }        
        json.data = doc;
        res.json(json);
    });
});

User.post('/profileimage', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let userid = req.body.userid;
    let path = req.body.path;
    let filename = req.body.filename;
    let thumb = req.body.thumb;

    if (!userid) {
        json.status = "fail";
        json.desc = "invalid params";
        res.json(json);
        return;
    }

    let params = {
        userid: userid,
        path: path,
        filename: filename,
        thumb: thumb
    };

    db.database().userChangeProfileImage(params, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }        
        res.json(json);
    });
});

User.post('/update', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let userid = req.body.userid;
    let username = req.body.username;
 
    if (!userid || !username) {
        json.status = "fail";
        json.desc = "invalid params";
        res.json(json);
        return;
    }

    let params = {
        userid: userid,
        username: username
    };

    db.database().updateUserInfo(params, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }        
        res.json(json);
    });
});

export default User