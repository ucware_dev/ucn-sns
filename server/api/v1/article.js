'use strict';

import express from 'express';
let Article = express.Router();
import { JsonResponse } from '../api';
import db from '../db/db';
import ogs from 'open-graph-scraper';
import wst from '../../wst';
import uc from './apiutil';


Article.post('/fileupload',  (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    uc.apiutil.fileupload(req, res).then( (fileObjects) => {
        uc.apiutil.createThumbnail(fileObjects, ()=> {
            json.data = fileObjects;
            res.json(json);
        });
    }, (e)=> {
        next(e, req, res);
    });
});

Article.post('/post',  (req, res, next) => {
    
    let json = new JsonResponse("success", "", null);

    let content = req.body.content;
    let attachfiles = req.body.files;
    let ogs = req.body.ogs;
    let author = JSON.parse(req.body.author);
    let teamid = req.body.teamid;
    let vote = JSON.parse(req.body.vote);

    if (!author) {
        res.json(json);
        return;
    }

    if (content.length <= 0 && (!attachfiles || attachfiles.length <= 0)) {
        let json = new JsonResponse("fail", "empty content or files", null);
        res.json(json);
        return;
    }

    let newArticle = {
        teamid: teamid,
        author: author,
        content: content,
        ogs: ogs,
        vote: vote
    };

    db.database().saveArticle(newArticle, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }

        let articleid = doc._id.toString();
        uc.apiutil.saveAttachments(attachfiles, articleid, doc.team.toString(), false, (attList) => {
            doc.files = attList && attList;
            db.database().updateArticle(articleid, doc, (doc, err) => {
                if (err) {
                    next(err, req, res);
                    return;
                }
                res.json(json);
            });
        });
    });
});

Article.post('/update',  (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let teamid = req.body.teamid;
    let content = req.body.content;
    let attachfiles = req.body.files;
    let vote = JSON.parse(req.body.vote);

    let willSaveAtts = [];
    let didSaveAtts = [];
    attachfiles && attachfiles.forEach((v, i) => {
        if (!v._id) {
            willSaveAtts.push(v);
        } else {
            didSaveAtts.push(v);
        }
    })

    let willDelAtts = []
    db.database().findArticle(articleid, (doc, err) => {

        if (err) {
            next(err, req, res);
            return;
        }

        doc.files && doc.files.forEach((v, i) => {

            let found = false;
            didSaveAtts.forEach((vv, i) => {
                if (v._id.toString() === vv._id.toString()) {
                    found = true;
                }
            });

            if (found === false) {
                willDelAtts.push(v._id);
            }
        });

        //제거된 파일 삭제.
        db.database().removeAttachmentsByIds(willDelAtts, () => {

            //새로 첨부된 파일 첨부.
            uc.apiutil.saveAttachments(willSaveAtts, articleid, teamid, false, (attList) => {

                let mergeAttList = didSaveAtts;
                attList && attList.forEach((v, i) => {
                    mergeAttList.push(v);
                });

                let modifyArticle = {
                    content: content,
                    files: mergeAttList,
                    vote: vote
                };   

                db.database().updateArticle(articleid, modifyArticle, (doc, err) => {
                    if (err) {
                        next(err, req, res);
                        return;
                    }
                    json.data = doc;
                    res.json(json);
                });
            });
        });
    });
});

Article.post('/updateEmoji',  (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let objID = req.body.id;
    if (!objID) {
        json.status = "fail";
        json.desc = "Invalid article id";
        res.json(json);
        return;
    }

    let user = JSON.parse(req.body.user);
    if (!user) {
        json.status = "fail";
        json.desc = "Invalid user";
        res.json(json);
        return;
    }

    let emojiCode = req.body.emojiCode;
    if (!emojiCode) {
        json.status = "fail";
        json.desc = "Invalid emojiCode";
        res.json(json);
        return;
    }

    db.database().updateEmojiArticle(objID, user, emojiCode, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Article.post('/list', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let count = parseInt(req.body.count) ;
    let teamid = req.body.teamid;

    db.database().fetchArticle(count, teamid, (list, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = list;
        res.json(json);
    });
});



Article.post('/newlist', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let teamid = req.body.teamid;

    db.database().fetchNewArticle( articleid, teamid, (list, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = list;
        if (list.length === 0 ) {
            json.status = 'fail';
            json.desc = 'no result';
        }
        res.json(json);
    });
});

Article.post('/oldlist', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    let teamid = req.body.teamid;
    let count = 10;
    try {
        count = parseInt(req.body.count);
    } catch(e) {
        count = 10;
    }

    db.database().fetchOldArticle( articleid, teamid, count, (list, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = list;
        if (list.length === 0 ) {
            json.status = 'fail';
            json.desc = 'no result';
        }
        res.json(json);
    });
});

Article.post('/delete', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;

    if(!articleid) {
        json.status = 'fail';
        json.desc = 'invalid id';
        res.json(json);
        return;
    }
    
    db.database().removeArticle(articleid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = {'deleteId': articleid};
        res.json(json);
    });
});

Article.post('/find', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;

    if (!articleid ) {
        json.status = 'fail';
        json.desc = 'invalid article id';
        res.json(json);
        return;
    }
    db.database().findArticle( articleid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Article.post('/like', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleId = req.body.articleId;
    let userInfo = JSON.parse(req.body.userInfo);

    if (!articleId || !userInfo) {
        json.status = 'fail';
        json.desc = 'invalid id';
        res.json(json);
        return;
    }

    db.database().updateLike( articleId, userInfo, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        res.json(json);
    });
});

Article.post('/meta', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let url = req.body.url;

    if (!url) {
        json.status = 'fail';
        json.desc = 'invalid url';
        res.json(json);
        return;
    }

    let options = {'url': url};
    ogs(options, (e, r) => {
        if (e) {
            next(e, req, res);
            return;
        }
        r.status = 'success';
        json.data = r;
        res.json(r);
    })
});

Article.post('/search', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let keyword = req.body.keyword;


    if (!teamid && !keyword) {
        json.status = 'fail';
        json.desc = 'invalid url';
        res.json(json);
        return;
    }
    db.database().articleSearch(teamid, keyword, function (doc, err) {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});


Article.post('/votefinish', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let articleid = req.body.articleid;
    
    if (!articleid) {
        json.status = 'fail';
        json.desc = 'invalid param';
        res.json(json);
        return;
    }
    
    db.database().finishVote(articleid, function (doc, err) {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});


export default Article
