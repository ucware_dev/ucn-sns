'use strict';

import express from 'express';
import { JsonResponse } from '../api';
import db from '../db/db';
let Team = express.Router();

import wst from '../../wst';

//Create Team
Team.post('/create', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamInfo = JSON.parse(req.body.teamInfo);
    let operatorid = req.body.operatorid;

    if ((!teamInfo || !teamInfo.name) || !operatorid) {
        json = new JsonResponse("fail", "Invalid params.", null);
        json.data = teamInfo;
        res.json(json);
        return;
    }

    db.database().createTeam(teamInfo, operatorid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/list', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let count = req.body.count;

    if (!teamid) {
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().fetchTeam(teamid, count, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/join', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let userid = req.body.userid;

    if (!teamid || !userid) {
        json.status = "fail";
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().joinTeam(teamid, userid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/find', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamids = JSON.parse(req.body.teamids);
    if (!teamids) {
        json.status = "fail";
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().findTeamByIds(teamids, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});


Team.post('/admission', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let userid = req.body.userid;

    if (!teamid || !userid) {
        json.status = "fail";
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().admissionUser(teamid, userid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/search', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let keyword = req.body.keyword;

    if (!keyword) {
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().teamSearch(keyword, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/info', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    if (!teamid) {
        json.status = "fail";
        json = new JsonResponse("fail", "Invalid params.", null);
        res.json(json);
        return;
    }

    db.database().teamInfo(teamid, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/filelist', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let limit = parseInt(req.body.limit);
    if (!limit) {
        limit = 10;
    }

    db.database().articleFileList(teamid, limit, function (doc, err) {
        if (err) {
            next(err, req, res);
            return;
        }
        json.data = doc;
        res.json(json);
    });
});

Team.post('/changeprofile', (req, res, next) => {

    let json = new JsonResponse("success", "", null);

    let teamid = req.body.teamid;
    let name = req.body.name;
    let desc = req.body.desc;
    let cover = JSON.parse(req.body.cover)

    if (!teamid) {
        json.status = "fail";
        json.desc = "invalid params";
        res.json(json);
        return;
    }

    let params = {
        teamid: teamid,
        cover: cover,
        name: name,
        desc: desc
    };

    db.database().teamChangeProfile(params, (doc, err) => {
        if (err) {
            next(err, req, res);
            return;
        }        
        res.json(json);
    });
});

export default Team
