'use strict';

import mongodb from './mongodb/mongodb';

const db = {};

db.database = function() {
    if (this.dbType === 'mongodb') {
        return mongodb;
    }
};

db.connect = function(type, url, name, poolSize) {
    this.dbType = type;
    this.database().connect(url, name, poolSize);
};

export default db