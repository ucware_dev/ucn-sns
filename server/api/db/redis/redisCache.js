'use strict';

import config from '../../../../server.config';
import redis from 'redis';
import wst from '../../../wst';

const option = {host: config.redisdb.ip, port: config.redisdb.port};
const redisClient = redis.createClient(option);
redisClient.on("error", (err) => {
    console.log("REDIS Error : " + err);
});


const redisCache = {};

redisCache.set = function (key, value, cb) {
    redisClient.set(key, value,  (err, reply) => {

        if (err) {
            wst.log('REDIS : ' + err);
            cb(err.message.toString(), null);
            return;
        }
        cb(null, value);
    });
};

redisCache.get = function (key, cb) {
    redisClient.get(key, (err, user) => {

        if (err) {
            wst.log('REDIS : ' + err);
            cb(err, null);
            return;
        }

        if (!user) {
            cb(null, null);
            return;
        }
        cb(null, user);
    });
}

redisCache.del = (key) => {
    redisClient.del(key);
}

export default redisCache
