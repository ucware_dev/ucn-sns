'use strict';

import mongoose from 'mongoose';
import { User, Article, Comment, Team, Attachment, UcClass, UcUser } from './models';
import bluebird from 'bluebird';
import os from 'os';
import wst from '../../../wst';

const mongodb = {
    isConnected: false
};

mongodb.connect = function(location, name, poolSize) {

    if (this.isConnected)  {
        return;
    }

    let cpunum = os.cpus().length;
    if (cpunum > poolSize) {
        poolSize = cpunum;
    }

    let uri = `${location}/${name}`;
    let options = {
        promiseLibrary: bluebird,
        server: { poolSize: poolSize }
    };
    mongoose.Promise = global.Promise;
    mongoose.connect(uri, options, (err) => {
        mongodb.isConnected = true;
        if (err) {
            wst.i('[MONGO]-connect : ' + err);
        }
    });
};

mongodb.saveUser = function(user, cb) {

    let newUser = new User();
    newUser.email = user.email;
    newUser.username = user.username;
    newUser.passwd = user.passwd;
    newUser.profileImage = user.profileImage;

    newUser.save((err, doc) => {

        if (err) {
            let param = {'user':user};
            wst.i(`[MONGO]-saveUser, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.findUser = function(condition, cb) {
    
    let q = User.findOne(condition).populate('teams');
    q.exec((err, doc) => {
        if (err) {
            let param = {'condition':condition};
            wst.i(`[MONGO]-findUser, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
};

mongodb.saveArticle = function(article, cb) {

    let newArticle = new Article();
    newArticle.content = article.content;
    newArticle.files = article.files;
    newArticle.ogs = article.ogs;
    newArticle.author = article.author._id;
    newArticle.team = new mongoose.Types.ObjectId(article.teamid);;
    newArticle.vote = article.vote;
    
    newArticle.save((err, doc) => {

        if (err) {
            let param = article;
            wst.i(`[MONGO]-saveArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.updateArticle = function(articleId, article, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleId);

    Article.update({ _id: articleIdObj }, {
        $set: {
            content: article.content,
            files: article.files,
            vote: article.vote
        }
    }, (err, doc) => {

        if (err) {
            let param = {'articleId':articleId};
            wst.i(`[MONGO]-updateArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};


mongodb.updateEmojiArticle = function(articleId, user, emojiCode, cb) {

    let id = new mongoose.Types.ObjectId(articleId);
    let newEmoji = {
        user: user,
        emojiCode: emojiCode
    };

    Article.findOne({_id:id}, (err, doc) => {

        if (err) {
            let param = {'articleId':articleId, 'emoji':newEmoji};
            wst.i(`[MONGO]-updateEmojiArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (!doc.emojis || doc.emojis.length <= 0) {
            Article.update({_id: id}, { $push: {
                            emojis: newEmoji
                }}, (err, doc) => {

                if (err) {
                    let param = {'articleId':articleId, 'emoji':newEmoji};
                    wst.i(`[MONGO]-updateEmojiArticle-update, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                    cb(null, err);
                    return;
                }

                if (cb) {
                    cb(doc);
                }
            });
            return;

        } else {

            let found = false;
            let removeIdx = -1;
            doc.emojis.forEach( (v, i) => {

                if (v.user._id.toString() === user._id.toString()) {

                    if (v.emojiCode === emojiCode) {
                        removeIdx = i;
                    }

                    v.emojiCode = emojiCode;
                    found = true;
                }
            });

            if (found) {
                if (removeIdx >= 0) {
                    doc.emojis.splice(removeIdx, 1);
                }
                Article.update({_id: id}, {$set:{ emojis: doc.emojis}}, (err, doc) => {

                    if (err) {
                        let param = {'articleId':articleId, 'emoji':newEmoji};
                        wst.i(`[MONGO]-updateEmojiArticle-update2, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                        cb(null, err);
                        return;
                    }

                    if (cb) {
                        cb();
                    }
                });

            } else {
                Article.update({_id: id}, {$push:{ emojis: newEmoji}}, (err, doc) => {

                    if (err) {
                        let param = {'articleId':articleId, 'emoji':newEmoji};
                        wst.i(`[MONGO]-updateEmojiArticle-update3, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                        cb(null, err);
                        return;
                    }

                    if (cb) {
                        cb();
                    }
                });
            }
        }
    });
};

mongodb.updateArticleCommentCount = function(articleId, commentCnt, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleId);

    Article.update({_id: articleIdObj}, {$set:{ commentsCount: commentCnt}}, (err, doc) => {

        if (err) {
            let param = {'articleId':articleId, 'commentCnt':commentCnt};
            wst.i(`[MONGO]-updateArticleCommentCount, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb();
        }
    });
};


mongodb.fetchArticle = function(count, teamid, cb) {

    let teamIdObj = new mongoose.Types.ObjectId(teamid);

    let q = Article.find({'team' : teamIdObj}).populate('author files vote.items.voters').sort('-date').limit(count);
    q.exec((err, doc) => {

        if (err) {
            let param = {'count':count, 'teamid':teamid};
            wst.i(`[MONGO]-fetchArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.fetchNewArticle = function(articleid, teamid, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleid);
    let teamIdObj = new mongoose.Types.ObjectId(teamid);

    let q = Article.find().and([{_id: { $gt: articleIdObj }}, {'team' : teamIdObj}]).populate('author files vote.items.voters').sort('date');
    q.exec((err, doc) => {

        if (err) {
            let param = {'articleid':articleid, 'teamid':teamid};
            wst.i(`[MONGO]-fetchNewArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.fetchOldArticle = function(articleid, teamid, count, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleid);
    let teamIdObj = new mongoose.Types.ObjectId(teamid);

    let q = Article.find().and([{ _id: { $lt: articleIdObj }}, {'team' : teamIdObj}]).populate('author files vote.items.voters').sort('-date').limit(count);
    q.exec((err, doc) => {

        if (err) {
            let param = {'articleid':articleid, 'teamid':teamid, 'count':count};
            wst.i(`[MONGO]-fetchOldArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.removeArticle = function(articleid, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleid);
    
    Attachment.remove({'article' : articleIdObj}, (err, doc) => {
        if (err) {
            let param = {'articleid':articleid};
            wst.i(`[MONGO]-removeArticle-AttachmentRemove, [PARAMS]:${JSON.stringify(param)}\n${err}`);
        }

        let q = Article.remove({ _id: articleIdObj });
        q.exec((err, doc) => {

            if (err) {
                let param = {'articleid':articleid};
                wst.i(`[MONGO]-removeArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                cb(null, err);
                return;
            }

            if (cb) {
                cb();
            }
        });
    });
};

mongodb.removeAttachmentsByIds = function(articleidList, cb) {

    if (!articleidList || articleidList.length <= 0) {
        cb && cb();
        return;
    }

    Attachment.remove({'_id' : { $in: articleidList }}, (err, doc) => {
        if (err) {
            wst.i(`[MONGO]-removeAttachmentsById, [PARAMS]:${JSON.stringify(articleidList)}\n${err}`);
            cb(null, err);
            return;
        }
        cb && cb();
    });
}

mongodb.removeAttachmentsByCommentId = function(commentid, cb) {

    if (!commentid) {
        cb && cb();
        return;
    }

    let cmtIdObj = new mongoose.Types.ObjectId(commentid);

    Attachment.remove({'comment' : cmtIdObj}, (err, doc) => {
        if (err) {
            wst.i(`[MONGO]-removeAttachmentsByCommentId, [PARAMS]:${JSON.stringify(articleidList)}\n${err}`);
            cb(null, err);
            return;
        }
        cb && cb();
    });
}

mongodb.findArticle = function(articleid, cb) {

    let articleIdObj = new mongoose.Types.ObjectId(articleid);

    let q = Article.findOne({_id: articleIdObj}).populate('author files');
    q.exec((err, doc) => {

        if (err) {
            let param = {'articleid':articleid};
            wst.i(`[MONGO]-findArticle, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
};

mongodb.saveComment = function(c, cb) {

    let comment = new Comment();
    comment.article = new mongoose.Types.ObjectId(c.article);
    comment.author = new mongoose.Types.ObjectId(c.author);
    comment.content = c.content;
    
    comment.save((err, doc) => {

        if (err) {
            let param = c;
            wst.i(`[MONGO]-saveComment, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        let articleIdObj = new mongoose.Types.ObjectId(c.article);
        Comment.count({article: articleIdObj}, (err, cnt) => {
            if (err) {
                let param = c;
                wst.i(`[MONGO]-saveComment-count, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            }
            mongodb.updateArticleCommentCount(c.article, cnt, () => {
                let copyDoc = JSON.parse(JSON.stringify(doc));
                copyDoc.commentCount = cnt;

                cb && cb(copyDoc);
            });
        });
    });
};

mongodb.updateComment = function(commentId, content, imgInfo, cb) {

    let cmtIdObj = new mongoose.Types.ObjectId(commentId);

    Comment.update({_id: cmtIdObj}, { $set: {
        content: content,
        date : Date.now(),
        imgInfo: imgInfo
    }}, (err, doc) => {

        if (err) {
            let param = {'commentId':commentId, 'content':content, 'imgInfo':imgInfo};
            wst.i(`[MONGO]-updateComment, [PARAMS]:${JSON.stringify(param)}\n${err}`);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};



mongodb.deleteComment = function(articleId, commentId, cb) {

    let cmtIdObj = new mongoose.Types.ObjectId(commentId);

    Attachment.remove({'comment' : cmtIdObj}, (err, doc) => {

        if (err) {
            let param = {'articleid':articleid};
            wst.i(`[MONGO]-removeArticle-AttachmentRemove, [PARAMS]:${JSON.stringify(param)}\n${err}`);
        }

        Comment.remove({_id: cmtIdObj}, (err, doc) => {

            if (err) {
                let param = {'articleId':articleId, 'commentId':commentId};
                wst.i(`[MONGO]-deleteComment, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                cb(null, err);
                return;
            }

            let articleIdObj = new mongoose.Types.ObjectId(articleId);
            Comment.count({article: articleIdObj}, (err, cnt) => {
                if (err) {
                    let param = {'articleId':articleId, 'commentId':commentId};
                    wst.i(`[MONGO]-deleteComment-count, [PARAMS]:${JSON.stringify(param)}\n${err}`);
                }
                mongodb.updateArticleCommentCount(articleId, cnt, () => {
                    cb & cb({"commentCount" : cnt });
                });
            });
        });
    });
};


mongodb.fetchComments = function(articleId, limit, cb) {

    let articleObj = new mongoose.Types.ObjectId(articleId);

    let q = Comment.find({ article: articleObj }).sort('-date').populate('author imgInfo').limit(limit);
    q.exec((err, doc) => {

            if (err) {
                wst.i('[MONGO]-fetchComments : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
};

mongodb.fetchNewComments = function(articleId, commentId, cb) {

    let articleObj = new mongoose.Types.ObjectId(articleId);    
    let cmtIdObj = new mongoose.Types.ObjectId(commentId);

    let q = Comment.find({ article: articleObj, _id: {$gt:cmtIdObj}}).populate('author imgInfo').sort('-date');
    q.exec((err, doc) => {

            if (err) {
                wst.i('[MONGO]-fetchNewComments : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
};

mongodb.fetchOldComments = function(articleId, commentId, count, cb) {

    let articleObj = new mongoose.Types.ObjectId(articleId);
    let cmtIdObj = new mongoose.Types.ObjectId(commentId);

    let q = Comment.find({ article: articleObj, _id: {$lt:cmtIdObj}}).sort('-date').populate('author imgInfo').limit(count);
    q.exec((err, doc) => {

        if (err) {
            wst.i('[MONGO]-fetchOldComments : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.findComment = function(commentid, cb) {

    let cmtIdObj = new mongoose.Types.ObjectId(commentid);

    let q = Comment.findOne({ _id: cmtIdObj });
    q.exec((err, doc) => {

            if (err) {
                wst.i('[MONGO]-findComment : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
};


mongodb.updateLike = function(articleId, userInfo, cb) {

    let atid = new mongoose.Types.ObjectId(articleId);

    Article.findOneAndUpdate(
        {_id: atid},
        {$push: {like: userInfo}},
        {new: true},
        (err, doc) => {

            if (err) {
                wst.i('[MONGO]-updateLike : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
};

mongodb.createTeam = function(teamInfo, operatorid, cb) {
    
    let newTeam = new Team();
    newTeam.name = teamInfo.name;
    newTeam.coverImage = teamInfo.coverImage;
    newTeam.maxMembers = 0;
    newTeam.description = teamInfo.desc;
    newTeam.operator = new mongoose.Types.ObjectId(operatorid);

    newTeam.save((err, doc) => {

        if (err) {
            wst.i('[MONGO]-createTeam : ' + err);
            cb(null, err);
            return;
        }

        this.addTeamToUser(operatorid, doc._id.toString(), (result, err) => {

            if (err) {
                wst.i('[MONGO]-createTeam-addTeamToUser : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
    });
};

mongodb.addTeamToUser = function(userId, teamid, cb) {

    let id = new mongoose.Types.ObjectId(userId);
    let teamObj = new mongoose.Types.ObjectId(teamid);

    User.findOneAndUpdate({_id: id}, {$push: {teams: teamObj}}, {new: true},
        (err, doc) => {

            if (err) {
                wst.i('[MONGO]-addTeamToUser : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
};

mongodb.teamInfo = function(id, cb) {

    let teamid = new mongoose.Types.ObjectId(id);

    let q = Team.findOne({ _id: teamid}).populate('operator suboperators members');
    q.exec((err, doc) => {
        if (err) {
            wst.i('[MONGO]-teamInfo : ' + err);
            cb(null, err);
            return;
        }
        cb && cb(doc);
    });
}

mongodb.fetchTeam = function(id, count, cb) {

    let currentId = null;
    if (id !== 'new') {
        currentId = new mongoose.Types.ObjectId(id);
    }
    let limitCount = count;

    //팀 아이디가 없는 경우 첫번째부터 count 까지 찾는다.
    if (!currentId) {
        let q = Team.find().populate('operator suboperators').sort('-date').limit(parseInt(count));
        q.exec((err, doc) => {

            if (err) {
                wst.i('[MONGO]-fetchTeam1 : ' + err);
                cb(null, err);
                return;
            }

            if (cb) {
                cb(doc);
            }
        });
        return;
    }

    let q = Team.find({ _id: { $gt: currentId }}).populate('operator suboperators').sort('-date').limit(parseInt(count));
    q.exec((err, doc) => {
        if (err) {
            wst.i('[MONGO]-fetchTeam2 : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
}

mongodb.joinTeam = function(teamid, userid, cb) {

    let teamidObj = new mongoose.Types.ObjectId(teamid);
    let useridObj = new mongoose.Types.ObjectId(userid);

    let upate = {
        $addToSet: {joinRequestMembers : useridObj}
    };

    Team.findOneAndUpdate({_id: teamidObj}, upate, (err, doc) => {

        if (err) {
            wst.i('[MONGO]-joinTeam : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
}

mongodb.findTeamByIds = function(teamids, cb) {

    let ids = [];
    teamids.forEach((v, i) => {
        let obj = new mongoose.Types.ObjectId(v);
        ids.push(obj);
    });

    let condition = {
        _id : {$in: ids}
    };

    let q = Team.find(condition).populate('operator suboperators joinRequestMembers members');
    q.exec((err, doc) => {

        if (err) {
            wst.i('[MONGO]-findTeamByIds : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
}

mongodb.admissionUser = function(teamid, userid, cb) {

    let teamidObj = new mongoose.Types.ObjectId(teamid);
    let useridObj = new mongoose.Types.ObjectId(userid);

    Team.findOne({_id: teamidObj}, (err, doc) => {

        if (err) {
            wst.i('[MONGO]-admissionUser : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            //팀 정보 수정.
            let newData = doc.joinRequestMembers;
            let found = -1;
            newData.forEach((v, i) => {
                if (v.toString() === userid) {
                    found = i;
                }
            });

            if (found >= 0) {
                newData.splice(found, 1);

                Team.findOneAndUpdate({_id: teamidObj},
                    {
                        $set: {joinRequestMembers: newData},
                        $push: {members: useridObj}
                    },
                    {new: true}, (err, doc) => {

                        if (err) {
                            wst.i('[MONGO]-admissionUser-findOneAndUpdate : ' + err);
                            cb(null, err);
                            return;
                        }

                        if (cb) {
                            //유저에 팀 추가.
                            mongodb.addTeamToUser(userid, teamid, cb);
                        }
                    });
            }
        }
    });
}

mongodb.userInfo = function(userid, cb) {

    let id = new mongoose.Types.ObjectId(userid);

    let q = User.findOne({_id: id}).populate('teams');
    q.exec((err, doc) => {
        if (err) {
            wst.i('[MONGO]-userInfo : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
}

mongodb.teamSearch = function(keyword, cb) {

    let condition = [
        { name: {$regex: keyword} },
        { description: {$regex: keyword} }
        ];

    let q = Team.find().or(condition).populate('operator suboperators');
    q.exec((err, doc) => {

        if (err) {
            wst.i('[MONGO]-teamSearch : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
}

mongodb.teamChangeProfile = function(params, cb) {

    let id = new mongoose.Types.ObjectId(params.teamid);

    Team.findOneAndUpdate({_id:id}, 
                        { "coverImage.path" : params.cover.path,
                          "coverImage.filename" : params.cover.filename,
                          "coverImage.thumb" : params.cover.thumb,
                          "name": params.name,
                          "description": params.desc}, { upsert: false}, (err, doc) => {
        if (err) {
            wst.i('[MONGO]-teamChangeProfile : ' + err);
            cb(null, err);
            return;
        }
        cb && cb(doc);
    });    
};


mongodb.articleSearch = function(teamid, keyword, cb) {

    let teamIdObj = new mongoose.Types.ObjectId(teamid);
    let condition = [
        { content:{$regex: keyword}}
        ];

    let q = Article.find().and({'team' : teamIdObj}).or(condition).populate('author files').sort('-date');
    q.exec((err, doc) => {

        if (err) {
            wst.i('[MONGO]-articleSearch : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
}

mongodb.saveAttachment = function(attachment, cb) {

    let att = new Attachment();
    att.type = attachment.type;
    att.mime = attachment.mime;
    att.originalname = attachment.originalname;
    att.filename = attachment.filename;
    att.path = attachment.path;
    att.thumb = attachment.thumb;
    att.size = attachment.size;
    att.user = attachment.user._id.toString();
    att.article = attachment.article; 
    att.comment = attachment.comment; 
    att.team = attachment.team;

    att.save((err, doc) => {

        if (err) {
            wst.i('[MONGO]-saveAttachment : ' + err);
            cb(null, err);
            return;
        }

        if (cb) {
            cb(doc);
        }
    });
};

mongodb.articleFileList = function(teamid, limit, cb) {

    let teamIdObj = new mongoose.Types.ObjectId(teamid);

    let q = Attachment.find({'type' : 'file'}).and({'team' : teamIdObj}).populate('user').sort('-date');;
    q.exec((err, doc) => {
        if (err) {
            wst.i('[MONGO]-articleFileList : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
};

mongodb.userChangeProfileImage = function(params, cb) {

    let id = new mongoose.Types.ObjectId(params.userid);

    User.findOneAndUpdate({_id:id}, 
                        { "profileImage.path" : params.path,
                          "profileImage.filename" : params.filename,
                          "profileImage.thumb" : params.thumb}, { upsert: false}, (err, doc) => {

        if (err) {
            wst.i('[MONGO]-userChangeProfileImage : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
};

mongodb.updateUserInfo = function(params, cb) {

    let id = new mongoose.Types.ObjectId(params.userid);

    User.findOneAndUpdate({_id:id}, 
                        { "username" : params.username}, { upsert: false}, (err, doc) => {

        if (err) {
            wst.i('[MONGO]-userChangeProfileImage : ' + err);
            cb(null, err);
            return;
        }

        if (cb)
            cb(doc);
    });
};


mongodb.finishVote = function(artileid, cb) {

    let id = new mongoose.Types.ObjectId(artileid);

    Article.findOneAndUpdate({_id:id}, 
                        { "vote.finishVote" : true}, { upsert: false}, (err, doc) => {

        if (err) {
            wst.i('[MONGO]-finishVote : ' + err);
            cb(null, err);
            return;
        }
        cb && cb(doc);    
    });
};


mongodb.orgMigrations = function(orgTables, cb) {

    const createPromise = (org) => {
        return new Promise((rs, rj) => {
            UcClass.findOneAndUpdate({id: org.id},
            {
                id: org.id,
                kind: org.kind,
                upper_class_id: org.upper_class_id,
                org_group_code: org.org_group_code,
                group_code: org.group_code,
                group_name: org.group_name,
                user_id: org.user_id,
                order_no: org.order_no,
                user_writetime: org.user_writetime
            }
            , { upsert: true}, (e, d) => {
                if (e) {
                    rj();
                } else {
                    rs();
                }
            })
        });
    };

    //부서 정보
    let promiseArr = [];
    orgTables.forEach((v, i) => {
        let org = new UcClass();

        org.id = v.class_id ? v.class_id : '';
        org.kind = v.class_kind ? v.class_kind : '';
        org.upper_class_id = v.class_upper_class_id ? v.class_upper_class_id : '';
        org.org_group_code = v.class_org_group_code ? v.class_org_group_code : '';
        org.group_code = v.class_group_code ? v.class_group_code : '';
        org.group_name = v.class_group_name ? v.class_group_name : '';
        org.user_id = v.class_user_id ? v.class_user_id : '';
        org.order_no = v.class_order_no ? v.class_order_no : '';
        org.user_writetime = v.user_writetime ? v.user_writetime : '';

        promiseArr.push(createPromise(org));
    });


    Promise.all(promiseArr).then(()=> {
        if (cb) {
            cb({result: promiseArr.length + ' ok'});
        }
    }).catch((e) => {
        wst.i('[MONGO] orgMigrations : ' + e);
    });
};

mongodb.userMigrations = function(ucuserTables, cb) {

    const createPromise = (ucuser) => {
        return new Promise((rs, rj) => {
            UcUser.findOneAndUpdate({ id: ucuser.id },
                {
                    id: ucuser.id,
                    jumin: ucuser.jumin,
                    certnum: ucuser.certnum,
                    name: ucuser.name,
                    pass: ucuser.pass,
                    paycl_name: ucuser.paycl_name,
                    payra_name: ucuser.payra_name,
                    class_name: ucuser.class_name,
                    group_code: ucuser.group_code,
                    group_name: ucuser.group_name,
                    add_group_code1: ucuser.add_group_code1,
                    add_group_name1: ucuser.add_group_name1,
                    add_group_code2: ucuser.add_group_code2,
                    add_group_name2: ucuser.add_group_name2,
                    add_group_code3: ucuser.add_group_code3,
                    add_group_name3: ucuser.add_group_name3,
                    address: ucuser.address,
                    zipno: ucuser.zipno,
                    birthday: ucuser.birthday,
                    birth_gubun: ucuser.birth_gubun,
                    tel_home: ucuser.tel_home,
                    tel_office: ucuser.tel_office,
                    tel_company: ucuser.tel_company,
                    tel_mobile: ucuser.tel_mobile,
                    tel_fax: ucuser.tel_fax,
                    tel_ipphone: ucuser.tel_ipphone,
                    remote_tel_ipphone: ucuser.remote_tel_ipphone,
                    ipphone_hostname: ucuser.ipphone_hostname,
                    tel_ipphone_trk: ucuser.tel_ipphone_trk,
                    ipphone_fwd: ucuser.ipphone_fwd,
                    email: ucuser.email,
                    homepage: ucuser.homepage,
                    certify: ucuser.certify,
                    bigo: ucuser.bigo,
                    picture_pos: ucuser.picture_pos,
                    caption1: ucuser.caption1,
                    caption2: ucuser.caption2,
                    caption3: ucuser.caption3,
                    caption4: ucuser.caption4,
                    caption5: ucuser.caption5,
                    field1: ucuser.field1,
                    field2: ucuser.field2,
                    field3: ucuser.field3,
                    field4: ucuser.field4,
                    field5: ucuser.field5,
                    writetime: ucuser.writetime,
                    gubun: ucuser.gubun,
                    view_org_groups: ucuser.view_org_groups,
                    gbl_name: ucuser.gbl_name,
                    etc_name: ucuser.etc_name,
                    tel_roming: ucuser.tel_roming,
                    tweet_id: ucuser.tweet_id,
                    tel_ipphone_ip: ucuser.tel_ipphone_ip,
                    tel_ipphone_kind: ucuser.tel_ipphone_kind,
                }
                , { upsert: true }, (e, d) => {
                    if (e) {
                        rj();
                    } else {
                        rs();
                    }
                })
        });
    };


    let promiseArr = [];
    ucuserTables.forEach((v, i) => {
        let ucuser = new UcUser();

        ucuser.id = v.user_id ? v.user_id : '';
        ucuser.jumin = v.user_jumin ? v.user_jumin : '';
        ucuser.certnum = v.user_certnum ? v.user_certnum : '';
        ucuser.name = v.user_name ? v.user_name : '';
        ucuser.pass = v.user_pass ? v.user_pass.user_pass : '';
        ucuser.paycl_name = v.user_paycl_name ? v.user_paycl_name : '';
        ucuser.payra_name = v.user_payra_name ? v.user_payra_name : '';
        ucuser.class_name = v.user_class_name ? v.user_class_name : '';
        ucuser.group_code = v.user_group_code ? v.user_group_code : '';
        ucuser.group_name = v.user_group_name ? v.user_group_name : '';
        ucuser.add_group_code1 = v.user_add_group_code1 ? user_add_group_code1 : '';
        ucuser.add_group_name1 = v.user_add_group_name1 ? v.user_add_group_name1 : '';
        ucuser.add_group_code2 = v.user_add_group_code2 ? v.user_add_group_code2 : '';
        ucuser.add_group_name2 = v.user_add_group_name2 ? v.user_add_group_name2 : '';
        ucuser.add_group_code3 = v.user_add_group_code3 ? v.user_add_group_code3 : '';
        ucuser.add_group_name3 = v.user_add_group_name3 ? v.user_add_group_name3 : '';
        ucuser.address = v.user_address ? v.user_address: '';
        ucuser.zipno = v.user_zipno ? v.user_zipno : '';
        ucuser.birthday = v.user_birthday ? v.user_birthday : '';
        ucuser.birth_gubun = v.user_birth_gubun ? v.user_birth_gubun : '';
        ucuser.tel_home = v.user_tel_home ? v.user_tel_home : '';
        ucuser.tel_office = v.user_tel_office ? v.user_tel_office : '';
        ucuser.tel_company = v.user_tel_company ? v.user_tel_company: '';
        ucuser.tel_mobile = v.user_tel_mobile ? v.user_tel_mobile : '';
        ucuser.tel_fax = v.user_tel_fax ? v.user_tel_fax : '';
        ucuser.tel_ipphone = v.user_tel_ipphone ? v.user_tel_ipphone : '';
        ucuser.remote_tel_ipphone = v.user_remote_tel_ipphone ? v.user_remote_tel_ipphone : '';
        ucuser.ipphone_hostname = v.user_ipphone_hostname ? v.user_ipphone_hostname : '';
        ucuser.tel_ipphone_trk = v.user_tel_ipphone_trk ? v.user_tel_ipphone_trk : '';
        ucuser.ipphone_fwd = v.user_ipphone_fwd ? v.user_ipphone_fwd : '';
        ucuser.email = v.user_email ? v.user_email : '';
        ucuser.homepage = v.user_homepage ? v.user_homepage : '';
        ucuser.certify = v.user_certify ? v.user_certify : '';
        ucuser.bigo = v.user_bigo ? v.user_bigo : '';
        ucuser.picture_pos = v.user_picture_pos ? v.user_picture_pos : '';
        ucuser.caption1 = v.user_caption1 ? v.user_caption1 : '';
        ucuser.caption2 = v.user_caption2 ? v.user_caption2 : '';
        ucuser.caption3 = v.user_caption3 ? v.user_caption3 : '';
        ucuser.caption4 = v.user_caption4 ? v.user_caption4 : '';
        ucuser.caption5 = v.user_caption5 ? v.user_caption5 : '';
        ucuser.field1 = v.user_field1 ? v.user_field1 : '';
        ucuser.field2 = v.user_field2 ? v.user_field2 : '';
        ucuser.field3 = v.user_field3 ? v.user_field3 : '';
        ucuser.field4 = v.user_field4 ? v.user_field4 : '';
        ucuser.field5 = v.user_field5 ? v.user_field5 : '';
        ucuser.writetime = v.user_writetime ? v.user_writetime : '';
        ucuser.gubun = v.user_gubun ? v.user_gubun : '';
        ucuser.view_org_groups = v.user_view_org_groups ? v.user_view_org_groups : '';
        ucuser.gbl_name = v.user_gbl_name ? v.user_gbl_name : '';
        ucuser.etc_name = v.user_etc_name ? v.user_etc_name : '';
        ucuser.tel_roming = v.user_tel_roming ? v.user_tel_roming : '';
        ucuser.tweet_id = v.user_tweet_id ? v.user_tweet_id : '';
        ucuser.tel_ipphone_ip = v.user_tel_ipphone_ip ? v.user_tel_ipphone_ip : '';
        ucuser.tel_ipphone_kind = v.user_tel_ipphone_kind ? v.user_tel_ipphone_kind : '';

        promiseArr.push(createPromise(ucuser));
    });

     Promise.all(promiseArr).then(()=> {
        if (cb) {
            cb({result: promiseArr.length + ' ok'});
        }
    }).catch((e) => {
        wst.i('[MONGO] userMigrations : ' + e);
    });
};

export default mongodb
