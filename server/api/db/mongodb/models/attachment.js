'use strict';

import mongoose from 'mongoose';

class Attachment extends mongoose.Schema {
  constructor() {
    super({
        type: String,  //file, image, video, profile
        mime: String,
        originalname: String,
        filename: String,
        path: String,
        thumb: String,
        date: { type: Date, default: Date.now },
        size: String,
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        article: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Article'
        },
        comment: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Comment'
        },
        team: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Team'
        }
    });
  }
}

export default mongoose.model('Attachment', new Attachment, 'attachments')
