'use strict';

import mongoose from 'mongoose';

class Article extends mongoose.Schema {
  constructor() {
    super({
        content: String,
        date: { type: Date, default: Date.now },
        ogs: [],
        emojis: [],
        commentsCount: { type: Number, default: 0 },
        files: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Attachment'
        }],
        team: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Team'
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        vote: {
            items: [{
                _id:false,
                title: String,
                order: String,
                voters: [{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }]
            }],
            finishDate: Date,
            useFinishDate: Boolean, 
            isFinishVote: Boolean,
            isSecret: Boolean,
            isMultiChoice: Boolean,
            title: String
        }
    });
  }
}

export default mongoose.model('Article', new Article, 'articles')
