'use strict';

import User from './user';
import Article from './article';
import Comment from './comment';
import Team from './team';
import Attachment from './attachment';
import UcClass from './ucClass';
import UcUser from './ucuser';

export { User, Article, Comment, Team, Attachment, UcClass, UcUser}