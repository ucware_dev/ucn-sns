'use strict';

import mongoose from 'mongoose'; 

class Team extends mongoose.Schema {
  constructor() {
    super({
        name: String,
        coverImage: Object,
        description: String,
        operator:  {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        suboperators: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
          }
        ],
        joinRequestMembers: [   
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
          }
        ],
        members: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
          }
        ], 
        maxMembers: Number,
    });
  }
}

export default mongoose.model('Team', new Team, 'teams')