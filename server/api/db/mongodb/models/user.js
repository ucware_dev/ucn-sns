'use strict';

import mongoose from 'mongoose'; 

class User extends mongoose.Schema {
  constructor() {
    super({
        email: String,
        username: String,
        passwd: String,
        teams: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Team'
          }
        ],
        profileImage: {
          path: String,
          filename: String,
          thumb: String,
        },
        level: Number,
    });
  }
}

export default mongoose.model('User', new User, 'users')