'use strict';

import mongoose from 'mongoose'; 

class UcOrgGroup extends mongoose.Schema {
  constructor() {
    super({
        group_code: String,          //조직 코드 PK
        group_name: String,          //조직 명
        caption: String,             //조직 표시 명
        user_writetime:String,
    });
  }
}

export default mongoose.model('UcOrgGroup', new UcOrgGroup, 'ucurggroups')