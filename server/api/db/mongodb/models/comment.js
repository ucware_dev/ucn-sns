'use strict';

import mongoose from 'mongoose';

class Comment extends mongoose.Schema {
  constructor() {
    super({
        content: String,
        date: { type: Date, default: Date.now },
        imgInfo: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Attachment'
        },
        article: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Article'
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    });
  }
}

export default mongoose.model('Comment', new Comment, 'comments')
