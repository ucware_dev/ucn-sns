'use strict';

import mongoose from 'mongoose'; 

class UcUser extends mongoose.Schema {
  constructor() {
    super({
        id: String,                      //사용자 ID, PK (몽고디비 _id와는 별개)
        jumin: String,                  //주민번호
        certnum: String,                //확인번호
        name: String,                   //사용자명
        pass: String,                   //사용자 Password
        paycl_name: String,             //직위명
        payra_name: String,             //직책명
        class_name: String,             //직급명
        group_code: String,             //소속부서코드
        group_name: String,             //소속부서명
        add_group_code1: String,        //겸직부서코드1
        add_group_name1: String,        //겸직부서명1
        add_group_code2: String,        //겸직부서코드2
        add_group_name2: String,        //겸직부서명2
        add_group_code3: String,        //겸직부서코드3
        add_group_name3: String,        //겸직부서명3
        address: String,                //주소
        zipno: String,                  //우편번호
        birthday: String,               //생년월일 (yyyymmdd)
        birth_gubun: String,            //생일구분
        tel_home: String,               //집 전화번호
        tel_office: String,             //근무지 전화번호
        tel_company: String,            //근무지 내선 전화번호
        tel_mobile: String,             //휴대 전화번호
        tel_fax: String,                //FAX 번호
        tel_ipphone: String,            //IP폰 번호
        remote_tel_ipphone: String,     //IP폰 (Remote office)
        ipphone_hostname: String,       //IP폰 호스트명
        tel_ipphone_trk: String,        //IP폰 국번호
        ipphone_fwd: String,            //IP폰 포워딩
        email: String,                  //전자우편
        homepage: String,               //홈페이지 주소
        certify: String,                //승인 
        bigo: String,                   //사용자 관련 참고사항
        picture_pos: String,            //사용자 사진 URL
        caption1: String,               //사용자필드1 
        caption2: String,               //사용자필드2
        caption3: String,               //사용자필드3
        caption4: String,               //사용자필드4
        caption5: String,               //사용자필드5
        field1: String,                 //사용자필드 값1
        field2: String,                 //사용자필드 값2
        field3: String,                 //사용자필드 값3
        field4: String,                 //사용자필드 값4
        field5: String,                 //사용자필드 값5
        writetime: String,
        gubun: String,                  //사용자 구분
        view_org_groups: String,        //조직그룹 뷰
        gbl_name: String,               //서버 공통어
        etc_name: String,               //서버 검색어
        tel_roming: String,             //로밍폰
        tweet_id: String,               //트위터 아이디
        tel_ipphone_ip: String,         //IP폰 IP주소
        tel_ipphone_kind: String,       //단말기 종류

        //밴드 추가 데이터
        orgInfo: {},
        teams: []
    });
  }
}

export default mongoose.model('UcUser', new UcUser, 'ucusers')