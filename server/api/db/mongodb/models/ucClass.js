'use strict';

import mongoose from 'mongoose'; 

class UcClass extends mongoose.Schema {
  constructor() {
    super({
        id: Number,                     //클래스 ID PK (몽고디비 _id와 는 별개)
        kind: String,                   //클래스 구분
        upper_class_id: Number,         //상위 클래스 ID
        org_group_code: String,         //조직 코드
        group_code: String,             //부서 코드 또는 그룹 코드
        group_name: String,             //부서명 또는 그룹명
        user_id: String,                //사용자 ID
        order_no: Number,               //같은 그룹 내의 표시 순서
        gubun: String,                  //데이터 수정 방식 ( null : 자동, 9:수동 )
        user_writetime: String,         //최근수정일
    }); 
  }
}

export default mongoose.model('UcClass', new UcClass, 'ucclasses')