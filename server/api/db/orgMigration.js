'use strict';
var promise = require('bluebird');
var options = {
    promiseLib: promise 
};
var pgp = require('pg-promise')(options);

const orgMigration = {};

orgMigration.config = {
    host: 'sns.ucware.net',
    port: 5432,
    database: 'ucware_db',
    user: 'postgres',
    password: 'uc_qw_!@#$'
};

orgMigration.query = function(q, cb) {

    const connect = () => {
        if (!this.db) {
            this.db = pgp(this.config);
        }
            
    };

    connect();

    this.db.query(q).then((data)=> {

        if (cb) {
            cb(data);
        }

    }).catch((err) => {
        throw err;
    });
}

orgMigration.end = function() {
    pgp.end();
    this.db = null;
}

export default orgMigration;