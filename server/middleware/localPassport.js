'use strict';

import passport from 'passport';
let LocalStrategy = require('passport-local').Strategy;

import db from '../api/db/db';

const localPassport = {};

localPassport.setup = function() {

     passport.use(new LocalStrategy({usernameField: 'email', 
                                    passwordField: 'passwd'}, 
        (email, password, cb) => {

            let condition = { email: email};
            db.database().findUser(condition, (user) => {

                if (!user) {
                    return cb(null, null, {message: "unknown user"});
                }

                if (user.passwd !== password) {
                    return cb(null, null, {message: "wronk passwd"});
                }

                let modifyUser = JSON.parse(JSON.stringify(user));
                delete modifyUser.passwd;
                
                return cb(null, modifyUser, null);
            });
        }
    ));

    passport.serializeUser(function(user, cb) {
        cb(null, user.id);
    });

    passport.deserializeUser(function(id, cb) {
        cb(err, id);
    });
};

export default localPassport