import redis from 'redis';
import ioredis from 'socket.io-redis';
import config from '../../server.config';

const socketHandler = {};

socketHandler.listen = function(io, http) {
    io.adapter(ioredis({ 
                        host: config.redisdb.ip, 
                        port: config.redisdb.port }));
    this.io = io;
    this.store = redis.createClient();  
    
    this.handler();
};

socketHandler.handler = function() {

    this.io.on('connection', (client) => {
        client.info = {};
        client.info.team = client.handshake.query.team;
        client.join(client.info.team);
        console.log('## SOCKET CONN : ' + client.info.team);
        
        client.on('updateClientInfo', (info) =>{
            console.log('### updateClientInfo' + JSON.stringify(info));
            if (info.team !== client.info.team) {
                client.leave(client.info.team);    
                client.join(info.team);
            }
            client.info = info;
        });

        client.on('newArticle', () =>{
            console.log('## NEW ARTICLE : ' + client.info.team);
            this.io.sockets.in(client.info.team).emit('newArticle');
        });      

        client.on('disconnect', () => {
            console.log('## SOCKET DISCONN : ' + client.info.team);
            client.leave(client.info.team);
        });
    });
};
export default socketHandler


