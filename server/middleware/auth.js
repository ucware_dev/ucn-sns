'use strict';

import jwt from 'jsonwebtoken';
import redisCache from '../api/db/redis/redisCache';
import { JsonResponse } from '../api/api';
import config from '../../server.config';

const auth = {};

auth.isAuthenticated = (req, res, next) => {

    let json = new JsonResponse();

    if (!req.session.signinInfo.user) {
        json.status = 'fail';
        json.desc = 'UserData null';
        res.json(json);
        return;
    }

    let authorization = req.headers.authorization;
    if (!authorization) {
        json.status = 'fail';
        json.desc = 'Access denine';
        res.json(json);
        return;
    }

    authorization = authorization.split(' ')[1];
    jwt.verify(authorization, config.auth.secret, function (err, decoded) {
        
        if (err) {
            json.status = 'fail';
            json.desc = err.message.toString();
            res.json(json);
            return;
        }

        redisCache.get(req.session.signinInfo.user.email, (err, user) => {

            if (err) {
                json.status = 'fail';
                json.desc = err.message.toString();
                res.json(json);
                return;
            }

            //이미 로그아웃된 유저.
            if (!user) {
                json.status = 'fail';
                json.desc = 'signout';
                res.json(json);
                return;
            }

            //인증시간 만료
            // var now = new Date().getTime();
            // if (user.exp <= (now / 1000)) {
            //     json.status = 'fail';
            //     json.desc = 'expired';
            //     res.json(json);
            //     return;
            // }
            next();
        });
    });
};

auth.signToken = (email, cb) => {
    //const option = {'expiresIn': '1d'};
    const option = {};
    const payload = { id: email };
    const token = jwt.sign(payload, config.auth.secret, option);

    redisCache.set(email, token, (err, reply) => {
        cb(err, reply);
    });
};

auth.revokeToken = (email) => {
    redisCache.del(email);
};

export default auth