let config = {
    server: {
        port: 8090,
        devPort: 8091
    },

    dbType: 'mongodb',

    mongodb: {
        url: 'mongodb://localhost',
        poolsize: 5,
        port: 27017,
        name: 'ucnsns'
    },

    redisdb: {
        url: 'redis://localhost',
        ip: '127.0.0.1',
        port: 6379
    },

    auth: {
      secret: 'secret'
    },

    crypto: {
        workFactor: 5000,
        keylen: 32,
        randomSize: 256
    },
    lang: 'ko'
};

module.exports = config;