### nodejs  ###
* LTS 버전 설치


### Redis ###
* 설치 : sudo apt-get install redis-server
* 실행 : redis-server

### MongoDB ###
* 설치 : sudo apt-get install -y mongodb-org
* 실행 : sudo service mongod start

### PROJECT 모듈 설치 ###
* 전역모듈 : pm2

### 실행 ###
* 빌드 : npm run build
* 실행 : npm run start
* 중지 : npm run stop
