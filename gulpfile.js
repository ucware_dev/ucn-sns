const gulp = require('gulp'),
uglify  = require('gulp-uglify'),
babel = require('gulp-babel'),
webpack = require('webpack-stream'),
webpackConfig = require('./webpack.config.js'),
clean = require('gulp-clean'),
zip = require('gulp-zip'),
jsonfile = require('jsonfile');

const package = require('./package.json');
const DEST_TMP = './dist/tmp/';

gulp.task('before', function() {
    return gulp.src('./dist', { allowEmpty: true })
        .pipe(clean());
});

gulp.task('build-server', function() {
    return gulp.src('server/**/*.js')
        .pipe(babel({
            presets: ["es2015", "stage-0", "react"]
        }))
        .pipe(uglify())
        .pipe(gulp.dest(DEST_TMP + 'bin/'));
});

gulp.task('copy-server-config', function() {
    const newPackage = package;
    delete newPackage.devDependencies;
    delete newPackage.repository;
    delete newPackage.testBrowser;
    delete newPackage.testServer;
    delete newPackage.scripts.dev;
    delete newPackage.scripts.build;
    const filepath = DEST_TMP + 'package.json';
    jsonfile.writeFileSync(filepath, newPackage, {spaces: 4, EOL: '\r\n'});

    return gulp.src(['server.config.js', 'pm2.json'])
        .pipe(gulp.dest(DEST_TMP));
});

gulp.task('copy-browser-res', function() {
    return gulp.src(['public/**', '!public/uploads/**', '!bundle.js'])
        .pipe(gulp.dest(DEST_TMP + '/public/'));
});

gulp.task('build-browser', function() {
    return gulp.src('./browser/index.js')
        .pipe(babel({
            presets: ["es2015", "stage-0", "react"]
        }))
        .pipe(webpack(webpackConfig))
        .pipe(gulp.dest(DEST_TMP + '/public/'));
});

gulp.task('zip', function() {
    const destFilename = 'ucn_sns_' + package.version + '.zip';
    return gulp.src([DEST_TMP + '**'])
        .pipe(zip(destFilename))
        .pipe(gulp.dest('dist'))
});

gulp.task('after', function() {
return gulp.src(DEST_TMP)
    .pipe(clean());
});


gulp.task('default', gulp.series([
    'before',
    'build-server',
    'copy-server-config',
    'copy-browser-res',
    'build-browser',
    'zip',
    'after'
]));
