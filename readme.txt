CentOS 6.x 기준

0) 기본 빌드툴 설치
	yum install gcc gcc-c++


1. 몽고디비 설치.

    a) 시스템 확인
        uname -m

    b) 저장소 추가.
        nano /etc/yum.repos.d/mongodb.repo

        64 시스템
        [mongodb]
        name=MongoDB Repository
        baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
        gpgcheck=0
        enabled=1

        32 시스템
        [mongodb]
        name=MongoDB Repository
        baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/i686/
        gpgcheck=0
        enabled=1

    c) 몽고디비 설치
        yum install mongodb-org

    d) 서비스 시작 
        service mongod start

    e) 서버 실행시 자동 실행
        chkconfig mongod on

    f) 버전 확인
        mongo --version


2. 레디스 설치

	a) 저장소 추가
		rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
		rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

	b) 설치
		yum --enablerepo=epel,remi install redis

	c) 서비스 시작
		service redis restart

	e) 서버 실행시 자동 실행
		chkconfig redis on


3. Node.Js 6.x LTS 설치
	
	a) 다운로드
		curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -

	b) 설치
		yum -y install nodejs

		* 설치 안되면 a), b) 중간에 yum remove -y nodejs npm


4. npm  최신버전 업데이트 
	
	a) npm install -g npm
	
	b) 버전 확인
		npm -v
	
	c) 설치는 성공 했는데 버전이 안바뀔경우 수동 링크
        */usr/local/lib/node_modules/npm 가 새로 설치된 위치.
        */usr/local/lib/node_modules/npm/bin/npm-cli.js -v 로 버전 확인하고 맞으면 링크 생성.

        ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/bin/npm

5. nginx 설치 및 설정
	a) yum install nginx


	b) 설정 수정
		/etc/nginx/conf.d/default.conf 파일을 아래와 같이 수정하고 listen 포트 변경	
	//------------------------------------------------------------------------------------------------------------------------
	server {
	    listen       8060  default_server;
	    listen       [::]:8060 default_server;
	    server_name  _;
	    root         /usr/share/nginx/html;

	    # Load configuration files for the default server block.
	    include /etc/nginx/default.d/*.conf;

        #내부 node.js 서버 주소: 포트
	    location / {
	        proxy_set_header X-Real-IP $remote_addr;
	        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	        proxy_set_header Host $http_host;
	        proxy_set_header X-NginX-Proxy true;
	        proxy_pass http://127.0.0.1:8070/;
	        proxy_redirect off;
	    }

        #업로드 파일 제한. 설정 안하면 기본 1M.
	    client_max_body_size 30M

        #전송 용량 줄이기 위한 gzip
	    gzip on;
	    gzip_comp_level 2;
	    gzip_proxied any;
	    gzip_min_length 1000;
	    gzip_disable "MSIE [1-6]\."
	    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;


	    error_page 404 /404.html;
	        location = /40x.html {
	    }

	    error_page 500 502 503 504 /50x.html;
	        location = /50x.html {
	    }
	}
	//------------------------------------------------------------------------------------------------------------------------

	c) 서비스 실행
		service nginx start

6. 이미지 리사이징 도구 설치 (GraphicsMagick)
	
	a) 종속 라이브러리 설치
		yum install -y gcc libpng libjpeg libpng-devel libjpeg-devel ghostscript libtiff libtiff-devel freetype freetype-devel

	b) 소스 받기 및 압축 해제
		wget ftp://ftp.graphicsmagick.org/pub/GraphicsMagick/1.3/GraphicsMagick-1.3.9.tar.gz
		tar zxvf GraphicsMagick-1.3.9.tar.gz
		cd GraphicsMagick-1.3.9

	c) 설치
		./configure --enable-shared
		make
		make install

7. 동영상 썸네일 캡쳐용 FFMPEG 설치
	 
	 a) 저장소 추가
	 	rpm -Uvh http://li.nux.ro/download/nux/dextop/el6/x86_64/nux-dextop-release-0-2.el6.nux.noarch.rpm

	 b) 설치
	 	yum install ffmpeg ffmpeg-devel





	