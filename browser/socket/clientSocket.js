//CYH3813 2016/12/26
//명시적인 연결해제는 하지 않는다. updateClientInfo 이벤트로 team 이외의 정보를 지우기만함.

const clientSocket = {
    TEAM_GLOBAL: 'global'
};

clientSocket.connect = function() {
    this.socket = io.connect({query:'team=' + this.TEAM_GLOBAL});
};

clientSocket.updateNewArticle = function() {
    this.socket.emit('newArticle');
};

clientSocket.onUpdateNewArticle = function(cb) {
    this.socket.on('newArticle', cb);
};

clientSocket.updateClientInfo = function(userData) {
    
    let info = {
        team: this.TEAM_GLOBAL
    };

    if (!userData) {    
        this.socket.emit('updateClientInfo', info);
        return;
    }

    if (userData.team) {
        info.team = userData.team;
    }
    info.email = userData.email;
    info.username = userData.username;

    this.socket.emit('updateClientInfo', info);
};

export default clientSocket


