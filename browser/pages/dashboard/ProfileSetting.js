import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';

import modal from '../widget/Modal';
import ucalert from '../widget/Alert';

 import {
    userFileUploadRequest,
    userChangeProfileImgRequest,
    fetchUserInfoRequest,
    userChangeInfoRequest
 } from '../../actions/user';


class ProfileSetting extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.handleSelectTeamImage = this.handleSelectTeamImage.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleModify = this.handleModify.bind(this);

        this.uploadedCover = undefined;
    }

    handleSelectTeamImage(e) {
        e.preventDefault();

        let images = $('#input_cover_img')[0].files;
        if (!images || images.length <= 0) {
            return;
        }

        modal.showUploadModal('업로드', 0, '');

        this.handleFileUpload(images);
        
        return false;
    }

     handleFileUpload(files) {

        let file = files[0];
        let progress = ( (percent, source) => {

            if (this.uploadCanccel) {
                source.cancel();
                modal.hideUploadModal();
                return;
            }

            modal.progressUploadModal(file.name, percent, `${percent}% 업로드`);                  
            
            if (percent === 100) {
                modal.progressUploadModal(null, 100, `${percent}% 업로드 완료`);
                modal.hideUploadModal();
            }
        });

        this.props.userFileUploadRequest(this.props.auth.data, files, progress).then(() => {

            if (this.props.user.fileupload.status === 'SUCCESS') {

                this.uploadedCover = this.props.user.fileupload.data;
                delete this.uploadedCover.user;
                
                let coverImage = this.uploadedCover.path;
                coverImage = coverImage.replace(/public/gi, '');
                coverImage = coverImage + '/' + this.uploadedCover.thumb;
                $('#user-profile-img').attr('src', coverImage);        
            }
        });
    }

    handleModify(e) {

        let user = this.props.auth.data.user;
        let $username = $('#user-profile-usename').val();        

        //CYH: 현재 유저 프로필 이미지, 이름 변경 API 따로여거 각각 처리.        
        if (this.uploadedCover || ($username.length > 0 && ($username !== user.username))) {

            let self = this;

            let imgPromise = new Promise.resolve();
            if (this.uploadedCover) {
                imgPromise = new Promise(function(resolve, reject) {
                    self.props.userChangeProfileImgRequest(self.props.auth.data, self.uploadedCover, null).then(() => {
                        let result = self.props.user.profileImgChange.status === 'SUCCESS';
                        resolve(result);
                    });
                });
            }
            let namePromise = new Promise.resolve();
            if ($username.length > 0 && ($username !== user.username)) {
                namePromise = new Promise(function(resolve, reject) {
                    self.props.userChangeInfoRequest(self.props.auth.data, $username).then(() => {
                        let result = self.props.user.updateInfo.status === 'SUCCESS';
                        resolve(result);
                    });
                });
            }

            Promise.all([imgPromise, namePromise]).then(function(values) { 
                self.props.fetchUserInfoRequest(self.props.auth.data).then(()=>{
                    ucalert.info('#ucalert', '프로필 수정', '프로필이 수정되었습니다.');
                });
            });

        } else {
            ucalert.warning('#ucalert', '프로필 수정', '수정된 정보가 없습니다.');
        }
    }


    initEvent() {
        $('#input_cover_img').on('change', this.handleSelectTeamImage);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.auth.isSignin;
    }

    componentWillMount() {
        if (this.props.auth.isSignin === false) {
            this.props.router.replace('/');
        }
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        let user = this.props.auth.data.user;
        let userImage = user.profileImage.path.replace(/public/gi, '');
        userImage  = `${userImage}/${user.profileImage.thumb}`;

        return(
            <div className="col-md-4 col-md-offset-4">
                <br />
                <div className="box box-primary">
                    <br />
                    <div className="box-body box-profile">                        
                        <div className={style.ucChangImgBox} >
                            <img id="user-profile-img" src={userImage}
                                className={"profile-user-img img-responsive img-circle " + style.ucUserProfileImg} />

                            <label htmlFor="input_cover_img">
                                <img className={"img-circle clickable " + style.ucChangImg} src="/images/change_img.png" />
                            </label>
                            <input id="input_cover_img" className={style.ucInputHidden} type="file" accept="image/*"  />
                        </div>
                        
                        <h3 className="profile-username text-center">{user.username}</h3>

                        <ul className="list-group list-group-unbordered">
                            <li className="list-group-item">
                                <strong><i className="fa fa-envelope margin-r-5"></i> 이메일</strong>
                                <p className="text-muted">{user.email}</p>
                            </li>

                            <li className="list-group-item">
                                <strong><i className="fa fa-phone margin-r-5"></i> 전화번호</strong>
                                <p className="text-muted">미입력</p>
                            </li>
                        </ul>

                        <div>
                            <div className="form-group">
                                <input id="user-profile-usename" type="text" className="form-control" placeholder="이름" />
                            </div>                      
                            <a href="#" className="btn btn-primary btn-block" onClick={this.handleModify}><b>수정하기</b></a>
                        </div>

                    </div>
                </div>

                {/*<div className="modal fade" id="modal-user-img-upload" data-keyboard="false" data-backdrop="static">
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="info-box bg-aqua">
                                <span className="info-box-icon">
                                    <i className="ion ion-ios-cloud-upload-outline"></i>
                                </span>
                                <div className="info-box-content">
                                    <span className="info-box-text">업로드</span>
                                    <span id="upload-filename" className={"info-box-number : " + style.ucUploadFilename}><small>FILENAME</small></span>
                                    <div className="progress">
                                        <div id="upload-progress" className="progress-bar" style={{width: "0%"}}></div>
                                    </div>
                                    <span id="upload-desc" className="progress-description"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>*/}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userFileUploadRequest: (userData, files, cb) => {
            return dispatch(userFileUploadRequest(userData, files, cb));
        },
        userChangeProfileImgRequest: (userData, files, cb) => {
            return dispatch(userChangeProfileImgRequest(userData, files, cb));
        },
        fetchUserInfoRequest: (userData) => {
            return dispatch(fetchUserInfoRequest(userData));
        },
        userChangeInfoRequest: (userData, username) => {
            return dispatch(userChangeInfoRequest(userData, username));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSetting)
