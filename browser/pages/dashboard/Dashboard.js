import React from 'react';
import { connect } from 'react-redux';

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (this.props.auth.isSignin) {
            this.props.router.replace('/mypage');
        }
    }

    render() {
        uc.testLogin = false;
        return(
            <div className="lockscreen-wrapper">
                <div className="lockscreen-logo">
                    <a href="#"><b>UC</b>SNS</a>
                </div>
                
                <div className="lockscreen-item">
                    <div className="lockscreen-image">
                        <img src="pkgs/dist/img/user1-128x128.jpg" alt="User Image" />
                    </div>
                    <form className="lockscreen-credentials">
                    <div className="input-group">
                        <input type="password" className="form-control" placeholder="password" />

                        <div className="input-group-btn">
                        <button type="button" className="btn"><i className="fa fa-arrow-right text-muted"></i></button>
                        </div>
                    </div>
                    </form>
                </div>

                <div className="help-block text-center">
                    Enter your password to retrieve your session
                </div>
                <div className="text-center">
                    <a href="#">Or sign in as a different user</a>
                </div>
                <div className="lockscreen-footer text-center">
                    Copyright &copy; 2016-2017 <b><a href="#" className="text-black">UCWARE</a></b><br />
                    All rights reserved
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

Dashboard.propTypes = {
};

Dashboard.defaultProps = {   signInRequest: null,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)

