import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import alert from '../widget/Alert';

import { signInRequest } from '../../actions/auth';

class Signin extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    initEvent () {
        $('#bnt-signin').on('click', this.handleLogin);
    }

    handleLogin(e) {
        e.preventDefault();
        
        let email = $('input[type=email]').val();
        let passwd = $('input[type=password]').val();

        return this.props.signInRequest(email, passwd).then(() => {
                if(this.props.auth.status === "SUCCESS") {
                    $('.alert').slideUp();
                    browserHistory.push('mypage');
                    return false;
                } else {
                    alert.danger('#ucalert', '로그인 실패', '아이디 패스워드를 확인해 주세요');
                    return false;
                }
            }
        );
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.auth.isSignin) {
            return false;
        }   
        return true;
    }

    componentWillMount() {
        if (this.props.auth.isSignin) {
            this.props.router.replace('/mypage');
        }
    }

    componentDidMount() {
        this.initEvent();
        $('.alert').slideUp();
    }

    render() {
        return(
            <div className="hold-transition">
                
                <div className="login-box">

                    <div className="login-logo">
                        <a href="/"><b>UC</b>SNS</a>
                    </div>
                    
                    <div className="login-box-body">
                        <p className="login-box-msg">로그인</p>
                        <form>
                            <div className="form-group has-feedback">
                                <input type="email" className="form-control" placeholder="이메일" />
                                <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input type="password" className="form-control" placeholder="패스워드" />
                                <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div className="row">
                                <div className="col-xs-8"></div>
                                <div className="col-xs-4">
                                    <button id="bnt-signin" type="submit" className="btn btn-primary btn-block btn-flat">로그인</button>
                                </div>
                            </div>
                        </form>
                        <Link to="signup" className="text-center">회원가입</Link>
                    </div>
                    
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signInRequest: (email, password) => {
            return dispatch(signInRequest(email, password));
        },
    };
};

Signin.propTypes = {
    signInRequest: React.PropTypes.func,
    signOutRequest: React.PropTypes.func,
};

Signin.defaultProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Signin)
