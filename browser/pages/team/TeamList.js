import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import { selectTeamRequest, searchBarRequest } from '../../actions/auth';
import { 
    teamListRequest, 
    teamUserJoinRequest 
} from '../../actions/team';

class TeamList extends React.Component {

    constructor(props) {
        super(props);

        this.handleRegister = this.handleRegister.bind(this);
        this.getTeamList = this.getTeamList.bind(this);
        this.showAllTeams = this.showAllTeams.bind(this);
    }

    handleRegister(e) {    
        let selectedTeam = undefined;
        let $teamid = $(e.currentTarget).data('team');

        return this.props.teamUserJoinRequest(this.props.auth.data, $teamid).then(()=> {
            alert('가입 요청이 완료되었습니다.');
        });
    }

     showAllTeams(teams) {
        if (!teams || teams.length <= 0){
            return;
        }

        return teams.map( (t, i) => {

            let coverImage = t.coverImage.path;
            if (t.coverImage.filename) {
                coverImage = t.coverImage.path.replace(/public/gi, '') + '/' + t.coverImage.filename;
            }

            let areadyRegister = false;
            t.members.forEach((v, i) => {
                if (v === this.props.auth.data.user._id.toString()) {
                    areadyRegister = true;
                }
            });
            let registerButton = (
                <a className="btn btn-primary btn-block" data-team={t._id.toString()} onClick={this.handleRegister}><b>가입하기</b></a>
            );
            if (areadyRegister) {
                registerButton = (
                    <button className="btn btn-primary btn-block" disabled>가입됨</button>
                );
            }

            return (
                <div id="team-box" className="col-xs-6 col-sm-4 col-md-3" key={i} >
                    <div className="box box-primary" >
                        <div className="box-body box-profile">
                            <div className={style.ucTeamProfileImg} 
                                    style={{background: `url("${coverImage}") center center / cover`, backgroundRepeat: "no-repeat"}}>
                            </div>
                            
                            <br />
                            <h3 className={"profile-username text-center " + style.ucTeamListTitle}>{t.name}</h3>
                            <p className={"text-muted text-center " + style.ucTeamListDescription}>{t.description}</p>

                            <ul className="list-group list-group-unbordered">
                                <li className="list-group-item">
                                    <b>리더</b> <a className="pull-right">{t.operator.username}</a>
                                </li>
                                <li className="list-group-item">
                                    <b>멤버</b> <a className="pull-right">{t.members.length}</a>
                                </li>
                            </ul>
                            { registerButton }
                        </div>
                    </div>
                </div>
            );
        });
    }

    getTeamList() {
        this.props.teamListRequest(this.props.auth.data, null);
    }

    componentDidMount() {
        this.getTeamList();

        this.props.searchBarRequest('team');
    }

    render() {
        return(
            <div className={style.ucTeamDiv}>

                <section className={"content-header " + style.ucPageTitleSection}>
                    <h1 className="text-muted"> 
                        전체 팀목록 <b>{this.props.teams.data.length}</b>
                    </h1>
                    <ol className="breadcrumb">
                        <li className="active"><Link to="/mypage"><b>나의 팀</b></Link></li>
                    </ol>
                </section>
                <br />
                <div>
                    { this.showAllTeams(this.props.teams.data) }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        teams:state.team.list,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectTeamRequest: (team) => {
            return dispatch(selectTeamRequest(team));
        },
        teamListRequest: (userData, teamid) => {
            return dispatch(teamListRequest(userData, teamid));
        },
        teamUserJoinRequest: (userData, teamid) => {
            return dispatch(teamUserJoinRequest(userData, teamid));

        },searchBarRequest: (type) => {
            return dispatch(searchBarRequest(type));
        }
        
    };
};

TeamList.propTypes = {
    selectTeamRequest: React.PropTypes.func,
};

TeamList.defaultProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamList)

