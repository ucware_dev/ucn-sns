import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import { teamAdmissionUserRequest, teamFindByIdsRequest } from '../../actions/team';

class TeamJoinRequest extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showJoinRequestTeam = this.showJoinRequestTeam.bind(this);
        this.handleAdmission = this.handleAdmission.bind(this);
        this.handleFetchOwnTeamsInfo = this.handleFetchOwnTeamsInfo.bind(this);
    }

    initEvent() {
    }

    handleFetchOwnTeamsInfo () {
        if (this.props.auth.data.user) {

            let ownTeamids = [];
            this.props.auth.data.user.teams.forEach((v, i) => {
                if (v.operator.toString() === this.props.auth.data.user._id.toString()) {
                    ownTeamids.push(v._id.toString());
                }
            });

            if (ownTeamids.length <= 0) {
                return;
            }
            this.props.teamFindByIdsRequest(this.props.auth.data, ownTeamids).then(() => {
            });
        }
    }

    handleAdmission(e) {
        let teamid = $(e.currentTarget).data('teamid');
        let userid = $(e.currentTarget).data('userid');

        this.props.teamAdmissionUserRequest(this.props.auth.data, teamid, userid).then(()=>{
            alert('가입 승인이 완료되었습니다.');
            this.handleFetchOwnTeamsInfo();
        });

        return false;
    }

    showJoinRequestTeam(teams) {

        if (!teams || teams.length <= 0){
            return;
        }

        const showMembers = (members, teamid) => {
 
            return members.map((m, i) => {

                let profileImage = m.profileImage.path.replace(/public/gi, '');
                profileImage = `${profileImage}/${m.profileImage.thumb}`;

                return (
                    <div className="box-comment" key={ i + Date.now().toString() }>
                        <img className="img-circle img-sm" src={profileImage} />
                        <div className="comment-text">
                            <span className="username">
                                {m.username}
                                <button className="btn btn-success btn-xs pull-right"
                                    data-teamid={teamid}
                                    data-userid={m._id.toString()}
                                    onClick={this.handleAdmission} >가입승인</button>
                            </span>
                            {m.email}
                        </div>
                    </div>                    
                );
            });
        };

        return teams.map( (t, i) => {

            if (t.joinRequestMembers.length <= 0) {
                return;
            }

            let coverImage = t.coverImage.path;
            coverImage = t.coverImage.path.replace(/public/gi, '') + '/' + t.coverImage.thumb;

            return (
                <div className="box box-widget" key={i}>
                    <div className="box-header with-border">
                        <div className="user-block">
                            <img className="" src={coverImage} />
                            <span className="username"><a href="#">{t.name}</a></span>
                            <span className="description">{t.description}</span>
                        </div>            
                    </div>

                    <div className="box-body">
                        <p>멤버 {t.members.length + 1}, 리더 {t.operator.username}</p>
                    </div>

                    <div className="box-footer box-comments" key={i}>
                        { showMembers(t.joinRequestMembers, t._id.toString()) }
                    </div>

                    <div className="box-footer">
                    </div>
                </div>
            );
        });
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return(
            <div className="col-md-6 col-md-offset-3">
                <h1 className="text-center text-muted">가입요청</h1>
                { this.showJoinRequestTeam(this.props.findteams.data) }
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        team:state.team,
        findteams: state.team.findteams
};
};

const mapDispatchToProps = (dispatch) => {
    return {
        teamAdmissionUserRequest: (userData, teamid, userid) => {
            return dispatch(teamAdmissionUserRequest(userData, teamid, userid));
        },
        teamFindByIdsRequest: (userData, teamids) => {
            return dispatch(teamFindByIdsRequest(userData, teamids));
        }

    };
};

TeamJoinRequest.propTypes = {
};

TeamJoinRequest.defaultProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamJoinRequest)
