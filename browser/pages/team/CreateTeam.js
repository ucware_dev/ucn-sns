import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import alert from '../widget/Alert';
import modal from '../widget/Modal';

import {
    fetchUserInfoRequest,
 } from '../../actions/user';

import {
    teamCreateRequest,
    teamFileUploadRequest
 } from '../../actions/team';


class CreateTeam extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.handleCreateTeam = this.handleCreateTeam.bind(this);
        this.handleSelectTeamImage = this.handleSelectTeamImage.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);

        this.uploadedCover = undefined;
    }

    initEvent() {
        $('#btn-ctreate-team').on('click', this.handleCreateTeam);
        $('#selTeamImg').on('change', this.handleSelectTeamImage);
    }

    handleCreateTeam(e) {
        e.preventDefault();

        let name = $('#team-name').val();
        let desc = $('#team-desc').val();

        if (!name || name.length <= 0) {
            alert.danger('#ucalert', '팀 만들기', '팀 이름을 입력해 주세요');
            return;
        }

        if (!desc || desc.length <= 0) {
            alert.danger('#ucalert', '팀 만들기', '팀 소개를 입력해 주세요');
            return;
        }

        if (!this.uploadedCover) {
            alert.danger('#ucalert', '팀 만들기', '팀 이미지를 선택해 주세요');
            return;
        }

        let team = {
            name: name,
            desc: desc,
            coverImage: {
                path: this.uploadedCover.path,
                filename: this.uploadedCover.filename,
                thumb: this.uploadedCover.thumb,
            },
            maxMembers: 0
        };

        this.props.teamCreateRequest(this.props.auth.data, team).then(() => {
            if (this.props.team.status === 'SUCCESS') {
                this.props.fetchUserInfoRequest(this.props.auth.data).then(()=>{
                    alert.hide();
                    setTimeout(()=> {
                        this.props.router.replace('/mypage');
                    }, 500);
                });
            }
        });
        
        return false;
    }

    handleSelectTeamImage(e) {
        e.preventDefault();

        let images = $('#selTeamImg')[0].files;
        if (!images || images.length <= 0) {
            return;
        }

        modal.showUploadModal('업로드', 0, '');
        
        this.handleFileUpload(images);
        
        return false;
    }

    handleFileUpload(files) {

        let file = files[0];
        let progress = ( (percent, source) => {

            if (this.uploadCanccel) {
                source.cancel();
                modal.hideUploadModal();
                return;
            }

            modal.progressUploadModal(file.name, percent, `${percent}% 업로드`);                  

            if (percent === 100) {
                modal.progressUploadModal(null, 100, `${percent}% 업로드 완료`);
                modal.hideUploadModal();
            }
        });

        this.props.teamFileUploadRequest(this.props.auth.data, files, progress).then(() => {

            if (this.props.team.fileupload.status === 'SUCCESS') {

                this.uploadedCover = this.props.team.fileupload.data;
                delete this.uploadedCover.user;

                let coverImage = this.uploadedCover.path;
                coverImage = coverImage.replace(/public/gi, '');
                coverImage = coverImage + '/' + this.uploadedCover.filename;

                $('#teamCoverImg').attr('src', coverImage);        
            }
        });
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return(
            <div className="col-md-6 col-md-offset-3">
                
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">팀만들기</h3>
                    </div>
                    
                    <div className="box-body">

                        <div className="box-body box-profile">
                            <img id="teamCoverImg" className={"img-responsive " + style.ucCreateTeamImgSize} src="/images/noimage.png" />
                            <br />
                            <div >
                                <label className="btn btn-primary clickable pull-right btn-sm" htmlFor="selTeamImg">팀 이미지</label>
                                <input id="selTeamImg" className={style.ucInputHidden} type="file" accept="image/*"  />
                            </div>
                        </div>
                        
                        <form role="form">
                            <div className="form-group">
                                <label>팀이름</label>
                                <input id="team-name" type="text" className="form-control" placeholder="팀 이름을 입력하세요..." />
                            </div>
                            <div className="form-group">
                                <label>팀 소개</label>
                                <textarea id="team-desc" className="form-control" rows="3" placeholder="소개글을 입력하세요... "></textarea>
                            </div>                            
                        </form>
                    </div>
                    
                     <div className="box-footer">
                        <button id="btn-ctreate-team" type="submit" className="btn btn-primary pull-right btn-sm">만들기</button>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        team:state.team,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        teamCreateRequest: (userData, teamInfo) => {
            return dispatch(teamCreateRequest(userData, teamInfo));
        },
        fetchUserInfoRequest: (userData) => {
            return dispatch(fetchUserInfoRequest(userData));
        },
        teamFileUploadRequest: (userData, files, cb) => {
            return dispatch(teamFileUploadRequest(userData, files, cb));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTeam)
