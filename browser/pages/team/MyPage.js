import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import { selectTeamRequest, searchBarRequest } from '../../actions/auth';
import { teamFindByIdsRequest } from '../../actions/team';

class MyPage extends React.Component {

    constructor(props) {
        super(props);

        this.showMyTeams = this.showMyTeams.bind(this); 
        this.handleSelectTeam = this.handleSelectTeam.bind(this);
        this.handleGetMyTeamsInfo = this.handleGetMyTeamsInfo.bind(this);
    }

    handleSelectTeam(e) {    

        let selectedTeam = undefined;
        let $teamid = $(e.currentTarget).data('team');
        this.props.auth.data.user.teams.forEach((t, i) => {
            if (t._id.toString() === $teamid) {
                selectedTeam = t;
            }
        });

        let params = {
                id: selectedTeam._id.toString(),
                name: selectedTeam.name,
                coverImage: selectedTeam.coverImage
            };
        this.props.selectTeamRequest(params);

        browserHistory.push('article');
    }

    showMyTeams(teams) {
        if (!teams || teams.length <= 0){
            return;
        }

        teams.sort(function(a, b) {
            return a.name > b.name;
        });

        return teams.map( (t, i) => {

            let coverImage = t.coverImage.path;
            if (t.coverImage.filename) {
                coverImage = t.coverImage.path.replace(/public/gi, '') + '/' + t.coverImage.filename;
            }

            return (                
                <div id="team-box" className="col-xs-6 col-sm-4 col-md-3" key={i} data-team={t._id.toString()} onClick={this.handleSelectTeam}>
                    <div className="box box-widget widget-user">
                        <div className="widget-user-header bg-black"  style={{background: `url("${coverImage}") center center / cover`, backgroundRepeat: "no-repeat"}}>
                        </div>
                        <div className={"box-footer " + style.ucTeamFooter}>
                            <div className="description-block">
                                <h5 className={"description-header " + style.ucTeamTitle}>{t.name}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }

    handleGetMyTeamsInfo() {

        $('#joinReq').text('0');
        
        if (!this.props.auth.data.user) {
            return;
        }

        let myTeamIds = [];
        this.props.auth.data.user.teams.forEach((t, i) => {
            if (t.operator.toString() === this.props.auth.data.user._id.toString()) {
                myTeamIds.push(t._id.toString());
            }
        });

        if (myTeamIds.length <= 0) {
            return;
        }

        let totalJoinsRequest = 0;
        this.props.teamFindByIdsRequest(this.props.auth.data, myTeamIds).then(() => {
            if (this.props.team.findteams.status === 'SUCCESS') {
                this.props.team.findteams.data.forEach((v, i) => {
                    if (v.joinRequestMembers) {
                        totalJoinsRequest += v.joinRequestMembers.length;
                    }
                });
                $('#joinReq').text(totalJoinsRequest);
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.auth.isSignin;
    }

    componentWillMount() {

        this.props.searchBarRequest(null);

        if (this.props.auth.isSignin === false) {
            this.props.router.replace('/');
        }
    }

    componentDidMount() {
        this.handleGetMyTeamsInfo();
    }

    render() {
        return(
            <div className={style.ucTeamDiv}>

                <section className={"content-header " + style.ucPageTitleSection}>
                    <h1 className="text-muted">
                        내 팀 <b>{this.props.auth.data.user.teams.length}</b>
                    </h1>
                    <ul className="breadcrumb">
                        <li><Link to="teams"><b>전체 팀 목록</b></Link></li>
                        <li><Link to="joinrequest"><b>가입요청(<span id="joinReq">0</span>)</b></Link></li>
                    </ul>
                </section>
                <br />
                <div>
                    { this.showMyTeams(this.props.auth.data.user.teams)}
                    <div className="col-xs-12 col-sm-4 col-md-3">
                        <Link to="createteam">
                            <div className={"box box-widget widget-user " + style.ucTeamCreateBox}>
                                팀 만들기
                            </div>
                        </Link>
                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        team:state.team,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        teamFindByIdsRequest: (userData, teamids) => {
            return dispatch(teamFindByIdsRequest(userData, teamids));
        },
        selectTeamRequest: (team) => {
            return dispatch(selectTeamRequest(team));

        },searchBarRequest: (type) => {
            return dispatch(searchBarRequest(type));
        }
    };
};

MyPage.propTypes = {
};

MyPage.defaultProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(MyPage)
