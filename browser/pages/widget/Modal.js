let Modal = {
    uploadModal: `
        <div class="modal fade" id="uc-upload-modal" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="info-box bg-aqua">
                        <span class="info-box-icon">
                            <i class="ion ion-ios-cloud-upload-outline"></i>
                        </span>
                        <div class="info-box-content">
                            <span id="upload-modal-title" class="info-box-text">업로드</span>
                            <span id="upload-modal-filename" class="info-box-number" style="font-size:10pt;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">FILENAME</span>

                            <div class="progress">
                                <div id="upload-modal-progress" class="progress-bar" style={{width: "0%"}}></div>
                            </div>
                            <span id="upload-modal-desc" class="progress-description" style="font-size:10pt;"></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        `   
}

Modal.showUploadModal = function(title, filename, desc) {

    $('#uc-upload-modal').modal('hide');
    $('#uc-upload-modal').remove();

    $('#upload-modal-progress').attr('style', `width:${0}%`);
    $('#upload-modal').html(this.uploadModal);
    $('#uc-upload-modal').modal('show');
};

Modal.progressUploadModal = function(filename, percent, desc) {
    filename && $('#upload-modal-filename').text(filename);
    percent && $('#upload-modal-progress').attr('style', `width:${percent}%`);
    desc && $('#upload-modal-desc').text(desc);
}

Modal.hideUploadModal = function() {
    
    setTimeout(()=>{
        $('#uc-upload-modal').modal('hide');
    }, 1000);
    
    setTimeout(()=>{
        $('#uc-upload-modal').remove();
        $('#upload-modal').html('');
    }, 2000);
};

export default Modal