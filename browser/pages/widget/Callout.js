const Callout = {};

Callout.danger = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="callout callout-danger">
            <h4>${title}</h4>

            <p>${message}</p>
        </div>
    `);
};

Callout.info = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="callout callout-info">
            <h4>${title}</h4>

            <p>${message}</p>
        </div>
    `);
};

Callout.warning = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="callout callout-warning">
            <h4>${title}</h4>

            <p>${message}</p>
        </div>
    `);
};

Callout.success = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="callout callout-success">
            <h4>${title}</h4>

            <p>${message}</p>
        </div>
    `);
};

Callout.hide = () => {
    $('.callout').slideUp();
};


export default Callout