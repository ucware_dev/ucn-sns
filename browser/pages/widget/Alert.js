const Alert = {};

Alert.danger = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> ${title}</h4>
            ${message}
        </div>`);
};

Alert.info = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i> ${title}</h4>
            ${message}
        </div>
    `);
};

Alert.warning = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-warning"></i> ${title}</h4>
            ${message}
        </div>
    `);
};

Alert.success = (selector, title, message) => {
    $(selector).html('');
    $(selector).html(`
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> ${title}</h4>
            ${message}
        </div>
    `);
};

Alert.hide = () => {
    $('.alert').slideUp();
};

export default Alert