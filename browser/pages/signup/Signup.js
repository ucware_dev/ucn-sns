import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import alert from '../widget/Alert';
import callout from '../widget/Callout';
import { signUpRequest } from '../../actions/auth';

class Signup extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    initEvent() {
        $('button[type=submit').on('click', this.handleSubmit);
    }

    handleSubmit(e) {
    
        let username = $('#username').val();
        let email = $('#email').val();
        let password = $('#password').val();
        let passwordConfirm = $('#password-confirm').val();

        if (password !== passwordConfirm) {
            $('#password-confirm').val('');
            $('#password-confirm').focus();
            alert.danger('#ucalert', '회원가입', '패스워드 확인이 일치하지 않습니다.');
            return false; 
        }

        this.props.signUpRequest(email, password, username).then(()=>{
            if (this.props.auth.status === 'SUCCESS') {

                $('input').prop( "disabled", true );
                $('button').prop( "disabled", true );

                callout.success('#ucalert', '환영합니다.', '회원가입이 완료되었습니다. 로그인 페이지로 이동합니다.');
                setTimeout(() => {
                    this.props.router.replace('/signin');
                    $('.callout').slideUp();
                }, 3000);
            } else {
                $('input').val('');
                alert.danger('#ucalert', '회원가입', '이미 가입된 회원입니다.');
            }
        });

        return false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.auth.isSignin) {
            return false;
        }

        return true;
    }

    componentWillMount() {
        if (this.props.auth.isSignin) {
            this.props.router.replace('/mypage');
        }
    }


    componentDidMount() {
        this.initEvent();
        $('.alert').slideUp();
    }

    render() {
        return(
            <div className="hold-transition" >

                <br />
                <div id="signin-alert"></div>
                <div className="register-box">
                    <div className="login-logo">
                        <a href="/"><b>UC</b>SNS</a>
                    </div>

                    <div className="register-box-body">
                        <p className="login-box-msg">회원가입</p>

                        <form>
                            <div className="form-group has-feedback">
                                <input id="username" type="text" className="form-control" placeholder="이름" />
                                <span className="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input id="email" type="email" className="form-control" placeholder="이메일" />
                                <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input id="password" type="password" className="form-control" placeholder="패스워드" />
                                <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input id="password-confirm" type="password" className="form-control" placeholder="패스워드 확인" />
                                <span className="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                            <div className="row">
                                <div className="col-xs-8"></div>
                                <div className="col-xs-4">
                                    <button type="submit" className="btn btn-primary btn-block btn-flat">회원가입</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signUpRequest: (email, password, username) => {
            return dispatch(signUpRequest(email, password, username));
        },
    };
};

Signup.propTypes = {
    signUpRequest: React.PropTypes.func,
};

Signup.defaultProps = {
    auth: null,
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup)