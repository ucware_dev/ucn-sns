import React from 'react';

class NotFound extends React.Component {
    render() {
        return (
            <div className="col s10 m8 l6 offset-l3 offset-m2 offset-s1">
                <h1><center>404 Not found</center></h1>
            </div>
        );
    }
}
export default NotFound