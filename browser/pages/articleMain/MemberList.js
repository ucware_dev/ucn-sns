import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';

class MemberList extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showMemberList = this.showMemberList.bind(this);
    }

    showMemberList(members) {

        if (!members || members.length <= 0) {
            return;
        }

        return members.map((m, i) => {

            let profileImage = m.profileImage.path.replace(/public/gi, '');
            profileImage = `${profileImage}/${m.profileImage.thumb}`;    

            return (
                <li key={i}>
                    <div className="user-block">
                        <img className="img-circle" src={profileImage} />
                        <span className="username">{m.username}</span>
                        <span className="description">{m.email}</span>
                    </div>
                </li>
            );
        });
    }

    initEvent() {
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        let leader = this.props.team.selectTeam.team.operator;
        let leaderImg = leader.profileImage.path.replace(/public/gi, '');
        leaderImg = `${leaderImg}/${leader.profileImage.thumb}`;    

        return(
            <div>
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">멤버</h3>
                    </div>
                    <div className="box-body">

                        <ul className={"nav nav-stacked " + style.ucMemberList}>
                            <li>
                                <div className="user-block">
                                    <img className="img-circle" src={leaderImg}  />
                                    <span className="username">{leader.username}<span className="label label-primary pull-right">리더</span></span>
                                    <span className="description">{leader.email}</span>
                                </div>
                            </li>

                            { this.showMemberList(this.props.team.selectTeam.team.members)}

                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        team:state.team
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MemberList)
