import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import moment from 'moment';
import filesize from 'filesize';

class TeamFiles extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showFileList = this.showFileList.bind(this);

        moment.locale('ko');
    }

    showFileList(files) {

        if (!files || files.length <= 0) {
            return;
        }

        return files.map((f, i) => {

            let icon = '/images/file.png';
            let d = new Date(f.date);

            return (
                <li key={i}>
                    <div className="user-block">
                        <img className={style.uTeamFileIcon} src={icon} />
                        <span className={"username " + style.ucLineEllipse}>{f.filename}</span>
                        <span className={"description " + style.ucLineEllipse}><b>{filesize(f.size)} {moment(d).format('YYYY년 MM월 DD일')}</b> {f.user.username}</span>
                        <span className="description">
                            <span data-toggle="tooltip" className="badge bg-light-grey clickable">
                                <Link to={'/article/' + f.article} style={{color:"white"}}>
                                    <small>원글보기</small>
                                </Link>
                            </span>
                        </span>
                    </div>
                </li>
            );
        });
    }

    initEvent() {
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        if (!this.props.team.filelist.data || this.props.team.filelist.data.length <= 0) {
            return (<div></div>);
        }

        return(
            <div>
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">파일</h3>
                    </div>
                    <div className="box-body">

                        <ul className={"nav nav-stacked " + style.ucMemberList}>
                             { this.showFileList(this.props.team.filelist.data)} 
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        team:state.team
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamFiles)

