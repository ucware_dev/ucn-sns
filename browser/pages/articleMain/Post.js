import style from './style.css';
import React from 'react';
import moment from 'moment';
import PostContent from './PostContent';
import CommentList from './CommentList';
import PostSummary from './PostSummary';

const CONTENT_MIN_SIZE = 350;

class Post extends React.Component {

    constructor(props) {
        super(props);
        
        moment.locale('ko');

        this.now = Date.now();

        this.state = {
            commentCount: this.props.post.commentsCount
        };
        
        this.initEvent = this.initEvent.bind(this);
        this.deletePost  = this.deletePost.bind(this);
        this.editPost  = this.editPost.bind(this);
        this.commentSubmit = this.commentSubmit.bind(this);
        this.reloadComment = this.reloadComment.bind(this);
        this.MORE_MCOMMENT_LIMIT = 10;
        this.moreComment = this.moreComment.bind(this);
        this.deleteCommentWithComentId = this.deleteCommentWithComentId.bind(this);
        this.parseDate = this.parseDate.bind(this);
    
        this.collapsePost = this.collapsePost.bind(this);
        this.collapseComments  = this.collapseComments.bind(this); 

        this.clickSummary = this.clickSummary.bind(this);
        
        this.handleVoteUpdate = this.handleVoteUpdate.bind(this);
    }

    handleVoteUpdate(vote) {
        return this.props.voteUpdate(this.props.post, vote);
    }

    initEvent() {
    }

    collapseComments(e) {

        let box = $(`#${this.postId}`);
        let bf = box.find(".box-comments");

        if ($(e.currentTarget).children().hasClass("fa-comment")) {
            $(e.currentTarget).children(".fa-comment").removeClass("fa-comment").addClass("fa-comment-o");
            bf.slideUp();
        } else {   
            $(e.currentTarget).children(".fa-comment-o").removeClass("fa-comment-o").addClass("fa-comment");
            bf.slideDown();

            if (!this.props.comment || this.props.comment.length <= 0) {
                this.props.getComments(this.articleid);
            }
        }
        return false;
    }

    clickSummary(e) {

        let box = $(`#${this.postId}`);

        let toolbutton = box.find('#post-collapse');
        let collapseBody = box.find('#collapse-body');
        let summary = collapseBody.find('#summary');
        
        $(toolbutton).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
        collapseBody.css("max-height", "100%");
        $(summary).hide();
    }


    collapsePost(e) {
        let self = e.currentTarget;
        
        let box = $(`#${this.postId}`);
        let collapseBody = box.find('#collapse-body');
        let summary = collapseBody.find('#summary');
        if ($(summary).length <= 0) {
            return false;
        }

        if (!$(self).children().hasClass("fa-plus")) {
            $(self).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            collapseBody.css('max-height', `${CONTENT_MIN_SIZE}px`);
            $(summary).show();

        } else {                   
            $(self).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            collapseBody.css("max-height", "100%");
            $(summary).hide();
        }

        return false;
    }

    deletePost() {
        if (confirm("삭제 하시겠습니까?") === true) {
            this.props.deletePost(this.props.post._id.toString()).then(() => {});
        }
    }

    editPost(e) {
        $('#modal-editor').trigger('editPost.bs.modal', this.props.post);
        return false;
    }

    reloadComment(cb) {
        if (!this.props.comment || this.props.comment.length <= 0) {
            this.props.getComments(this.articleid).then(() => {
                cb && cb();
            })
        } else {
            let latestComment = this.props.comment[0];
            this.props.getNewComments(this.articleid, latestComment._id.toString()).then((success) => {
                cb && cb();
            });
        }
    }

    moreComment() {
        let lastCommentId = this.props.comment[this.props.comment.length - 1]._id.toString();
        this.props.getOldComments(this.articleid, lastCommentId, this.MORE_MCOMMENT_LIMIT);
    }

    deleteCommentWithComentId(commentid) {
        this.props.deleteComment(this.articleid, commentid).then((success)=>{
            if (success) {
                this.setState({commentCount: this.props.afterDeleteCommentCount});                
            }
        });
    }

    commentSubmit(e) {

        if (e.keyCode == 13) { //엔터키
            e.preventDefault();

            let comment = $(e.currentTarget).val();
            if (comment.length > 0) {

                let param = {
                    articleid: this.articleid,
                    comment: comment,
                    imgInfo: null  
                };
                
                $(e.currentTarget).val('');
                this.props.postComment(param).then((success) => {
                    if (success) {
                        this.reloadComment(()=> {
                            this.setState({commentCount: this.props.postCommentData.commentCount});
                        });
                    }
                });
            }

            //auto collapse
            let box = $(`#${this.postId}`);
            let bf = box.find(".box-comments");
            let commentBtn = box.find('#btn-comment-collapse');
            if (commentBtn.children().hasClass("fa-comment-o")) {
                commentBtn.children(".fa-comment-o").removeClass("fa-comment-o").addClass("fa-comment");
                bf.slideDown();
            }
        }
    }

    parseDate(now, postDateTime) {
        let timeText = moment(postDateTime).from(this.now);
        if (timeText === '몇 초 전' || timeText === '몇 초 후') {
            timeText = '방금';
        }
        return timeText;
    }

    componentDidMount() {
        this.initEvent();        
    }

    render() {

        this.articleid = this.props.post._id.toString();
        let post = this.props.post;
        let author = this.props.post.author;
        let isMyPost = this.props.myUserId === this.props.post.author._id.toString();

        let btnPostDel = (
            <button type="button" className="btn btn-box-tool" onClick={this.deletePost}>
                <i className="fa fa-times"></i>
            </button>
        );
        let btnPostEdit = (
            <button type="button" className="btn btn-box-tool" data-articleid={this.articleid} onClick={this.editPost}>
                <i className="fa fa-edit"></i>
            </button>
        );

        let btnMoreComment = (
            <button className="btn btn-block btn-xs" onClick={this.moreComment}><small>댓글 더보기</small></button>
        );    
        let allCommentsLoaded = (this.state.commentCount === this.props.comment.length);

        let userImage = author.profileImage.path.replace(/public/gi, '');
        userImage  = `${userImage}/${author.profileImage.thumb}`;

        let myIamge = this.props.myUserInfo.profileImage.path.replace(/public/gi, '');
        myIamge  = `${myIamge}/${this.props.myUserInfo.profileImage.thumb}`;

        this.postId = `post_${this.articleid}`;
        this.postCollapse = `collapse_${this.articleid}`;

        return(
            <div className="row">
                <div id={this.postId} className="box box-widget" >
                    <div className="box-header with-border">

                        {/* 사용자 */}
                        <div className={"user-block " + style.ucUserBlockFontSize}>
                            <img className="img-circle" src={userImage} alt="User Image" />
                            <span className="username">
                                <a className={style.ucBoxUserName} href="#">{author.username}</a>
                            </span>
                            <span className="description">{this.parseDate(this.now, post.date)}</span>
                        </div>
                        {/* 툴 버튼 */}
                        <div className="box-tools">
                            <button id="post-collapse" type="button" className="btn btn-box-tool"  onClick={this.collapsePost}>
                                <i className="fa fa-plus"></i>
                            </button>
                            { isMyPost && btnPostEdit }
                            { isMyPost && btnPostDel }
                        </div>
                    </div>                    

                    {/* 내용 */}
                    <div id="collapse-body" className={"box-body " + style.ucCollapseBody}>
                        <div>
                            <PostContent 
                                articleid={post._id.toString()}
                                content={post.content} 
                                vote={post.vote}
                                voteUpdate={this.handleVoteUpdate}
                                myUserInfo={this.props.myUserInfo}
                                isMyPost={isMyPost}
                                />
                            <PostSummary 
                                files={post.files} 
                                vote={post.vote}
                                clickSummary={this.clickSummary}
                                />
                        </div>
                    </div>

                    <div className="box-body" >
                        <button id="btn-comment-collapse" type="button" className="pull-right btn btn-default btn-xs" data-count={this.props.post.commentsCount} onClick={this.collapseComments}>
                            <i className="fa fa-comment-o"></i> 댓글 <span>{this.state.commentCount}</span>
                        </button>
                    </div>

                    {/* 댓글 목록 */}
                    <div className="box-footer box-comments" style={{"display": "none"}}>
                        <CommentList 
                            parseDate={this.parseDate}
                            comments={this.props.comment}
                            myUserId={this.props.myUserId}
                            getComments={this.props.getComments}
                            deleteCommentWithComentId={this.deleteCommentWithComentId}
                            />
                        { allCommentsLoaded || btnMoreComment }
                    </div>

                    {/* 푸터: 탯글 입력창  START*/}
                    <div className="box-footer">
                        <form>
                            <img className="img-responsive img-circle img-sm" src={myIamge}  />
                            <div className="img-push">
                                <input type="text" className="form-control input-sm" placeholder="댓글을 입력하세요..." onKeyDown={this.commentSubmit} />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

Post.propTypes = {
    post: React.PropTypes.object,    
    comment: React.PropTypes.array,    
};

Post.defaultProps = {
    post: null,
    comment: [],
};

export default Post
