import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import TeamProfile from './LeftMenu/TeamProfile';
import FileList from './RightMenu/FileList';
import Chat from './RightMenu/Chat';
import { teamSelectInfo, teamFileListRequest, } from '../../actions/team';
import { searchBarRequest } from '../../actions/auth';

class ArticleMain extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.handleTeamInfo = this.handleTeamInfo.bind(this);
        this.handleTeamFilelist = this.handleTeamFilelist.bind(this);
    }

    handleTeamInfo() {
        return this.props.teamSelectInfo(this.props.auth.data, this.props.currentTeam.id).then(()=> {});
    }

    handleTeamFilelist() {
        return this.props.teamFileListRequest(this.props.auth.data, this.props.currentTeam.id).then(() => {
            return this.props.team.filelist.status === 'SUCCESS';
        });
    }

    initEvent() {}

    shouldComponentUpdate(nextProps, nextState) {
        if (!this.props.auth.isSignin || !this.props.auth.data) {
            return false;
        }
        return true;
    }

    componentWillMount() {

        this.props.searchBarRequest('article');

        if (this.props.auth.isSignin === false) {
            this.props.router.replace('/');
        }
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        if (!this.props.auth.isSignin || !this.props.auth.data) {
            return (<div></div>);
        }

        return(
            <div className={style.ucArticleBody}>

                <div className={"col-md-3 hidden-xs hidden-sm " + style.ucLeftMenu}>
                    <TeamProfile 
                        fetchTeamInfo={this.handleTeamInfo}
                        user={this.props.auth.data.user}
                        team={this.props.team.selectTeam.team} 
                        articleSearchClear={this.props.searchBarRequest}
                        />
                </div>

                <div className={"col-md-6 " + style.ucNoPadding}>
                    { this.props.children }
                </div>
                
                <div className={"col-md-3 hidden-xs hidden-sm " + style.ucRightMenu}>                    
                    <FileList 
                        teamFileList={this.props.team.filelist}
                        getTeamFileList={this.handleTeamFilelist}
                    />
                    <Chat />                
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        currentTeam: state.auth.currentTeam,
        team: state.team,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        teamSelectInfo: (userData, teamids) => {
            return dispatch(teamSelectInfo(userData, teamids));
        },
        teamFileListRequest: (userData, teamid) => {
            return dispatch(teamFileListRequest(userData, teamid));
        },
        searchBarRequest: (type) => {
            return dispatch(searchBarRequest(type));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleMain)
