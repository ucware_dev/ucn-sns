import style from './style.css';
import React from 'react';
import { Link, browserHistory } from 'react-router';

class TeamProfile extends React.Component {

    constructor(props) {
        super(props);

        this.teamInfo = null;
        this.moveToArticle = this.moveToArticle.bind(this);
    }

    moveToArticle() {
        this.props.articleSearchClear('article');
        browserHistory.replace('/article');
    }

    componentDidMount() {
        this.props.fetchTeamInfo().then((teaminfo) => {});
    }

    render() {
        if (!this.props.team || !this.props.user) {
            return(<div></div>);
        }

        let isLeader = this.props.team.operator._id.toString() === this.props.user._id.toString();
        let settingBtn = (
            <li id="team-setting">
                <Link to="/article/setting">설정 </Link>
            </li>
        );

        let coverImage = this.props.team.coverImage.path;
        coverImage = this.props.team.coverImage.path.replace(/public/gi, '') + '/' + this.props.team.coverImage.thumb;

        return(
            <div className="box box-widget widget-user-2">
                
                <div id="team-img" className={"widget-user-header bg-black clickable " + style.ucTeamProfileDiv} 
                    style={{"background":`url(${coverImage}) center center / cover`, backgroundRepeat: "no-repeat"}}
                    onClick={this.moveToArticle}
                    >
                    <h3 className={style.ucTeamName}>{this.props.team.name}</h3>
                </div>

                <div className="box-footer no-padding">
                    <ul className="nav nav-stacked">
                        <li id="tp-member-list">
                            <Link to="/article/memberlist">멤버 <span id="team-members" className="pull-right badge bg-blue">{this.props.team.members.length + 1}</span></Link>
                        </li>
                        {isLeader && settingBtn }
                    </ul>
                </div>
            </div>
        );
    }
}

TeamProfile.propTypes = {
    team: React.PropTypes.object,
};

TeamProfile.defaultProps = {
    team: null
};

export default TeamProfile
