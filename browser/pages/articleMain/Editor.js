import style from './style.css';
import React from 'react';
import { browserHistory } from 'react-router';
import ucalert from '../widget/Alert';
import modal from '../widget/Modal';


class Editor extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.initCKEditor = this.initCKEditor.bind(this);
        this.handlePost = this.handlePost.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.removeNonexistentFiles = this.removeNonexistentFiles.bind(this);
        
        this.handleSelectAttachment = this.handleSelectAttachment.bind(this);
        this.createUploadPromise = this.createUploadPromise.bind(this);
        this.initEditForm = this.initEditForm.bind(this);
        this.eidtVote = this.eidtVote.bind(this);

        this.showAttachmentPreview = this.showAttachmentPreview.bind(this);
        this.showAttachmentImgPreview = this.showAttachmentImgPreview.bind(this);
        this.showAttachmentVideoPreview = this.showAttachmentVideoPreview.bind(this);
        this.showAttachmentFilePreview = this.showAttachmentFilePreview.bind(this);
        this.showUrlMetaPreview = this.showUrlMetaPreview.bind(this);
        this.showYoutubeIframe = this.showYoutubeIframe.bind(this);
        this.showVoteEditor = this.showVoteEditor.bind(this);
        this.showVoteSummary = this.showVoteSummary.bind(this);
        this.showModifyVoteSummary = this.showModifyVoteSummary.bind(this);
    
        this.uploadedFiles = [];
        this.stageFiles = [];       //실제로 서버에 올라갈 파일 목록.

        this.editorMode = {
            isNew: true,
            article: null
        };
    }

    initCKEditor() {

        let self = this;
        
        ///TOOLBAR
        if (uc.isMobile()) {
            CKEDITOR.replace('ckedt', {
            toolbarGroups : [
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                ],
                removeButtons: 'Subscript,Superscript,Cut,Scayt,Link,Undo,Image,Source,NumberedList,Outdent,Blockquote,Styles,About,Maximize,Copy,Paste,PasteText,PasteFromWord,Redo,Unlink,Anchor,HorizontalRule,Table,BulletedList,Indent,Format,SpecialChar',
                removePlugins : 'elementspath',
                format_tags : 'p;h1;h2;h3;pre',
                enterMode: CKEDITOR.ENTER_BR,
                allowedContent: true,
                basicEntities : false,
                extraPlugins : 'pastetext'
            });

        } else {
            CKEDITOR.replace('ckedt', {
            toolbarGroups : [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },                
                    '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'others', groups: [ 'others' ] },
                    { name: 'about', groups: [ 'about' ] }
                ],
                removeButtons: 'Subscript,Superscript,Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,SelectAll,Scayt,Checkbox,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiRtl,Language,BidiLtr,Anchor,Flash,Iframe,ShowBlocks,About,PageBreak,Links,Insert',
                removePlugins : 'elementspath',
                format_tags : 'p;h1;h2;h3;pre',
                enterMode: CKEDITOR.ENTER_BR,
                allowedContent: true,
                removeFormatAttributes: '',
                basicEntities : false,
                extraPlugins : 'pastetext',
                height: '450px',
            });
        }

        
        ///EVENT
        $('#modal-editor').on('shown.bs.modal', () => {   
            $("body").addClass("modal-open");
            CKEDITOR.instances.ckedt.focus();
        });

        $('#modal-editor').on('hidden.bs.modal', () => { 
            $("body").removeClass("modal-open")
            this.initEditForm();  
        });

        $('#modal-editor').on('editPost.bs.modal', (e, article) => { 
            $(`#editor-title`).text('글수정');
            $(`#btn-post`).text('수정');

            this.editorMode.isNew = false;
            this.editorMode.article = article;
            
            let setContent = (htmlContent, vote) => {
                CKEDITOR.instances.ckedt.setData(htmlContent, function(){                
                    if (CKEDITOR.instances.ckedt.getData().length <= 0) {
                        setTimeout(()=> setContent(htmlContent), 10);
                        return;

                    }  else {
                        var range = CKEDITOR.instances.ckedt.createRange();
                        range.moveToElementEditEnd( range.root );
                        editor.getSelection().selectRanges( [ range ] );
                        
                        if (vote) {
                            self.showModifyVoteSummary(vote);
                        }
                    }
                    $('#modal-editor').modal('show') ;
                } );
            };
            setContent(this.editorMode.article.content, this.editorMode.article.vote);
        });

        $('#modal-editor').on('newPost.bs.modal', (e) => { 
            
            $(`#editor-title`).text('글쓰기');
            $(`#btn-post`).text('게시');

            this.initEditForm();  
        });

        $('#modal-editor').on('voteAdd.bs.modal', (e, vote) => {
            this.vote = vote;
            if (this.editorMode.isNew === false) {
                this.editorMode.article.vote = this.editorMode.article && vote;
            }
            this.showVoteSummary(this.vote);
        });

        let editor = CKEDITOR.instances.ckedt;
        editor.on('paste', function(e) {
            let text = e.data.dataValue;
            if (/^(http|https)/.test(text)) {
                e.data.dataValue = `<a href="${e.data.dataValue}" target="_blank">${e.data.dataValue}</a>`;
                self.props.getUrlMeta(text).then((success) => {
                    if (success) {
                        self.showUrlMetaPreview(self.props.metaData.data, text);
                    }
                });
            }
        }, editor.element.$);

        editor.on('contentDom', () => {        
            let editable = editor.editable();
            editable.attachListener( editable, 'click', function(e) {
                if ($(e.data.$.target).attr('class') === 'ucEditorPreviewDel') {
                    $(e.data.$.target.parentElement).remove();
                    return;
                }

                if ($(e.data.$.target).attr('class') === 'votePreviewOpt') {
                    if ($(e.data.$.target).attr('id') === 'del') {
                        $(e.data.$.target.parentElement.parentElement).remove();
                        self.vote = null;
                        return;
                    }
                    if ($(e.data.$.target).attr('id') === 'edit') {
                        self.eidtVote();
                        return;
                    }
                }
            });
        });
    }

    initEditForm() {

        this.editorMode.isNew = true;
        this.editorMode.article = null;
        this.vote = null;

        CKEDITOR.instances.ckedt.setData('');

        this.stageFiles =  this.uploadedFiles = [];

        $('#post-upload-images').val("");
        $('#post-upload-images').closest('form').trigger('reset');

        $('#post-upload-videos').val("");
        $('#post-upload-videos').closest('form').trigger('reset');

        $('#post-upload-files').val("");
        $('#post-upload-files').closest('form').trigger('reset');
    }

    eidtVote() {
        $('#modal-vote-editor').trigger('editVote.bs.modal', this.vote || this.editorMode.article.vote);
    }

    handleSelectAttachment(e) {
        
        e.preventDefault();

        let files = $(e.currentTarget)[0].files;
        if (!files || files.length <= 0) {
            return;
        }

        let content = CKEDITOR.instances.ckedt.getData();
        this.stageFiles = this.removeNonexistentFiles(this.stageFiles, content);

        modal.showUploadModal('업로드', '', '업로드 준비중..');
        this.handleFilesUpload(files);
        
        return false;
    }

    handleFilesUpload(files) {

        ucalert.hide();

        let uploadPromise = [];
        for (let i = 0; i < files.length; i++) {
            uploadPromise.push(this.createUploadPromise(files[i]));
        }

        this.uploadedFiles = [];
        Promise.all(uploadPromise).then(() => {          
            this.showAttachmentPreview();

        }).catch((err) => {
            modal.hideUploadModal();
            $('#modal-editor').modal('hide') ; 
            ucalert.danger('#ucalert', '파일 업로드', '파일 업로드가 실패했습니다. 잠시 후 다시 시도하세요.');
        });
    }

    createUploadPromise(file) {

         let progress = ( (percent, source) => {

            if (this.uploadCanccel) {
                source.cancel();
                modal.hideUploadModal();
                return;
            }

            modal.progressUploadModal(file.name, percent, `${percent}% 업로드`);                              
            if (percent === 100) {
                modal.progressUploadModal(null, 100, `${percent}% 업로드 완료`);
            }
        });

        //업로드 1개에 대한 프로미스.
        return new Promise( (rs, rj) => {
            this.props.fileUpload([file], progress).then((success) => {
                if (success) {
                    this.uploadedFiles.push(this.props.uploadedFile.data[0]);
                    rs();
                } else {
                    rj();
                }
            });
        });
    }

    showAttachmentPreview() {

        if (this.uploadedFiles.length <= 0) {
            return;
        }
 
        this.uploadedFiles.forEach((f, i) => {
            
            if (/image/g.test(f.mime)) {
                let src = f.path.replace(/public/gi, '') + '/' + f.filename;
                this.showAttachmentImgPreview(src);

            } else if (/video/g.test(f.mime)) {
                let src = f.path.replace(/public/gi, '') + '/' + f.filename;
                let poster = "";
                if (f.thumb) {
                    poster = f.path.replace(/public/gi, '') + '/' + f.thumb;
                }
                this.showAttachmentVideoPreview(src, poster);

            } else {
                let link = f.path.replace(/public/gi, '') + '/' + f.filename;
                this.showAttachmentFilePreview(link, f.filename);
            }
            this.stageFiles.push(f)
        });
        modal.hideUploadModal();
    }

    showAttachmentImgPreview(imagePath) {

        let editor = CKEDITOR.instances.ckedt;
        let img = `
            <div class="ucEditorPreviewBox" contenteditable="false">
                <img src=${imagePath} />
                <div class="ucEditorPreviewDel">삭제</div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(img);

        $('#post-upload-images').val("");
        $('#post-upload-images').closest('form').trigger('reset');
    }   

    showAttachmentVideoPreview(videopath, poster) {
        
        let video = `
            <div class="ucEditorPreviewBox" contenteditable="false">
                <video src=${videopath} width="100%" controls poster=${poster} ></video>
                <div class="ucEditorPreviewDel">삭제</div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(video);

        $('#post-upload-videos').val("");
        $('#post-upload-videos').closest('form').trigger('reset');

    }   

    showAttachmentFilePreview(link, filename) {
        let file = `
            <div class="ucEditorPreviewBox" onclick="window.open('${link}', '_blank');" contenteditable="false">
                <div class="ucEditorFileBox">                    
                    <div></div>
                    <div>
                        <span>파일</span><br />
                        <span>${filename}</span>
                    </div>
                </div>
                <div class="ucEditorPreviewDel">삭제</div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(file);

        $('#post-upload-files').val("");
        $('#post-upload-files').closest('form').trigger('reset');
    }

    showYoutubeIframe(meta, url) {

        if (!meta.ogVideo || !meta.ogVideo.url) {
            return;
        }

        let desc = meta.ogTitle.trim();
        if (!desc || desc.length <= 0) {
            desc = meta.ogDescription;
            if (!desc) {
                desc ='여기를 눌러 링크를 확인하세요.';
            }
        }

        let title = meta.ogSiteName;
        if (!title) {
            title = meta.ogTitle;
            if (!title) {
                title = meta.ogUrl;
            }
        } 

        let youtube = `
            <div class="ucYoutubeIframe" contenteditable="false" >
                <div>
                    <iframe id="ytplayer" type="text/html" width="100%" height="360px" src="${meta.ogVideo.url}" frameborder="0" allowscriptaccess="always"></iframe>
                </div>
                <div class="ucYoutubeInfo" onclick="window.open('${url}', '_blank');">
                    <p>${desc}</p>
                    <p>${title}</p>
                </div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(youtube);
    }

    showUrlMetaPreview(meta, url) {

        if (!meta.ogImage || !meta.ogImage.url) {
            return;
        }

        if (meta.ogSiteName && meta.ogSiteName.toLowerCase() === 'youtube') {
            this.showYoutubeIframe(meta, url);
            return;
        }
        
        let logo = meta.ogImage.url;
        if (/^(http|https)/.test(logo) === false) {
            let host = meta.ogUrl;
            if (!host) {
                host = url; 
            }
            logo = `${host}${meta.ogImage.url}`;
        }

        let desc = meta.ogDescription.trim();
        if (!desc || desc.length <= 0) {
            desc = meta.ogTitle;
            if (!desc) {
                desc ='여기를 눌러 링크를 확인하세요.';
            }
        }

        let title = meta.ogSiteName;
        if (!title) {
            title = meta.ogTitle;
            if (!title) {
                title = meta.ogUrl;
            }
        } 
        
        let openGraph = `
            <div class="ucOpenGraphBox" onclick="window.open('${url}', '_blank');" contenteditable="false" >
                <div class="ucOpenGraphBox-cover" style="background: url('${logo}') center center / cover; background-repeat:no-repeat" ></div>
                <div class="ucOpenGraphBox-info">
                    <p>${desc}</p>
                    <p>${title}</p>
                </div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(openGraph);
    }

    showVoteEditor() {
        if (this.vote) {
            alert('이미 투표가 첨부되어 있습니다.');
            return;
        }
        
        $('#modal-vote-editor').trigger('voteEditorInit.bs.modal');
        $('#modal-vote-editor').modal('show') ;
    }
    showModifyVoteSummary(vote) {
        let $votePreview = $(CKEDITOR.instances.ckedt.document.$).find('#votePreview')
        let voteSummary = `
            <div id="voteSummary">                 
                <div class="votePreviewIcon"></div>
                <div class="votePreviewDesc">
                    <span>투표</span><small> 0명 참여</small><br />
                    <span>${vote.title}</span>
                </div>
            </div>
            <div class="votePreviewOption">
                <div id="del" class="votePreviewOpt">삭제</div>
                <div id="edit" class="votePreviewOpt">수정</div>
            </div>
        `;
        $votePreview.html(voteSummary);
    }
    showVoteSummary(vote) {

        $(CKEDITOR.instances.ckedt.document.$).find('#votePreview').remove();

        let voteSummary = `
            <div id="votePreview" contenteditable="false" >                 
                <div id="voteSummary">                 
                    <div class="votePreviewIcon"></div>
                    <div class="votePreviewDesc">
                        <span>투표</span><small> 0명 참여</small><br />
                        <span>${vote.title}</span>
                    </div>
                </div>
                <div class="votePreviewOption">
                    <div id="del" class="votePreviewOpt">삭제</div>
                    <div id="edit" class="votePreviewOpt">수정</div>
                </div>
            </div>
            <br />
        `;
        CKEDITOR.instances.ckedt.insertHtml(voteSummary);
    }

    initEvent() {
        $('#btn-post').on('click', this.handlePost);
        $('#post-upload-images').on('change', this.handleSelectAttachment);        
        $('#post-upload-videos').on('change', this.handleSelectAttachment);        
        $('#post-upload-files').on('change', this.handleSelectAttachment);        
    }

    handlePost(e) {

        if (!this.editorMode.isNew) {
            this.handleEdit();
            return false;
        }

        //투표는 데이터만 저장하고 따로 그린다.
        $(CKEDITOR.instances.ckedt.document.$).find('#votePreview').html('');

        let content = CKEDITOR.instances.ckedt.getData();
        this.stageFiles = this.removeNonexistentFiles(this.stageFiles, content);

        let params = {
            content: content,
            files: this.stageFiles ? this.stageFiles : null,
            ogs: null,
            vote: this.vote ? this.vote : null
        };

        this.props.postArticle(params).then((success) => {
            browserHistory.replace('/article');
            this.props.getNewArticles();
            $('#modal-editor').modal('hide');
        });

        return false;
    }

    handleEdit() {

        let articleid = this.editorMode.article._id.toString();
        let files = this.editorMode.article.files || [];
        files = files.concat(this.stageFiles);
        if (files.length <= 0) {
            files = null;
        }
        let vote = this.editorMode.article.vote;
        let content = CKEDITOR.instances.ckedt.getData();

        files = this.removeNonexistentFiles(files, content);

        this.props.editArticle(articleid, content, files, vote).then((success) => {
            this.props.getNewArticles();
            $('#modal-editor').modal('hide')  
        });
    }

    removeNonexistentFiles(files, content) {

        if (!files || files.length <= 0) {
            return [];
        }

        //순서 유지.
        var newList = [];
        
        const searchAttachment = (f) => {
            let src = f.path.replace(/public/gi, '') + '/' + f.filename;
            let re = new RegExp(src, 'ig');
            return re.test(content);
        };

        files.forEach((f, i) => {
            if (searchAttachment(f)) {
                newList.push(f);
            }
        });

        return newList;
    }

    componentDidMount() {
        this.initCKEditor();
        this.initEvent();
    }

    render() {

        let editorSize = style.ucModalEditorSize;
        if (uc.isMobile()) {
            editorSize = style.ucModalEditorSizeMobile
        }
    
        return(
            <div className="modal fade" id="modal-editor">
                <div className={"modal-dialog " + editorSize}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 id="editor-title" className="modal-title">글쓰기</h4>
                        </div>
                        <div className="modal-body">
                            <div className="box-body pad">
                                <form>
                                    <textarea id="ckedt" name="ckedt" rows="20" cols="75"></textarea>
                                </form>
                            </div>
                        </div>

                        <div className={style.ucUploadBox}>

                            <div className={style.ucUploadBtn}>
                                <label className="fa fa-image clickable text-primary" htmlFor="post-upload-images"></label>
                                <input id="post-upload-images" className={style.ucInputHidden} type="file" accept="image/*" multiple />
                            </div>

                            <div className={style.ucUploadBtn}>
                                <label className="fa fa-video-camera clickable text-primary" htmlFor="post-upload-videos"></label>
                                <input id="post-upload-videos" className={style.ucInputHidden} type="file" accept="video/*" multiple />
                            </div>

                            <div className={style.ucUploadBtn}>
                                <label className="fa  fa-paperclip clickable text-primary" htmlFor="post-upload-files"></label>
                                <input id="post-upload-files" className={style.ucInputHidden} type="file" accept="*/*" multiple />
                            </div>

                            <div className={style.ucUploadBtn} onClick={this.showVoteEditor}>
                                <label className="fa  fa-pie-chart clickable text-primary"></label>
                            </div>    
                        </div>
                        
                        <div className="modal-footer">
                            <button id="btn-post" type="button" className="btn btn-primary">게시</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Editor


