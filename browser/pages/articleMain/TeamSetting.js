import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';

import modal from '../widget/Modal';
import alert from '../widget/Alert';

import {
    teamSelectInfo,
    teamFileUploadRequest,
    teamChangeProfileRequest
 } from '../../actions/team';

 import { fetchUserInfoRequest } from '../../actions/user';

class TeamSetting extends React.Component {

    constructor(props) {
        super(props);

        this.uploadedCover = undefined;

        this.initEvent = this.initEvent.bind(this);
        this.initWidget = this.initWidget.bind(this);

        this.handleSelectTeamImage = this.handleSelectTeamImage.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleModify = this.handleModify.bind(this);
    }

    handleSelectTeamImage(e) {
        e.preventDefault();

        let images = $('#team-setting-sel-img')[0].files;
        if (!images || images.length <= 0) {
            return;
        }

        modal.showUploadModal('업로드', 0, '');

        this.handleFileUpload(images);
        
        return false;
    }

    handleModify(e) {

        let team = this.props.team.selectTeam.team;
        let $name = $('#team-setting-name').val();
        let $desc = $('#team-setting-desc').val();

        if (!this.uploadedCover && !$name.length && !$desc.length) {
            alert.warning('#ucalert', '팀 프로필 수정', '수정된 정보가 없습니다.');
            return false;
        }

        $name || ($name = team.name); 
        $desc || ($desc = team.description);
        this.uploadedCover || (this.uploadedCover = team.coverImage);
        

        this.props.teamChangeProfileRequest(this.props.auth.data, team._id.toString(), this.uploadedCover, $name, $desc).then(()=> {
            alert.info('#ucalert', '팀 프로필 수정', '수정이 완료되었습니다..');

            if (this.props.team.changeProfile.status === 'SUCCESS'){
                this.props.teamSelectInfo(this.props.auth.data, team._id.toString()).then(()=>{
                    this.props.fetchUserInfoRequest(this.props.auth.data).then(()=>{
                        
                        alert.hide();
                        
                        setTimeout(()=> {
                            this.props.router.replace('/article');
                        }, 500);
                    });
                });
            }
        });

        return false;
    }

    handleFileUpload(files) {

        let file = files[0];
        let progress = ( (percent, source) => {

            if (this.uploadCanccel) {
                source.cancel();
                modal.hideUploadModal();
                return;
            }

            modal.progressUploadModal(file.name, percent, `${percent}% 업로드`);                  

            if (percent >= 100) {
                modal.progressUploadModal(null, 100, `${percent}% 업로드 완료`);
                modal.hideUploadModal();
            }
        });

        this.props.teamFileUploadRequest(this.props.auth.data, files, progress).then(() => {

            if (this.props.team.fileupload.status === 'SUCCESS') {

                this.uploadedCover = this.props.team.fileupload.data;
                delete this.uploadedCover.user;

                let coverImage = this.uploadedCover.path;
                coverImage = coverImage.replace(/public/gi, '');
                coverImage = coverImage + '/' + this.uploadedCover.filename;
                
                $('#team-img-box').css({"background":`url(${coverImage}) center center / cover`, backgroundRepeat: "no-repeat"});         
            }
        });
    }


    initEvent() {
        $('#team-setting-sel-img').on('change', this.handleSelectTeamImage);
    }
    initWidget() {
        let team = this.props.team.selectTeam.team;
        $('#team-setting-name').attr('placeholder', team.name);
        $('#team-setting-desc').attr('placeholder', team.description);
    }

    componentDidMount() {
        this.initEvent();
        this.initWidget();
    }

    render() {

        let team = this.props.team.selectTeam.team;

        let coverImage = team.coverImage.path;
        coverImage = team.coverImage.path.replace(/public/gi, '') + '/' + team.coverImage.filename;

        return(
            <div>
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">팀 정보</h3>
                    </div>
                    
                    <div className="box-body">
                        <div className="box-body box-profile">

                            <div id="team-img-box" className={style.ucTeamProfileImg} 
                                    style={{background: `url("${coverImage}") center center / cover`, backgroundRepeat: "no-repeat"}}>
                            </div>

                            <div className={style.ucTeamImgInputBox} >
                                <label className="btn btn-primary clickable pull-right btn-xs" htmlFor="team-setting-sel-img">이미지 변경</label>
                                <input id="team-setting-sel-img" className={style.ucInputHidden} type="file" accept="image/*"  />
                            </div>
                            
                            <h3 className="profile-username text-center">{team.name}</h3>

                            <p className="text-muted text-center">
                                {team.description}
                            </p>
                            <br />

                            <ul className="list-group list-group-unbordered">
                                <li className="list-group-item">
                                <b>멤버</b> <a className="pull-right">{team.members.length}</a>
                                </li>
                                <li className="list-group-item">
                                <b>리더</b> <a className="pull-right">{team.operator.username}</a>
                                </li>
                            </ul>
                        </div>

                        <div>
                            <div className="form-group">
                                <input type="text" className="form-control" id="team-setting-name" placeholder="팀 이름" />
                            </div>                      

                            <div className="form-group">
                                <textarea className="form-control" id="team-setting-desc" placeholder="팀 설명"></textarea>
                            </div>
                            <a href="#" className="btn btn-primary btn-block" onClick={this.handleModify}><b>수정하기</b></a>
                        </div>
                    </div>
                </div>   
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        team:state.team
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        teamSelectInfo: (userData, teamids) => {
            return dispatch(teamSelectInfo(userData, teamids));
        },
        teamFileUploadRequest: (userData, files, cb) => {
            return dispatch(teamFileUploadRequest(userData, files, cb));
        },
        teamChangeProfileRequest: (userData, teamid, cover, name, desc) => {
            return dispatch(teamChangeProfileRequest(userData, teamid, cover, name, desc));
        },
        fetchUserInfoRequest: (userData) => {
            return dispatch(fetchUserInfoRequest(userData));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamSetting)

