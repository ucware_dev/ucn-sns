import style from './style.css';
import React from 'react';
import { connect } from 'react-redux';
import ucui from '../../utils/ucui';
import MiniEditor from './MiniEditor';
import PostList from './PostList';
import Editor from './Editor';
import VoteEditor from './VoteEditor';

import {
    articlePostRequest,
    articleListRequest,
    newArticleListByIdRequest,
    oldArticleListByIdRequest,
    deleteArticleRequest,
    fileUploadRequest,
    articleUrlMetaRequest,
    articleUpdateRequest,
    articleFindRequest
 } from '../../actions/article';

 import {
    commentPostRequest,
    commentListRequest,
    commentNewListRequest,
    commentOldListRequest,
    deleteCommentRequest
 } from '../../actions/comment';

class Article extends React.Component {

    constructor(props) {
        super(props);

        this.postFetchMonitor = {
            state: false,
            get: function() {
                return this.state;
            },
            set: function() {
                this.state = !this.state;
            } 
        };


        this.emptyPostCount = Array.apply(null, {length: 10}).map(Number.call, Number);

        this.initEvent = this.initEvent.bind(this);
        
        this.getArticles = this.getArticles.bind(this);
        this.getNewArticles = this.getNewArticles.bind(this);
        this.getOldArticles = this.getOldArticles.bind(this);
        this.deleteArticle = this.deleteArticle.bind(this);
        this.postArticle = this.postArticle.bind(this);
        this.editArticle = this.editArticle.bind(this);

        this.postComment = this.postComment.bind(this);
        this.getComments = this.getComments.bind(this);
        this.getNewComments = this.getNewComments.bind(this);
        this.getOldComments = this.getOldComments.bind(this);
        this.deleteComment = this.deleteComment.bind(this);
        this.getUrlMeta = this.getUrlMeta.bind(this);
        this.getArticleWithAttachments = this.getArticleWithAttachments.bind(this);
        this.voteUpdate = this.voteUpdate.bind(this);

        this.showEmptyList = this.showEmptyList.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleArticleParams = this.handleArticleParams.bind(this);
    }

    initEvent() {
        $(document).ready(()=>{
           ucui.infiniteScroll(this, this.getOldArticles);
        });
    }

    showEmptyList(l) {
        return l.map( (v, i) => {
            return (
                <div className="row" key={i}>
                    <div id={this.props.postId} className={"box box-widget " + style.ucLodgingFormSize}>
                        <img src="/images/loadingform.png" />
                    </div>
                </div>
            );
        });
    }

    deleteArticle(articleid) {
        return this.props.deleteArticleRequest(this.props.auth.data, articleid).then(()=> {
            //글 펼쳐보기 상태에서 글삭제하면 삭제된 항목 아래 항목이 자동으로 펼쳐보기가 되는 벅스 있어서 강제로 목록 받는다.
            this.getArticles();
            return this.props.article.list.status === 'SUCCESS';
        });
    }

    postArticle(params) {
        
        let content = params.content;
        let files = params.files;
        let ogs = params.ogs;
        let vote = params.vote;

        return this.props.articlePostRequest(this.props.auth.data, this.props.auth.currentTeam.id, content, files, ogs, vote).then(() => {
            return this.props.article.post.status === 'SUCCESS';
        });
    }

    editArticle(articleid, content, files, vote) {
        return this.props.articleUpdateRequest(this.props.auth.data, articleid, this.props.auth.currentTeam.id, content, files, vote).then(() => {
            return this.props.article.update.status === 'SUCCESS';
        });
    }

    voteUpdate(article, vote) {
        
        let articleid = article._id.toString();
        let copyArticle = JSON.parse(JSON.stringify(article));
        copyArticle.vote = vote;

        return this.props.articleUpdateRequest(this.props.auth.data, articleid, this.props.auth.currentTeam.id, article.content, article.files, vote).then(() => {
            return this.props.article.update.status === 'SUCCESS';
        });
    }

    getArticles() {
        if (this.postFetchMonitor.get()) {
            return;
        }
        this.postFetchMonitor.set();

        this.props.articleListRequest(this.props.auth.data, this.props.auth.currentTeam.id).then(() => {
            if (this.props.article.list.data.length > 0) {
                $('#empty-list').hide();
            }
            this.postFetchMonitor.set();
        });
    }

    getNewArticles() {
        this.getArticles();
        /**
         * cyh3813
         * newArticleListByIdRequest 함수에 버그 있어서
         * 새로 목록 읽어오도록 수정.
         */

        // //빈게시판에 최초 글쓰기.
        // if (this.props.article.list.data.length <= 0) {
        //     this.getArticles();
        //     return;
        // }

        // if (this.postFetchMonitor.get()) {
        //     return;
        // }
        // this.postFetchMonitor.set();
        
        // let latestPost = this.props.article.list.data[0];
        // let teamid = this.props.auth.currentTeam.id;

        // this.props.newArticleListByIdRequest(this.props.auth.data, latestPost._id, teamid).then(() => {
        //     this.postFetchMonitor.set();
        // });
    }

    getOldArticles() {
        if (!this.props.auth.isSignin || !this.props.article.list.data || this.props.article.list.data.length <= 0 || this.props.article.list.isLast) {
            return;
        }
        if (this.postFetchMonitor.get()) {
            return;
        }

        this.postFetchMonitor.set();

        let teamid = this.props.auth.currentTeam.id;
        let listLen = this.props.article.list.data.length;
        let lastPost = this.props.article.list.data[listLen - 1];

        this.props.oldArticleListByIdRequest(this.props.auth.data, lastPost._id, teamid).then(() => {

            this.postFetchMonitor.set();
        })
    }

    getArticleWithAttachments(articleid) {

        if (this.postFetchMonitor.get()) {
            return;
        }
        this.postFetchMonitor.set();

        return this.props.articleFindRequest(this.props.auth.data, articleid).then(()=> {
            if (this.props.article.list.data.length > 0) {
                $('#empty-list').hide();
            }
            this.postFetchMonitor.set();
            return this.props.article.foundArticle.status === 'SUCCESS';
        });
    }

    getUrlMeta(url) {
        return this.props.articleUrlMetaRequest(this.props.auth.data, url).then(()=> {
            return this.props.article.meta.status === 'SUCCESS';
        });
    }

    postComment(params) {
        let articleid = params.articleid;
        let comment = params.comment;
        let imgInfo = params.imgInfo;

        return this.props.commentPostRequest(this.props.auth.data, articleid, 
                                            this.props.auth.currentTeam.id, comment, imgInfo).then(()=> {
            return this.props.comment.post.status === 'SUCCESS';
        });
    }

    getComments(articleid) {
        return this.props.commentListRequest(this.props.auth.data, articleid).then(()=> {
            return this.props.comment.status === 'SUCCESS';
        });
    }

    getNewComments(articleid, commentid) {
        return this.props.commentNewListRequest(this.props.auth.data, articleid, commentid).then(()=> {
            return this.props.comment.status === 'SUCCESS';
        });
    }

    getOldComments(articleid, commentid, count) {
        return this.props.commentOldListRequest(this.props.auth.data, articleid, commentid, count).then(()=> {
            return this.props.comment.status === 'SUCCESS';
        });
    }

    deleteComment(articleid, commentid) {
        return this.props.deleteCommentRequest(this.props.auth.data, articleid, commentid).then(()=> {
            return this.props.comment.delete.status === 'SUCCESS';
        });
    }

    handleFileUpload(files, progress) {
        return this.props.fileUploadRequest(this.props.auth.data, files, progress).then(()=> {
            return this.props.article.fileupload.status === 'SUCCESS';
        });
    }

    handleArticleParams() {
        if (this.props.params.articleid) {
            this.getArticleWithAttachments(this.props.params.articleid).then((success)=> {
            });

            setTimeout(()=> {
                
            }, 100);
        } 
    }

    componentWillReceiveProps(nextProps) {
        
        if (this.props.params.articleid != nextProps.params.articleid) {
            if (nextProps.params.articleid) {
                this.getArticleWithAttachments(nextProps.params.articleid);
                return;
            } 

            if (!nextProps.params.articleid) {
                this.getArticles();        
                return;
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {

        if (!this.props.auth.data.user || !this.props.auth.isSignin) {
            return false;
        }
        return true;
    }

    componentWillMount() {
        if (this.props.auth.isSignin === false) {
            this.props.router.replace('/');
        }
    }

    componentDidMount() {
        
        this.initEvent();

        if (this.props.params.articleid) {
            this.handleArticleParams();    
        } else {
            this.getArticles();        
        }
    }

    render() {

        
        return(
            <div>
                <MiniEditor 
                    user={this.props.auth.data.user}
                />
                <div id="empty-list">
                    { this.showEmptyList(this.emptyPostCount)  }
                </div>
                <PostList 
                    articleList={this.props.article.list.data} 
                    deleteArticle={this.deleteArticle}
                    postComment={this.postComment}
                    postCommentData={this.props.comment.post.data}
                    afterDeleteCommentCount={this.props.comment.delete.commentCount}
                    commentList={this.props.comment.data}
                    getComments={this.getComments}
                    getNewComments={this.getNewComments}
                    getOldComments={this.getOldComments}
                    deleteComment={this.deleteComment}
                    myUserId = {this.props.auth.data.user._id.toString()}
                    voteUpdate={this.voteUpdate}
                    myUserInfo={this.props.auth.data.user}
                    />
                <Editor 
                    uploadedFile={this.props.article.fileupload}
                    fileUpload={this.handleFileUpload}
                    postArticle={this.postArticle} 
                    editArticle={this.editArticle}
                    getNewArticles = {this.getNewArticles}
                    getUrlMeta={this.getUrlMeta}
                    metaData={this.props.article.meta.data}
                    />

                <VoteEditor />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        article: state.article,
        comment: state.comment,
        team: state.team,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        articlePostRequest: (userData, team, content, files, ogs, vote) => {
            return dispatch(articlePostRequest(userData, team, content, files, ogs, vote));
        },

        articleListRequest: (userData, teamid) => {
            return dispatch(articleListRequest(userData, teamid));
        },
        newArticleListByIdRequest: (userData, id, teamid) => {
            return dispatch(newArticleListByIdRequest(userData, id, teamid));
        },
        oldArticleListByIdRequest: (userData, id, teamid) => {
            return dispatch(oldArticleListByIdRequest(userData, id, teamid));
        },
        deleteArticleRequest: (userData, id) => {
            return dispatch(deleteArticleRequest(userData, id));
        },
        articleUpdateRequest: (userData, articleId, teamid, content, files, vote) => {
            return dispatch(articleUpdateRequest(userData, articleId, teamid, content, files, vote));
        },
        fileUploadRequest: (userData, files, cb) => {
            return dispatch(fileUploadRequest(userData, files, cb));
        },
        articleUrlMetaRequest: (userData, url) => {
            return dispatch(articleUrlMetaRequest(userData, url));
        },
        articleFindRequest: (userData, articleid) => {
            return dispatch(articleFindRequest(userData, articleid));
        },
        commentPostRequest: (userData, articleId, teamid, comment, imgInfo) => {
            return dispatch(commentPostRequest(userData, articleId, teamid, comment, imgInfo));
        },
        commentListRequest: (userData, articleId) => {
            return dispatch(commentListRequest(userData, articleId));
        },
        commentNewListRequest: (userData, articleId, commentId) => {
            return dispatch(commentNewListRequest(userData, articleId, commentId));
        },
        commentOldListRequest: (userData, articleId, commentId, count) => {
            return dispatch(commentOldListRequest(userData, articleId, commentId, count));
        },
        deleteCommentRequest: (userData, articleId, commentId) => {
            return dispatch(deleteCommentRequest(userData, articleId, commentId));
        },
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Article)
