import style from './style.css';
import React from 'react';

class MiniEditor extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
    }

    initEvent() {

        $('.alert').slideUp();

        $(`#me-input`).focus(function() {
            $('#modal-editor').trigger('newPost.bs.modal');
            $('#modal-editor').modal('show') ;
            CKEDITOR.instances.ckedt.focus();
        });
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        let userImage = this.props.user.profileImage.path.replace(/public/gi, '');
        userImage  = `${userImage}/${this.props.user.profileImage.thumb}`;


        return(
            <div className="row">
                <div className="box box-widget">
                    <div className="box-footer">
                        <form action="#">
                            <img className="img-responsive img-circle img-sm" src={userImage} />
                            <div className="img-push">
                                <input id="me-input" type="text" className="form-control input-sm" placeholder="멤버들에게 전할 소식을 남겨주세요." />
                            </div>

                            {/*
                            <br />
                            <div className={"box-tools pull-right " + style.ucTooBox}>
                                <button type="button" className="btn btn-box-tool" >
                                    <i className="fa fa-file-image-o"></i>
                                </button>

                                <button type="button" className="btn btn-box-tool" >
                                    <i className="fa fa-file-video-o"></i>
                                </button>

                                <button type="button" className="btn btn-box-tool" >
                                    <i className="fa fa-file-o"></i>
                                </button>

                                <button type="button" className="btn btn-box-tool" >
                                    <i className="fa fa-bar-chart-o"></i>
                                </button>
                            </div>
                            */}

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default MiniEditor
