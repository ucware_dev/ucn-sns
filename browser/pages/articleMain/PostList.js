 import style from './style.css';
import React from 'react';

import Post from './Post';


class PostList extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showPostList = this.showPostList.bind(this);
    }

    initEvent() {
    }

    showPostList(l) {

        if (!l) {
            return;
        }

        return l.map( (v, i) => {
            return (
                <Post 
                    key={i} 
                    post={v} 
                    deletePost={this.props.deleteArticle}
                    postComment={this.props.postComment}
                    myUserId={this.props.myUserId}
                    postCommentData={this.props.postCommentData}
                    afterDeleteCommentCount={this.props.afterDeleteCommentCount}
                    comment={this.props.commentList[v._id.toString()]}
                    getComments={this.props.getComments}
                    getNewComments={this.props.getNewComments}
                    getOldComments={this.props.getOldComments}
                    deleteComment={this.props.deleteComment}
                    voteUpdate={this.props.voteUpdate}
                    myUserInfo={this.props.myUserInfo}
                    />
            );
        });
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return(
            <div>
                {this.showPostList(this.props.articleList)}
            </div>
        );
    }
}

PostList.propTypes = {
    articleList: React.PropTypes.array
};

PostList.defaultProps = {
    articleList: []
};

export default PostList
