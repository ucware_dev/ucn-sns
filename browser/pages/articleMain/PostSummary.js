import style from './style.css';
import React from 'react';
import moment from 'moment';

class PostSummary extends React.Component {

    constructor(props) {
        super(props);

        moment.locale('ko');

        this.isMediaFile = this.isMediaFile.bind(this);
        this.isVideoFile = this.isVideoFile.bind(this);
        this.showMediaSummary = this.showMediaSummary.bind(this);
        this.showFileSummary = this.showFileSummary.bind(this);
        this.showVoteSummary = this.showVoteSummary.bind(this);

        this.parseFiles = this.parseFiles.bind(this);
        this.calSummarySize = this.calSummarySize.bind(this);
        this.mediaFiles = [];
        this.etcFiles = [];
    }

    isMediaFile(mime) {
        return /^(image|video)/.test(mime);
    }

    isVideoFile(mime) {
        return /^video/.test(mime);
    }

    parseFiles(files) {

        this.mediaFiles = [];
        this.etcFiles = [];

        files.forEach((f, i) => {
            if(this.isMediaFile(f.mime)) {
                this.mediaFiles.push(f);
            } else {
                this.etcFiles.push(f);
            }
        });
    }

    calSummarySize(files, vote) {

        this.height = 0;

        let existFiles = (files && files.length > 0);
        if (existFiles) {
            this.parseFiles(files);

            if (this.mediaFiles.length > 0) {
                this.height += 344;
            }
            if (this.etcFiles.length > 0) {
                this.height -= 78;
            }
        }

        if (existFiles && vote) {
            this.height -= 78;
        } else if ((!files || files.length <= 0) && vote) {
            this.height = 80;
        }
    }

    showMediaSummary(mediaFiles) {

        if (mediaFiles.length <= 0) {
            return '';
        }

        let mediaCnt = mediaFiles.length;

        //임시
        if ((this.height <= 188) && (mediaCnt > 3)) {
            mediaCnt = 3;
        }

        const drawMediaSummary = (w, h, file, index) => {

            //임시
            if (index >= mediaCnt) {
                return;
            }

            let src = file.path.replace(/public/gi, '') + '/' + file.thumb;
            if (this.isVideoFile(file.mime)) {
                return (
                    <div className={style.ucSummaryFitImg} key={index} style={{width:`${w}`, height: `${h}`, background: `url(${src}) center center / cover`, backgroundRepeat:"no-repeat"}} >
                        <div className={style.ucSummaryVideoIcon}></div>
                    </div>    
                );
            } else {

                if ( (mediaCnt > 6) &&  (index === 5) ) {
                    return (
                        <div className={style.ucSummaryFitImg} key={this.articleid + '_' + index} style={{width:`${w}`, height: `${h}`, background: `url(${src}) center center / cover`, backgroundRepeat:"no-repeat"}} >
                            <div className={style.ucSummaryMoreImage}>
                                {/* <span>+{mediaCnt - 6}</span> */}
                                <span>더보기</span>
                            </div>
                        </div>    
                    );    
                } else {
                    return (
                        <div className={style.ucSummaryFitImg} key={this.articleid + '_' + index} style={{width:`${w}`, height: `${h}`, background: `url(${src}) center center / cover`, backgroundRepeat:"no-repeat"}} >
                        </div>    
                    );
                }
            } 
        };
        
        if (mediaCnt <= 0) {
            return '';
        }

        let w = '100%', h = '99%';
        if (mediaCnt <= 3) {
            w = `${ parseInt( ((1 / mediaCnt) * 100)) - 1 }%`;            

        } else if (mediaCnt === 4) {
            w = h = '49%';

        } else if (mediaCnt === 5) {
           return mediaFiles.map((f, i) => {
                let src = f.path.replace(/public/gi, '') + '/' + f.thumb;
                w = '49%';
                if (i < 3) {
                    w = '32%';
                    h = '49%';
                } 
                return drawMediaSummary(w, h, f, i);
            });

        } else {
            w = '32%';
            h = '49%';
        }

        return mediaFiles.map((f, i) => {
            return drawMediaSummary(w, h, f, i);
        });
    }

    showFileSummary(files) {
        if (!files || files.length <= 0) {
            return '';
        }
        
        let filename = files[0].filename;
        return (
            <div className="ucEditorPreviewBox" >
                <div className="ucEditorFileBox">                    
                    <div></div>
                    <div>
                        <span>파일 {files.length}개</span><br />
                        <span>{filename}</span>
                    </div>
                </div>
                <div className="ucEditorPreviewDel">삭제</div>
            </div>
        );
    }

    showVoteSummary(vote) {

        if (!vote) {
            return '';
        }

        let totalVoters = 0;
        vote.items.forEach((v, i) => { totalVoters += v.voters.length});

        let finishDate = '';
        if (vote.useFinishDate) {
            finishDate = moment(vote.finishDate).format('YYYY년 MM월 DD일 a h시 mm분');
            finishDate = ` [종료일 : ${finishDate}]`;
        }
        
        return (
            <div id="votePreview">                 
                <div id="voteSummary">                 
                    <div className="votePreviewIcon"></div>
                    <div className="votePreviewDesc">
                        <span>투표</span> <small>{totalVoters}명 참여 {finishDate}</small><br />
                        <span>{vote.title}</span>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        if (!this.props.files && !this.props.vote) {
            return (<div></div>);
        }
                
        this.calSummarySize(this.props.files, this.props.vote);
        if (this.height <= 0) {
            return (<div></div>);
        }

        let heightStyle = {height: `${this.height}px`};
        return (
            <div id="summary" className={style.ucContentSummary} onClick={this.props.clickSummary}>
                <div style={heightStyle}>
                    { this.showMediaSummary(this.mediaFiles) } 
                    { this.showFileSummary(this.etcFiles) }
                    { this.showVoteSummary(this.props.vote) }   
                </div>
            </div>
        );
    }
}

export default PostSummary
