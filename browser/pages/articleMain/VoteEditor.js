import style from './style.css';
import React from 'react';

class VoteEditor extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);        
        this.addItem = this.addItem.bind(this);
        this.closeVoteEidtor = this.closeVoteEidtor.bind(this);
        this.changeOption = this.changeOption.bind(this);
        this.setFinishDate = this.setFinishDate.bind(this);
        this.initForm = this.initForm.bind(this);
        this.editForm = this.editForm.bind(this);
        this.confirm = this.confirm.bind(this);
        this.setCustomCheckBox = this.setCustomCheckBox.bind(this);
        
        this.itemCount = 3;
    }

    initForm() {

        this.itemCount = 3;

        $('#voteEditor input[type=text]').val('');
        /**
         * input 그래픽이 커스텀이라 수동으로 처리.
         */
        $('#secret-vote').prop('checked', false);
        $('#multi-vote').prop('checked', false);
        $('#finish-date-vote').prop('checked', false);
        $('#voteFinishDatePickerForm').hide(); 
        
        $('#voteOption label').addClass('fa-square-o text-muted');
        $('#voteOption label').removeClass('fa-check-square text-primary');
    }

    editForm(vote) {
        this.initForm();

        $('#voteTitle').val(vote.title);

        $('#secret-vote').prop('checked', vote.isSecret);
        this.setCustomCheckBox($('#secret-vote'), vote.isSecret);

        $('#multi-vote').prop('checked', vote.isMultiChoice);
        this.setCustomCheckBox($('#multi-vote'), vote.isMultiChoice);

        $('#finish-date-vote').prop('checked', vote.useFinishDate);
        this.setCustomCheckBox($('#finish-date-vote'), vote.useFinishDate);

        if (vote.useFinishDate) {
            $('#voteFinishDatePickerForm').show();        
            $('#finish-date').val(vote.finishDate);
        }

        vote.items.sort((a, b) => { return  parseInt(a.order) > parseInt(b.order) });

        let $items = $('.vote-item');
        for (let i = 0; i < vote.items.length; i++) {
            $($items[i]).val(vote.items[i].title);
        }
    }

    confirm() {
        let vote = {
            title: null,
            isMultiChoice: false,
            isSecret: true,
            isFinishVote: false,
            useFinishDate: false,
            finishDate: null,
            items: []
        };

        vote.title = $('#voteTitle').val();
        vote.isMultiChoice = $('#multi-vote').is(':checked');
        vote.isSecret = $('#secret-vote').is(':checked');
        vote.useFinishDate = $('#finish-date-vote').is(':checked');
        if (vote.useFinishDate) {
            vote.finishDate = new Date(($('#finish-date').val()));
        }

        $('.vote-item').each((i, v) => {
            if ($(v).val().trim().length > 0) {
                let item = {
                    order: $(v).attr('name').trim(),
                    title: $(v).val().trim(),
                    voters: []
                };
                vote.items.push(item);
            }
        });

        if (vote.title.length <= 0) {
            alert('투표 제목을 입력해 주세요.');
            return;
        }
        if (vote.items.length < 2) {
            alert('투표 항목을 2개 이상 입력해 주세요.');
            return;
        }
        
        $('#modal-editor').trigger('voteAdd.bs.modal', vote);

        this.closeVoteEidtor();
    }

    addItem(e) {

        let number = ++this.itemCount;
        $('#voteAddItems').append(`
            <div class=${style.voteItem}>
                <div><span class=${style.voteCircleSpan}>${number}</span></div>
                <div><input class="vote-item" type="text" placeholder="항목입력" name="${number}"></input></div>
            </div>
        `);

         $("#voteBody").animate({scrollTop: $('#voteBody')[0].scrollHeight - $('#voteBody')[0].clientHeight}, 300);
    }

    setFinishDate(e) {

        let checked = $('#finish-date-vote').is(':checked');
        if (checked) {
            $('#voteFinishDatePickerForm').show();        
        } else {
            $('#voteFinishDatePickerForm').hide(); 
        }

        return false;
    }

    setCustomCheckBox($checkbox, checked) {
        
        if (checked) {
            $checkbox.parent().find(`label`).removeClass('fa-square-o text-muted');
            $checkbox.parent().find(`label`).addClass('fa-check-square text-primary');

        } else {
            $checkbox.parent().find(`label`).addClass('fa-square-o text-muted');
            $checkbox.parent().find(`label`).removeClass('fa-check-square text-primary');
        }
    }

    changeOption(e) {

        let $self = $(e.currentTarget)
        let checked = $self.is(':checked');
        let chkId = $self.attr('id');

        this.setCustomCheckBox($self, checked);
    }

    closeVoteEidtor() {
        $('#modal-vote-editor').modal('hide');
    }

    initEvent() {
        $('#modal-vote-editor').on('shown.bs.modal', (e) => {
            $('.vote-item')[0].focus();
        });
        $('#modal-vote-editor').on('voteEditorInit.bs.modal', (e) => {
            this.initForm();
        });
        $('#modal-vote-editor').on('editVote.bs.modal', (e, vote) => {
            this.editForm(vote);
            $('#modal-vote-editor').modal('show') ;
        });
        
        $(document).ready(function(){
            $('#voteFinishDateTimePicker').datetimepicker();
        });

        $('#voteFinishDatePickerForm').hide();
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return (
            <div className="modal fade" id="modal-vote-editor">
                <div className="modal-dialog">
                    <div className={"modal-content " + style.voteModal}>
                        
                        <div id="voteEditor" className={style.voteEditor}>

                             <div className={style.voteTitle}>
                                <p>투표</p>
                                <div onClick={this.closeVoteEidtor}><i className="fa fa-remove"></i></div>
                            </div>

                            <div className={style.voteHeader}>
                                <input id="voteTitle" type="text" placeholder="투표 제목"></input>
                            </div>

                            <div id="voteBody" className={style.voteBody}>
                                <div className={style.voteItemBox}>

                                    <div className={style.voteItem}>
                                        <div><span className={style.voteCircleSpan}>1</span></div>
                                        <div><input className="vote-item"  type="text" placeholder="항목입력" name="1"></input></div>
                                    </div>

                                    <div className={style.voteItem}>
                                        <div><span className={style.voteCircleSpan}>2</span></div>
                                        <div><input className="vote-item" type="text" placeholder="항목입력" name="2"></input></div>
                                    </div>

                                    <div className={style.voteItem}>
                                        <div><span className={style.voteCircleSpan}>3</span></div>
                                        <div><input className="vote-item" type="text" placeholder="항목입력" name="3"></input></div>
                                    </div>

                                    <div id="voteAddItems">
                                    </div>

                                    <div className={style.voteItem} onClick={this.addItem}>
                                        <div><span className={style.voteCircleSpan}>+</span></div>
                                        <div><span className={style.voteItemAdd} >항목추가</span></div>
                                    </div>

                                    <div id="voteOption" className={style.voteOption}>
                                        <div className={style.voteItem}>
                                            <div>
                                                <label className="fa fa-square-o clickable text-muted" htmlFor="secret-vote"></label>
                                                <input id="secret-vote" className={style.ucInputHidden} type="checkbox" onChange={this.changeOption} />
                                            </div>
                                            <div><span className={style.voteItemAdd} >무기영 투표</span></div>
                                        </div>
                                        <div className={style.voteItem}>
                                            <div>
                                                <label className="fa fa-square-o clickable text-muted" htmlFor="multi-vote"></label>
                                                <input id="multi-vote" className={style.ucInputHidden} type="checkbox" onChange={this.changeOption} />
                                            </div>
                                            <div><span className={style.voteItemAdd} >중복 허용</span></div>
                                        </div>


                                        <div className={style.voteItem}>
                                            <div onClick={this.setFinishDate}>
                                                <label className="fa fa-square-o clickable text-muted" htmlFor="finish-date-vote"></label>
                                                <input id="finish-date-vote" className={style.ucInputHidden} type="checkbox" onChange={this.changeOption} />
                                            </div>
                                            <div><span className={style.voteItemAdd} >종료일 설정</span></div>
                                        </div>

                                        <div id="voteFinishDatePickerForm" className={"form-group " + style.voteFinishDateForm}>
                                            <div className="input-group date" id="voteFinishDateTimePicker">
                                                <input id="finish-date" type='text' className="form-control" />
                                                <span className="input-group-addon">
                                                    <span className="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className={style.voteFooter}>
                                <button className="btn btn-primary" onClick={this.confirm}>첨부하기</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default VoteEditor
