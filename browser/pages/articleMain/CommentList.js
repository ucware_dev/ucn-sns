import style from './style.css';
import React from 'react';

import Comment from './Comment';


class CommentList extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showList = this.showList.bind(this);
    }

    initEvent() {
    }

    showList(l) {

        if (!l) {
            return;
        }

        return l.map( (v, i) => {
            return (
                <Comment 
                    key={i} 
                    comment={v} 
                    myUserId={this.props.myUserId}
                    deleteCommentWithComentId={this.props.deleteCommentWithComentId}
                    parseDate={this.props.parseDate}
                    />
            );
        });
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return(
            <div>
                {this.showList(this.props.comments)} 
            </div>
        );
    }
}

CommentList.propTypes = {
    comments: React.PropTypes.array
};

CommentList.defaultProps = {
    comments: []
};

export default CommentList
