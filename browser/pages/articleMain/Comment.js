import style from './style.css';
import React from 'react';

class Comment extends React.Component {

    constructor(props) {
        super(props);

        this.now = Date.now();

        this.initEvent = this.initEvent.bind(this);
        this.deleteComment = this.deleteComment.bind(this);
    }

    initEvent() {
    }

    deleteComment() {
        this.props.deleteCommentWithComentId(this.props.comment._id);
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {

        let delButton = (
            <button type="button" className="btn btn-box-tool" onClick={this.deleteComment}>
                <i className="fa fa-times"></i>
            </button>
        );
        let isMyComment = this.props.myUserId === this.props.comment.author._id.toString();
        
        let userImage = this.props.comment.author.profileImage.path.replace(/public/gi, '');
        userImage  = `${userImage}/${this.props.comment.author.profileImage.thumb}`;

        return(
            <div className="box-comment">
                <img className="img-circle img-sm" src={userImage} />
                <div className="comment-text">
                    <span className="username">
                        {this.props.comment.author.username}
                        <span className="text-muted pull-right">{this.props.parseDate(this.now, this.props.comment.date)}</span>
                        {isMyComment && delButton}
                    </span>
                    <p>
                    {this.props.comment.content}
                    </p>
                </div>
            </div>
        );
    }
}

Comment.propTypes = {
    comment: React.PropTypes.object,
    myUserId: React.PropTypes.string,
};

Comment.defaultProps = {
    comment: null,
    myUserId: null,
};

export default Comment
