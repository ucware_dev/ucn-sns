import style from './style.css';
import React from 'react';
import { Link } from 'react-router';

class FileList extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
        this.showFileList = this.showFileList.bind(this);
    }

    showFileList(list) {
        return list.map((f, i) => {

            return (
                <li key={i}>
                    <p className={style.ucFileName}>
                        {f.filename}}
                        <span data-toggle="tooltip" className="badge bg-light-grey pull-right clickable">
                            <Link to={'/article/' + f.article} style={{color:"white"}}>
                                <small>원글보기</small>
                            </Link>
                        </span>
                    </p>
                </li>
            );
        });
    }

    initEvent() {
    }

    componentDidMount() {
        this.initEvent();

        this.props.getTeamFileList().then((success) => {});
    }

    render() {
        if (!this.props.teamFileList.data || this.props.teamFileList.data.length <= 0) {
            return(<div></div>);
        }

        return(            
            <div className="box box-primary">
                <div className="box-header with-border">
                    
                    <h3 className="box-title">파일</h3>
                    
                    <div className="box-tools pull-right">
                        <button type="button" className="btn btn-box-tool">
                            <Link to="/article/teamfiles"><small className="label bg-blue">더보기</small></Link>
                        </button>
                        <button type="button" className="btn btn-box-tool" data-widget="collapse">
                            <i className="fa fa-minus"></i>
                        </button>
                    </div>
                </div>

                <div className="box-body">
                    <ul className="nav nav-stacked">
                       { this.showFileList(this.props.teamFileList.data) }
                    </ul>
                </div>

            </div>
        );
    }
}
export default FileList
