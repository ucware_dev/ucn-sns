import React from 'react';

class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.initEvent = this.initEvent.bind(this);
    }

    initEvent() {
    }

    componentDidMount() {
        this.initEvent();
    }

    render() {
        return(            
            <div className="box box-primary direct-chat direct-chat-primary">
                
                <div className="box-header with-border">
                    <h3 className="box-title">채팅</h3>
                    <div className="box-tools pull-right">
                        <span data-toggle="tooltip" className="badge bg-light-blue">2</span>
                        <button type="button" className="btn btn-box-tool" data-widget="collapse">
                            <i className="fa fa-minus"></i>
                        </button>                        
                    </div>
                </div>

                <div className="box-body">
                    <div className="direct-chat-messages">
                        
                        <div className="direct-chat-msg">
                            <div className="direct-chat-info clearfix">
                                <span className="direct-chat-name pull-left">알렉스 김</span>
                                <span className="direct-chat-timestamp pull-right">1월 23일 2:00 오후</span>
                            </div>
                        
                            <img className="direct-chat-img" src="/pkgs/dist/img/user1-128x128.jpg" />
                            <div className="direct-chat-text">
                                <p>Is this template really for free? That's unbelievable!</p>
                            </div>
                        
                        </div>
                                                
                        <div className="direct-chat-msg right">
                            <div className="direct-chat-info clearfix">
                                <span className="direct-chat-name pull-right">사라 박</span>
                                <span className="direct-chat-timestamp pull-left">1월 23일 2:05 오후</span>
                            </div>
                        
                            <img className="direct-chat-img" src="/pkgs/dist/img/user3-128x128.jpg" />
                            <div className="direct-chat-text">
                                <p>안녕하세요</p>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div className="box-footer">
                    <form>
                        <div className="input-group">
                            <input type="text" name="message" placeholder="" className="form-control" />
                            <span className="input-group-btn">
                                <button type="submit" className="btn btn-primary btn-flat">보내기</button>
                            </span>
                        </div>
                    </form>
                </div>

            </div>
        );
    }
}
export default Chat
