import style from './style.css';
import React from 'react';
import moment from 'moment';

class PostContent extends React.Component {

    constructor(props) {
        super(props);

        moment.locale('ko');

        this.initEvent = this.initEvent.bind(this);
        this.showPostList = this.showPostList.bind(this);
        this.areadyVoted = this.areadyVoted.bind(this);

        this.totalVoters = 0;
        this.maxProgress = 0;
        this.selectedOrder;
        this.isVoted = false;
    }

    initEvent() {

        $(`#${this.contentDivId} input[name=${this.rdoname}]`).off('change');
        $(`#${this.contentDivId} input[name=${this.rdoname}]`).on('change', (e) => {

            let order = $(e.currentTarget).data('order');
            let voters = $(e.currentTarget).data('voters');

            let $parent = $($(e.currentTarget).parent()).parent();
            let $progress = $parent.find('.progress-bar');

            let tmp = JSON.parse(JSON.stringify(this.props.vote));
            tmp.items.forEach((v, i) => {
                if (v.order == order) {
                    v.voters.push(this.props.myUserInfo._id.toString());
                }
            });

            this.selectedOrder = order;
            this.showVote(tmp)
            
            return false;
        });       

        $(`#${this.contentDivId} #vote-finish`).off('click');
        $(`#${this.contentDivId} #vote-finish`).on('click', (e) => {
            let copyVote = JSON.parse(JSON.stringify(this.props.vote));
            copyVote.isFinishVote = true;

            this.props.voteUpdate(copyVote).then((success) => {
                $(`#${this.contentDivId}`).find('#vote-finish').hide();
                $(`#${this.contentDivId}`).find(`.${style.ucVoteTackFooter}`).append('<div>종료된 투표입니다.</div>');
                $(`#${this.contentDivId}`).find(`input[name=${this.rdoname}]`).prop('disabled', true);
                
            });
            return false;
        });

        $(`#${this.contentDivId} #vote-complete`).off('click');
        $(`#${this.contentDivId} #vote-complete`).on('click', (e) => {

            if (!this.selectedOrder) {
                return false;
            }

            let copyVote = JSON.parse(JSON.stringify(this.props.vote));
            copyVote.items.forEach((v, i) => {

                if (v.order == this.selectedOrder) {
                    v.voters.push(this.props.myUserInfo._id.toString());
                }
            });

            this.props.voteUpdate(copyVote).then((success) => {
                $(`#${this.contentDivId}`).find('#vote-complete').hide();
                $(`#${this.contentDivId}`).find(`input[name="${this.rdoname}"]`).prop('disabled', true);
                if (!this.props.isMyPost) {
                    $(`#${this.contentDivId}`).find(`.${style.ucVoteTackFooter}`).append('<div>투표 진행중 입니다.</div>');
                }
            });
            return false;
        });
    }

    areadyVoted() {        
        let myUserId = this.props.myUserInfo._id.toString();
        this.props.vote.items.forEach((v, i) => {
            v.voters.forEach((vv, j) => {
                if (vv._id) {
                    if (myUserId === vv._id.toString()) {
                        this.isVoted = true;
                        return;
                    }
                } else {
                    if (myUserId === vv) {
                        this.isVoted = true;
                        return;
                    }
                }
            });
        });
    }

    showVote(vote) {
        let $voteBody = $(`#${this.contentDivId} #votePreview`);
        if (!$voteBody || $voteBody.length <= 0){
            return;
        }

        this.totalVoters = 0;
        this.maxProgress = 0;

        //Draw Header
        $voteBody.html('');
        vote.items.forEach((v, i) => { 
            if (this.maxProgress < v.voters.length) {
                this.maxProgress = v.voters.length;
            }
            this.totalVoters += v.voters.length
        });
        
        let btnFinish = '<button id="vote-finish" class="btn btn-primary">투표 종료</button>';
        if (!this.props.isMyPost) {
            btnFinish = '';
        }
        
        let btnVoteDone = '<button id="vote-complete" class="btn btn-success">투표 하기</button>';
        if (this.isVoted) {
            btnVoteDone = '';
        }

        if (this.props.vote.isFinishVote || (vote.useFinishDate && moment(vote.finishDate).diff() < 0)) {
            btnFinish = '<div>종료된 투표입니다.</div>';
        } else {
            if (!this.props.isMyPost && this.isVoted) {
                btnFinish = '<div>투표 진행중 입니다.</div>';
            }
        }

        let finishDate = '';
        if (vote.useFinishDate) {
            finishDate = moment(vote.finishDate).format('YYYY년 MM월 DD일 a h시 mm분');
            finishDate = ` [종료일 : ${finishDate}]`;
        }

        $voteBody.html(`
            <div id="voteBody">
                <div class=${style.voteSummary}>                 
                    <div class="votePreviewIcon"></div>
                    <div class="votePreviewDesc">
                        <span>투표</span> <small> ${this.totalVoters}명 참여 ${finishDate}</small><br />
                        <span>${vote.title}</span>
                    </div>
                </div>
                <div id="vote-items">
                </div>
                <div class=${style.ucVoteTackFooter}>
                    ${btnFinish}
                    ${btnVoteDone}
                </div>
            </div>
        `);
 

        //Draw Item
        let $voteItems = $(`#${this.contentDivId} #votePreview #vote-items`);
        vote.items.sort((a, b) => { return  parseInt(a.order) > parseInt(b.order) });
        this.rdoname = `${this.props.articleid}-poll`;
        vote.items.forEach((v, i) => {
            let voters = v.voters.length;
            let percent = (voters / this.maxProgress) * 100;
            if (percent > 100) {
                percent = 100;
            }
            
            let input = `
                <input type="radio" name=${this.rdoname} data-order=${v.order} data-voters=${v.voters.length} 
            `;
            if (v.order == this.selectedOrder) {
                input = `
                    <input type="radio" name=${this.rdoname} data-order=${v.order} data-voters=${v.voters.length} checked 
                `;
            }
            if (this.isVoted || this.props.vote.isFinishVote) {
                input += ' disabled >';
            } else {
                input += ' >';
            }

            $voteItems.append(`
                <div class=${style.ucVoteTackItem}>
                    <div class=${style.ucVoteTackItemSelect}>
                        ${input}
                    </div>
                    <div class=${style.ucVoteTackItemProgress}>
                        <div class=progress-group">
                            <span class="progress-text">${v.title}</span>
                            <span class="progress-number pull-right"><b>${voters}</b></span>
                            <div class="progress sm">
                                <div class="progress-bar progress-bar-green" style="width: ${percent}%"></div>
                            </div>
                        </div>  
                    </div>
                </div>
            `);
        });

        this.initEvent();
    }

    showPostList(l) {
        return l.map( (v, i) => {
            return (
                <Post key={i} post={v} />
            );
        });
    }

    componentDidMount() {
        if (this.props.vote && this.props.articleid) {
            this.areadyVoted();
            this.showVote(this.props.vote);
        }
    }

    render() {
        this.contentDivId = `content_${this.props.articleid}`;
        return(
            <div id={this.contentDivId} className={style.ucPostContent} dangerouslySetInnerHTML={{__html: this.props.content}}>
            </div>
        );
    }
}

PostContent.propTypes = {
    content: React.PropTypes.string
};

PostContent.defaultProps = {
    content: null
};

export default PostContent
