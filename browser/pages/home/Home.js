import React from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import Footer from './Footer';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.handleSignout = this.handleSignout.bind(this);
    }


    handleSignout() {

        let token = this.props.token;
        let email = this.props.auth.data.user.email;
                    
        return this.props.signOutRequest(email, token).then(() => {
                return true;
            }
        );
    }

    componentDidMount() {
        $.AdminLTE.layout.fix();
        $('.alert').slideUp();
    }

    render() {

        return(
            <div className="wrapper">
                <Header />
                <div className="content-wrapper">
                    <div className="container">
                        <br />
                        <div id="ucalert"></div>
                        { this.props.children}
                        <div id="upload-modal"></div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

Home.propTypes = {
};

Home.defaultProps = {
    auth : null
};


export default connect(mapStateToProps, mapDispatchToProps)(Home)
