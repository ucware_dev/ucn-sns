import React from 'react';

class UserProfile extends React.Component {

    constructor(props) {
        super(props);
        
        this.initEvent = this.initEvent.bind(this);
    }

    initEvent() {
        $('#btn-signout').on('click', this.props.onSignout);
    }
    
    componentDidMount() {
        this.initEvent();
    }
    
    render() {

        let profileImage = this.props.userData.profileImage.path.replace(/public/gi, '');
        profileImage  = `${profileImage}/${this.props.userData.profileImage.thumb}`;    

        return(            
            <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <img src={profileImage} className="user-image"  />
                    <span className="hidden-xs">{this.props.userData.username}</span>
                </a>

                <ul className="dropdown-menu">
                    <li className="user-header">
                        <img src={profileImage} className="img-circle" alt="User Image" />
                        <p>
                        {this.props.userData.username}
                        <small>{this.props.userData.email}</small>
                        </p>
                    </li>
                    
                    <li className="user-footer">
                        <div className="pull-left">
                            <a href="/profile" className="btn btn-default btn-flat">프로필</a>
                        </div>
                        <div className="pull-right">
                            <a id="btn-signout" className="btn btn-default btn-flat">로그아웃</a>
                        </div>
                    </li>
                </ul>
            </li>
        );
    }
}

UserProfile.propTypes = {
    userData: React.PropTypes.object,
};

UserProfile.defaultProps = {
    userData: null
};


export default UserProfile
