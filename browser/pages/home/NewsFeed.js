import React from 'react';

class NewsFeed extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <li className="dropdown notifications-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-bell-o"></i>
                    <span className="label label-warning">4</span>
                </a>

                <ul className="dropdown-menu">
                    <li className="header">10개의 알림이 있습니다.</li>
                    <li>
                        <ul className="menu">
                            <li>
                                <a href="#">
                                    <i className="ion ion-ios-people info"></i> 새로운글 피드
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li className="footer"><a href="#">모두보기</a></li>
                </ul>
            </li>           
        );
    }
}

NewsFeed.propTypes = {
};

NewsFeed.defaultProps = {
    auth : null
};


export default NewsFeed
