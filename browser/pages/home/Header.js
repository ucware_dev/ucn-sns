import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import NewsFeed from './NewsFeed';
import UserProfile from './UserProfile';

import { unselectTeamRequest, signOutRequest } from '../../actions/auth';
import { teamSearchRequest, teamListRequest} from '../../actions/team';
import { articleListRequest, articleSearchRequest } from '../../actions/article';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.searchMode = undefined;

        this.initEvent = this.initEvent.bind(this);
        this.handleSignout = this.handleSignout.bind(this);
        this.search = this.search.bind(this);
        this.teamSearch = this.teamSearch.bind(this);
        this.articleSearch = this.articleSearch.bind(this);
    }
    
    componentWillReceiveProps(nextProps) {

        this.searchMode = nextProps.auth.searchbar;
        if (!this.searchMode) {
            $('#navbar-search-input').val('');
            $('#search-bar').addClass('hidden');

        } else if (this.searchMode === 'team') {
            $('#navbar-search-input').attr('placeholder', '팀 검색');
            $('#search-bar').removeClass('hidden');

        } else if (this.searchMode === 'article') {
            $('#navbar-search-input').val('');
            $('#navbar-search-input').attr('placeholder', '내용 검색');
            $('#search-bar').removeClass('hidden');

            this.articleSearch('');

        } else if (this.searchMode === 'clear') {
            $('#navbar-search-input').attr('placeholder', '검색');
            $('#navbar-search-input').val('');
        }
    }

    search(e) {

        if (e.keyCode !== 13) {
            return;
        }

        e.preventDefault();
        
        let keyword = $(e.currentTarget).val().trim();

        if (this.props.auth.searchbar === 'team') {
            this.teamSearch(keyword);

        } else if (this.props.auth.searchbar === 'article') {
            this.articleSearch(keyword);
        } 
    }

    teamSearch(keyword) {

        let authData = this.props.auth.data;

        if (keyword.length > 0) {
            this.props.teamSearchRequest(authData, keyword, keyword).then(()=> { });
        } else {
            this.props.teamListRequest(authData, null).then(()=> {});
        }
    }

    articleSearch(keyword) {

        if (!this.props.auth.data) {
            return;
        }

        let authData = this.props.auth.data;
        let teamid = this.props.auth.currentTeam.id;
        
        if (keyword.length > 0) {
            this.props.articleSearchRequest(authData, teamid, keyword).then(()=> { });
        } else {
            this.props.articleListRequest(authData, teamid).then(()=>{});
        }
    }
    
    initEvent() {
        //cyh3813
        //네비게이션 메뉴 클릭시 자동 숨김.(모바일에서만 동작.)
        $('.navbar-nav').on('click', function(){ 
            if($('.navbar-header .navbar-toggle').css('display') !='none'){
                $(".navbar-header .navbar-toggle").trigger( "click" );
            }
        });
        $('#navbar-search-input').on('keydown', this.search);
    }

    handleSignout() {
        
        let token = this.props.auth.data.token;
        let email = this.props.auth.data.user.email;

        return this.props.signOutRequest(email, token).then(() => {
                this.props.unselectTeamRequest();
                browserHistory.push('/');
            }
        );
    }
    
    componentDidMount() {
        this.initEvent();
    }

    render() {

        let searchBar = (
            <form className="navbar-form navbar-left" role="search">
                <div className="form-group">
                    <input type="text" className="form-control" id="navbar-search-input" placeholder="검색" />
                </div>
            </form>
        )

        let signoutMenu = (
             <ul id="sign-menu" className="nav navbar-nav navbar-right">
                <li><Link to="signup" className="text-center">회원가입</Link> </li>
                <li><Link to="signin" className="text-center">로그인</Link> </li>
            </ul>
        );

        let siginMenu = undefined;
        if (this.props.auth.isSignin) {
            siginMenu = (
                <div className="navbar-custom-menu">
                    <ul className="nav navbar-nav">
                        {/*<NewsFeed />*/}
                        <UserProfile 
                            onSignout={this.handleSignout} 
                            userData={this.props.auth.data.user}
                            />
                    </ul>
                </div>
            );
        }
        
        return(
            <header className="main-header">
                <nav className="navbar navbar-static-top">

                    <div className="container">
                        <div className="navbar-header">
                            <Link to="/" className="navbar-brand"><b>UC</b>SNS</Link>
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i className="fa fa-bars"></i>
                            </button>
                        </div>

                        <div className="collapse navbar-collapse" id="navbar-collapse">

                            <form id="search-bar" className="navbar-form navbar-left hidden" role="search">
                                <div className="form-group">
                                    <input type="text" className="form-control" id="navbar-search-input" placeholder="검색" />
                                </div>
                            </form>
                            
                            { this.props.auth.isSignin ? siginMenu : signoutMenu }

                        </div>

                    </div>   
                </nav>
            </header>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOutRequest: (email, token) => {
            return dispatch(signOutRequest(email, token));
        },
        unselectTeamRequest: () => {
            return dispatch(unselectTeamRequest());
        },
        teamListRequest: (userData, teamid) => {
            return dispatch(teamListRequest(userData, teamid));
        },
        teamSearchRequest: (userData, opname, keyword) => {
            return dispatch(teamSearchRequest(userData, opname, keyword));
        },
        articleListRequest: (userData, teamid) => {
            return dispatch(articleListRequest(userData, teamid));
        },
        articleSearchRequest: (userData, teamid, keyword) => {
            return dispatch(articleSearchRequest(userData, teamid, keyword));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header)
