import React from 'react';
import { connect } from 'react-redux';

class Footer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <footer className="main-footer">
                <div className="container">
                    <div className="pull-right hidden-xs">
                        <b>Version</b> 0.0.1
                    </div>
                    <strong>Copyright &copy; 2016-2017 <a href="http://ucware.co.kr">UCWare</a>.</strong> All rights
                    reserved.
                </div>
            </footer>
        );
    }
}

export default Footer
