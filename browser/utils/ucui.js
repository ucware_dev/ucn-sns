const ucui = {};

ucui.infiniteScroll = function(self, run) {

	$(window).scroll(function() {
		if ($(window).scrollTop() == $(document).height() - $(window).height()) { 
			run.apply(self);
		}
	});
}

export default ucui
