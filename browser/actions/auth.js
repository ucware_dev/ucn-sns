import {
    AUTH_SIGNUP,
    AUTH_SIGNUP_SUCCESS,
    AUTH_SIGNUP_FAILURE,
    AUTH_SIGNIN,
    AUTH_SIGNIN_SUCCESS,
    AUTH_SIGNIN_FAILURE,
    AUTH_SIGNOUT,
    AUTH_SELECT_TEAM,
    AUTH_SELECT_TEAM_SUCCESS,
    AUTH_UNSELECT_TEAM,
    AUTH_UNSELECT_TEAM_SUCCESS,
    AUTH_TEAM_ADD,
    AUTH_TEAM_ADD_SUCCESS,
    AUTH_TEAM_ADD_FAILURE,
    AUTH_SEARCH_BAR } from './actionTypes';

import { articleListClear } from './article';
import axios from 'axios';

const API_SIGN_UP_POST = '/api/v1/account/signup';
const API_SIGN_IN_POST = '/api/v1/account/signin';
const API_SIGN_OUT_POST = '/api/v1/account/signout';
const API_TEAM_ADD_POST = '/api/v1/account/addteam';

//특정 IE 버전에서 get 명령이 서버로 안나간다. 모든 ajax는 post 로 처리.

export function signUpRequest(email, password, username) {

    return (dispatch) => {

        dispatch(signup());

        return axios.post(API_SIGN_UP_POST, { email:email, passwd:password, username: username })
        .then((json) => {
            if (json.data.status == 'success') {
                dispatch(signupSuccess());
            } else {
                dispatch(signinFailure());
            }
        }).catch((error) => {
            dispatch(signinFailure());
        });
    }
}

export function signInRequest(email, password) {
    return (dispatch) => {

        dispatch(signin());
        dispatch(articleListClear());

        return axios.post(API_SIGN_IN_POST, { email:email, passwd:password }).then((json) => {
            if (json.data.status == 'success') {
                dispatch(signinSuccess(json.data.data));
            } else {
                dispatch(signupFailure());
            }
        }).catch((error) => {
            dispatch(signupFailure());
        });
    }
}

export function signOutRequest(email, token) {
    return (dispatch) => {

        dispatch(articleListClear());

        var config = {
          headers: {'authorization': 'bearer ' + token}
        };

        return axios.post(API_SIGN_OUT_POST, {email: email}, config)
        .then((json) => {
            dispatch(signinout());
        }).catch((error) => {
            dispatch(signinout());
        });
    }
}


export function teamAddToUserRequest(userId, team) {

    return (dispatch) => {

        dispatch(teamAdd());

        var config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };
        let data = {
            userid: userId,
            team: JSON.stringify(team)
        };

        return axios.post(API_TEAM_ADD_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(teamAddSuccess(team));
            } else {
                dispatch(teamAddFailure());
            }
        }).catch((error) => {
            dispatch(teamAddFailure());
        });
    }
}


export function selectTeamRequest(team) {
    return (dispatch) => {
        dispatch(selectTeam());
        dispatch(selectTeamSuccess(team));
    }
}

export function unselectTeamRequest() {
    return (dispatch) => {

        dispatch(unselectTeam());
        dispatch(unselectTeamSuccess());
    }
}

export function searchBarRequest(type) {
    return (dispatch) => {
        dispatch(searchBar(type));
    }
}


function signup() {
    return {
        type: AUTH_SIGNUP
    };
}

function signupSuccess(data) {
    return {
        type: AUTH_SIGNUP_SUCCESS,
    };
}

function signupFailure() {
    return {
        type: AUTH_SIGNUP_FAILURE
    };
}

function signin() {
    return {
        type: AUTH_SIGNIN
    };
}

function signinSuccess(data) {
    return {
        type: AUTH_SIGNIN_SUCCESS,
        data: data
    };
}

function signinFailure() {
    return {
        type: AUTH_SIGNIN_FAILURE
    };
}

function signinout() {
    return {
        type: AUTH_SIGNOUT
    };
}

function selectTeam() {
    return {
        type: AUTH_SELECT_TEAM
    };
}

function selectTeamSuccess(team) {
    return {
        type: AUTH_SELECT_TEAM_SUCCESS,
        data: team, 
    };
}

function unselectTeam() {
    return {
        type: AUTH_UNSELECT_TEAM
    };
}

function unselectTeamSuccess() {
    return {
        type: AUTH_UNSELECT_TEAM_SUCCESS
    };
}

function teamAdd() {
    return {
        type: AUTH_TEAM_ADD
    };
}

export function teamAddSuccess(team) {
    return {
        type: AUTH_TEAM_ADD_SUCCESS,
        data: team
    };
}

function teamAddFailure() {
    return {
        type: AUTH_TEAM_ADD_FAILURE
    };
}

function searchBar(type) {
    //team, article, null
    return {
        type: AUTH_SEARCH_BAR,
        data: type
    };
}
