import {
    USER_INFO,
    USER_INFO_SUCCESS,
    USER_INFO_FAILURE,
    USER_CHANGE_PROFILE_IMG,
    USER_CHANGE_PROFILE_IMG_SUCCESS,
    USER_CHANGE_PROFILE_IMG_FAILURE,
    USER_FILEUPLOAD,
    USER_FILEUPLOAD_SUCCESS,
    USER_FILEUPLOAD_FAILURE,
    USER_CHANGE_INFO,
    USER_CHANGE_INFO_SUCCESS,
    USER_CHANGE_INFO_FAILURE
 } from './actionTypes';
    
import axios from 'axios';

const API_USER_INFO_GET = '/api/v1/user/userinfo';
const API_USER_INFO_UPDATE = '/api/v1/user/update';
const API_FILEUPLOAD_POST = '/api/v1/article/fileupload';
const API_USER_PROFILE_IMG_CHANGE = '/api/v1/user/profileimage';


//특정 IE 버전에서 get 명령이 서버로 안나간다. 모든 ajax는 post 로 처리.

//fetchUserInfoRequest 의 결과는 state.auth에 저장되기 때문에 auth 리듀스에서 처리한다.(일단은..)
export function fetchUserInfoRequest(userData) {
    
    return (dispatch) => {

        dispatch(userInfo());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };
        
        let userid = userData.user._id.toString();
        
        return axios.post(API_USER_INFO_GET, {userid: userid}, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(userInfoSuccess(json.data.data));
            } else {
                dispatch(userInfoFailure());    
            }
        }).catch((error) => {
            dispatch(userInfoFailure());
        });
    }
}

export function userFileUploadRequest(userData, files, cb) {

    return (dispatch) => {
        
        dispatch(userFileUpload());
        
        let CancelToken = axios.CancelToken;
        let source = CancelToken.source();

        let config = {
            headers: {'authorization': 'bearer ' + userData.token},  
            
            onUploadProgress: (progressEvent) => {
              let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
              cb && cb(percentCompleted, source);
            },
            cancelToken: source.token
        };


        let form = new FormData();
        form.append('author', JSON.stringify(userData.user));
        if (files) {
            for (let i = 0; i < files.length; i++) {
                form.append('files', files[i]);
            }
        } else {
            dispatch(userFileUploadFailure());
        }

        return axios.post(API_FILEUPLOAD_POST, form, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(userFileUploadSuccess(json.data.data[0]));
            } else {
                dispatch(userFileUploadFailure());
            }
        }).catch((error) => {
            if (axios.isCancel(error)) {
                console.log('## CANCEL UPLOAD');
            }
            dispatch(userFileUploadFailure());
        });
    };
}

export function userChangeProfileImgRequest(userData, imgInfo) {
    
     return (dispatch) => {
        
         dispatch(userChangeProfileImg());

         let config = { headers: { 'authorization': 'bearer ' + userData.token } };
         let data = {
             userid: userData.user._id.toString(),
             path: null,
             filename: null,
             thumb: null
         };
         if (imgInfo) {
                data = {
                userid: userData.user._id.toString(),
                path: imgInfo.path,
                filename: imgInfo.filename,
                thumb: imgInfo.thumb
            };
         }
         return axios.post(API_USER_PROFILE_IMG_CHANGE, data, config).then((json) => {
             if (json.data.status === 'success') {
                 dispatch(userChangeProfileImgSuccess());
             } else {
                 dispatch(userChangeProfileImgFailure());
             }
         }).catch((error) => {
             if (axios.isCancel(error)) {
                 console.log('## CANCEL UPLOAD');
             }
             dispatch(userChangeProfileImgFailure());
         });
     }
}

export function userChangeInfoRequest(userData, username) {
    
    return (dispatch) => {
        
         dispatch(userInfoChange());

         let config = { headers: { 'authorization': 'bearer ' + userData.token } };
         let data = {
             userid: userData.user._id.toString(),
             username: username
         };

         return axios.post(API_USER_INFO_UPDATE, data, config).then((json) => {
             if (json.data.status === 'success') {
                 dispatch(userInfoChangeSuccess());
             } else {
                 dispatch(userInfoChangeFailure());
             }
         }).catch((error) => {
             dispatch(userInfoChangeFailure());
         });
     }
}

function userInfo() {
    return {
        type: USER_INFO
    };
}

function userInfoSuccess(userInfo) {
    return {
        type: USER_INFO_SUCCESS,
        data: userInfo
    };
}

function userInfoFailure() {
    return {
        type: USER_INFO_FAILURE
    };
}

function userChangeProfileImg() {
    return {
        type: USER_CHANGE_PROFILE_IMG
    };
}

function userChangeProfileImgSuccess() {
    return {
        type: USER_CHANGE_PROFILE_IMG_SUCCESS
    };
}

function userChangeProfileImgFailure() {
    return {
        type: USER_CHANGE_PROFILE_IMG_FAILURE
    };
}

function userFileUpload() {
    return {
        type: USER_FILEUPLOAD
    };
}

function userFileUploadSuccess(file) {
    return {
        type: USER_FILEUPLOAD_SUCCESS,
        data: file
    };
}

function userFileUploadFailure() {
    return {
        type: USER_FILEUPLOAD_FAILURE
    };
}

function userInfoChange() {
    return {
        type: USER_CHANGE_INFO
    };
}

function userInfoChangeSuccess() {
    return {
        type: USER_CHANGE_INFO_SUCCESS
    };
}

function userInfoChangeFailure() {
    return {
        type: USER_CHANGE_INFO_FAILURE
    };
}
