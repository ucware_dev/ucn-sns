import {
    TEAM_CREATE,
    TEAM_CREATE_SUCCESS,
    TEAM_CREATE_FAILURE,
    TEAM_LIST,
    TEAM_LIST_SUCCESS,
    TEAM_LIST_FAILURE,
    TEAM_LIST_LIMIT,
    TEAM_JOIN_REQ,
    TEAM_JOIN_REQ_SUCCESS,
    TEAM_JOIN_REQ_FAILURE,
    TEAM_FIND_TEAMS,
    TEAM_FIND_TEAMS_SUCCESS,
    TEAM_FIND_TEAMS_FAILURE,
    TEAM_ADMISSION_USER,
    TEAM_ADMISSION_USER_SUCCESS,
    TEAM_ADMISSION_USER_FAILURE,
    TEAM_SEARCH,
    TEAM_SEARCH_SUCCESS,
    TEAM_SEARCH_FAILURE,
    TEAM_SELECT_INFO,
    TEAM_SELECT_INFO_SUCCESS,
    TEAM_SELECT_INFO_FAILURE,
    TEAM_FILE_LIST,
    TEAM_FILE_LIST_SUCCESS,
    TEAM_FILE_LIST_FAILURE,
    TEAM_FILEUPLOAD,
    TEAM_FILEUPLOAD_SUCCESS,
    TEAM_FILEUPLOAD_FAILURE,
    TEAM_CHANGE_PROFILE,
    TEAM_CHANGE_PROFILE_SUCCESS,
    TEAM_CHANGE_PROFILE_FAILURE } from './actionTypes';

import axios from 'axios';
import FormData from 'form-data';
// import { teamAddSuccess } from './auth';

const API_TEAM_CREATE_POST = '/api/v1/team/create';
const API_TEAM_LIST_POST = '/api/v1/team/list';
const API_TEAM_INFO_POST = '/api/v1/team/info';
const API_TEAM_JOIN_REQ_POST = '/api/v1/team/join';
const API_TEAM_FIND_TEAMS_POST = '/api/v1/team/find';
const API_TEAM_ADMISSION_POST = '/api/v1/team/admission';
const API_TEAM_SEARCH_POST = '/api/v1/team/search';
const API_TEAM_FILELIST_POST = '/api/v1/team/filelist';
const API_TEAM_PROFILE_CHANGE = '/api/v1/team/changeprofile';
const API_FILEUPLOAD_POST = '/api/v1/article/fileupload';

//특정 IE 버전에서 get 명령이 서버로 안나간다. 모든 ajax는 post 로 처리.

export function teamCreateRequest(userData, teamInfo) {

    return (dispatch) => {

        dispatch(teamCreate());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };


        let data = {
            teamInfo: JSON.stringify(teamInfo),
            operatorid: userData.user._id.toString()
        };
        
        return axios.post(API_TEAM_CREATE_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(teamCreateSuccess(json.data.data));

            } else {
                dispatch(teamCreateFailure());
            }
        }).catch((error) => {
            dispatch(teamCreateFailure());
        });
    }
}

export function teamListRequest(userData, teamid) {

    return (dispatch) => {

        dispatch(teamList());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        if (!teamid) {
            teamid = 'new';
        }

        let data = {
            teamid: teamid,
             count: TEAM_LIST_LIMIT
        };

        return axios.post(`${API_TEAM_LIST_POST}`, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(teamListSuccess(json.data.data, teamid === 'new'));

            } else {
                dispatch(teamListFailure());
            }
        }).catch((error) => {
            dispatch(teamListFailure());
        });
    }
}

export function teamUserJoinRequest(userData, teamid) {

    return (dispatch) => {

        dispatch(teamJoinRequest());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid,
            userid: userData.user._id.toString()
        };
        return axios.post(`${API_TEAM_JOIN_REQ_POST}`, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(teamJoinRequestSuccess());

            } else {
                dispatch(teamJoinRequestFailure());
            }
        }).catch((error) => {
            dispatch(teamJoinRequestFailure());
        });
    }
}

export function teamFindByIdsRequest(userData, teamids) {

    return (dispatch) => {

        dispatch(teamFindRequest());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamids: JSON.stringify(teamids)
        };
        return axios.post(`${API_TEAM_FIND_TEAMS_POST}`, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(teamFindRequestSuccess(json.data.data));

            } else {
                dispatch(teamFindRequestFailure());
            }
        }).catch((error) => {
            dispatch(teamFindRequestFailure());
        });
    }
}

export function teamAdmissionUserRequest(userData, teamid, userid) {

    return (dispatch) => {

        dispatch(teamAdmission());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid,
            userid: userid
        };
        return axios.post(`${API_TEAM_ADMISSION_POST}`, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(teamAdmissionSuccess());

            } else {
                dispatch(teamAdmissionFailure());
            }
        }).catch((error) => {
            dispatch(teamAdmissionFailure());
        });
    }
}

export function teamSearchRequest(userData, opname, keyword) {

    return (dispatch) => {

        dispatch(teamSearch());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            operatorname: opname,
            keyword: keyword,
        };

        return axios.post(`${API_TEAM_SEARCH_POST}`, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(teamSearchSuccess(json.data.data));

            } else {
                dispatch(teamSearchFailure());
            }
        }).catch((error) => {
            dispatch(teamSearchFailure());
        });
    }
}

export function teamSelectInfo(userData, teamid) {

    return (dispatch) => {

        dispatch(teamInfo());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid.toString()
        };

        return axios.post(`${API_TEAM_INFO_POST}`, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(teamInfoSuccess(json.data.data));

            } else {
                dispatch(teamInfoFailure());
            }
        }).catch((error) => {
            dispatch(teamInfoFailure());
        });
    }
}

export function teamFileListRequest(userData, teamid) {

    return (dispatch) => {

       dispatch(teamFileList());

       let config = {
         headers: {'authorization': 'bearer ' + userData.token}
       };

       let data = {
           teamid: teamid,
           limit: '20'
       };

       return axios.post(`${API_TEAM_FILELIST_POST}`, data, config).then((json) => {
           if (json.data.status === 'success') {
               dispatch(teamFileListSuccess(json.data.data));

           } else {
               dispatch(teamFileListFailure());
           }
       }).catch((error) => {
           dispatch(teamFileListFailure());
       });
   }
}

export function teamFileUploadRequest(userData, files, cb) {

    return (dispatch) => {
            
        dispatch(teamFileUpload());
        
        let CancelToken = axios.CancelToken;
        let source = CancelToken.source();

        let config = {
            headers: {'authorization': 'bearer ' + userData.token},  
            
            onUploadProgress: (progressEvent) => {
              let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
              cb && cb(percentCompleted, source);
            },
            cancelToken: source.token
        };



        let form = new FormData();
        form.append('author', JSON.stringify(userData.user));
        if (files) {
            for (let i = 0; i < files.length; i++) {
                form.append('files', files[i]);
            }
        } else {
            dispatch(teamFileUploadFailure());
        }

        return axios.post(API_FILEUPLOAD_POST, form, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(teamFileUploadSuccess(json.data.data[0]));
            } else {
                dispatch(teamFileUploadFailure());
            }
        }).catch((error) => {
            if (axios.isCancel(error)) {
                console.log('## CANCEL UPLOAD');
            }
            dispatch(teamFileUploadFailure());
        });
    };
}


export function teamChangeProfileRequest(userData, teamid, cover, name, desc) {
    
     return (dispatch) => {
        
         dispatch(teamChangeProfile());

         let config = { headers: { 'authorization': 'bearer ' + userData.token } };
         let data = {
             teamid: teamid,
             cover: JSON.stringify(cover), 
             name: name,
             desc: desc
         };

         return axios.post(API_TEAM_PROFILE_CHANGE, data, config).then((json) => {
             if (json.data.status === 'success') {
                 dispatch(teamChangeProfileSuccess());
             } else {
                 dispatch(teamChangeProfileFailure());
             }
         }).catch((error) => {
             if (axios.isCancel(error)) {
                 console.log('## CANCEL UPLOAD');
             }
             dispatch(teamChangeProfileFailure());
         });
     }
}


function teamCreate() {
    return {
        type: TEAM_CREATE
    };
}

function teamCreateSuccess(newTeam) {
    return {
        type: TEAM_CREATE_SUCCESS,
        data: newTeam
    };
}

function teamCreateFailure() {
    return {
        type: TEAM_CREATE_FAILURE
    };
}


function teamList() {
    return {
        type: TEAM_LIST
    };
}

function teamListSuccess(teamlist, isFirst) {
    return {
        type: TEAM_LIST_SUCCESS,
        data: {
            teamlist: teamlist,
            isFirst: isFirst
        }
    };
}

function teamListFailure() {
    return {
        type: TEAM_LIST_FAILURE
    };
}


function teamJoinRequest() {
    return {
        type: TEAM_JOIN_REQ
    };
}

function teamJoinRequestSuccess() {
    return {
        type: TEAM_JOIN_REQ_SUCCESS,
    };
}

function teamJoinRequestFailure() {
    return {
        type: TEAM_JOIN_REQ_FAILURE
    };
}

function teamFindRequest() {
    return {
        type: TEAM_FIND_TEAMS
    };
}

function teamFindRequestSuccess(teams) {
    return {
        type: TEAM_FIND_TEAMS_SUCCESS,
        data: teams
    };
}

function teamFindRequestFailure() {
    return {
        type: TEAM_FIND_TEAMS_FAILURE
    };
}


function teamAdmission() {
    return {
        type: TEAM_ADMISSION_USER
    };
}

function teamAdmissionSuccess() {
    return {
        type: TEAM_ADMISSION_USER_SUCCESS,
    };
}

function teamAdmissionFailure() {
    return {
        type: TEAM_ADMISSION_USER_FAILURE
    };
}


function teamSearch() {
    return {
        type: TEAM_SEARCH
    };
}

function teamSearchSuccess(list) {
    return {
        type: TEAM_SEARCH_SUCCESS,
        data: list
    };
}

function teamSearchFailure() {
    return {
        type: TEAM_SEARCH_FAILURE
    };
}


function teamInfo() {
    return {
        type: TEAM_SELECT_INFO
    };
}

function teamInfoSuccess(team) {
    return {
        type: TEAM_SELECT_INFO_SUCCESS,
        data: team
    };
}

function teamInfoFailure() {
    return {
        type: TEAM_SELECT_INFO_FAILURE
    };
}

function teamFileList() {
    return {
        type: TEAM_FILE_LIST
    };
}

function teamFileListSuccess(list) {
    return {
        type: TEAM_FILE_LIST_SUCCESS,
        data: list
    };
}

function teamFileListFailure() {
    return {
        type: TEAM_FILE_LIST_FAILURE
    };
}

function teamFileUpload() {
    return {
        type: TEAM_FILEUPLOAD
    };
}

function teamFileUploadSuccess(file) {
    return {
        type: TEAM_FILEUPLOAD_SUCCESS,
        data: file
    };
}

function teamFileUploadFailure() {
    return {
        type: TEAM_FILEUPLOAD_FAILURE
    };
}


function teamChangeProfile() {
    return {
        type: TEAM_CHANGE_PROFILE
    };
}

function teamChangeProfileSuccess() {
    return {
        type: TEAM_CHANGE_PROFILE_SUCCESS
    };
}

function useteamChangeProfileFailure() {
    return {
        type: TEAM_CHANGE_PROFILE_FAILURE
    };
}
