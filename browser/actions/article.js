import {
    ARTICLE_POST,
    ARTICLE_POST_SUCCESS,
    ARTICLE_POST_FAILURE,
    ARTICLE_LIST,
    ARTICLE_LIST_SUCCESS,
    ARTICLE_LIST_FAILURE,
    ARTICLE_LIST_CLEAR,
    ARTICLE_NEW_POST,
    ARTICLE_NEW_POST_SUCCESS,
    ARTICLE_NEW_POST_FAILURE,
    ARTICLE_OLD_POST,
    ARTICLE_OLD_POST_SUCCESS,
    ARTICLE_OLD_POST_FAILURE,
    ARTICLE_FIRST_LIST_LIMIT,
    ARTICLE_OLD_LIST_LIMIT,
    ARTICLE_DELETE,
    ARTICLE_DELETE_SUCCESS,
    ARTICLE_DELETE_FAILURE,
    ARTICLE_LIKE,
    ARTICLE_LIKE_SUCCESS,
    ARTICLE_LIKE_FAILURE,
    ARTICLE_FILEUPLOAD,
    ARTICLE_FILEUPLOAD_SUCCESS,
    ARTICLE_FILEUPLOAD_FAILURE,
    ARTICLE_UPDATE_POST,
    ARTICLE_UPDATE_POST_SUCCESS,
    ARTICLE_UPDATE_POST_FAILURE,
    ARTICLE_UPDATE_EMOJI,
    ARTICLE_UPDATE_EMOJI_SUCCESS,
    ARTICLE_UPDATE_EMOJI_FAILURE,
    ARTICLE_EDIT_REQUEST,
    ARTICLE_EDIT_RESPONSE,
    ARTICLE_SEARCH,
    ARTICLE_SEARCH_SUCCESS,
    ARTICLE_SEARCH_FAILURE,
    ARTICLE_FIND,
    ARTICLE_FIND_SUCCESS,
    ARTICLE_FIND_FAILURE, 
    ARTICLE_VOTE_FINISH,
    ARTICLE_VOTE_FINISH_SUCCESS,
    ARTICLE_VOTE_FINISH_FAILURE, 
    ARTICLE_URL_META,
    ARTICLE_URL_META_SUCCESS,
    ARTICLE_URL_META_FAILURE
} from './actionTypes';

import axios from 'axios';
import FormData from 'form-data';

const API_ARTICLE_POST = '/api/v1/article/post';
const API_ARTICLE_UPDATE_POST = '/api/v1/article/update';
const API_ARTICLE_EMOJI_UPDATE_POST = '/api/v1/article/updateEmoji';
const API_ARTICLE_LIST_POST = '/api/v1/article/list';
const API_ARTICLE_NEW_LIST_POST = '/api/v1/article/newlist';
const API_ARTICLE_OLD_LIST_POST = '/api/v1/article/oldlist';
const API_ARTICLE_DELETE_POST = '/api/v1/article/delete';
const API_ARTICLE_LIKE = '/api/v1/article/like';
const API_FILEUPLOAD_POST = '/api/v1/article/fileupload';
const API_ARTICLE_ATTACHMENT_UPLOAD_POST = '/api/v1/article/attachment';
const API_ARTICLE_SEARCH_POST = '/api/v1/article/search';
const API_ARTICLE_FIND_POST = '/api/v1/article/find';
const API_ARTICLE_VOTE_FINISH_POST = '/api/v1/article/votefinish';
const API_ARTICLE_URL_META = '/api/v1/article/meta';


//특정 IE 버전에서 get 명령이 서버로 안나간다. 모든 ajax는 post 로 처리.

//사용안함.
export function articlePostRequest(userData, teamid, content, files, ogs, vote) {
    
    return (dispatch) => {

        dispatch(articlePost());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid,
            author: JSON.stringify(userData.user),
            content: content,
            files: files,
            ogs: ogs,
            vote: vote && JSON.stringify(vote)
        };

        return axios.post(API_ARTICLE_POST, data, config).then((json) => {
                if (json.data.status === 'success') {
                    dispatch(articlePostSuccess());
                } else {
                    dispatch(articlePostFailure());
                }
            }).catch((error) => {
                dispatch(articlePostFailure());
            });
    }
}

export function articleUpdateRequest(userData, articleId, teamid, content, files, vote) {

    return (dispatch) => {

        dispatch(articleUpdate());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            teamid: teamid,
            articleid: articleId,
            content: content,
            files: files,
            vote: vote && JSON.stringify(vote)
        };

        return axios.post(API_ARTICLE_UPDATE_POST, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(articleUpdateSuccess(data));
            } else {
                dispatch(articleUpdateFailure());
            }
        }).catch((error) => {
            dispatch(articleUpdateFailure());
        });
    }
}

export function articleUpdateEmojiRequest(userData, articleId, emojiCode) {

    return (dispatch) => {

        dispatch(articleUpdateEmoji());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            id: articleId,
            user: JSON.stringify(userData.user),
            emojiCode: emojiCode
        };

        return axios.post(API_ARTICLE_EMOJI_UPDATE_POST, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(articleUpdateEmojiSuccess(data));
            } else {
                dispatch(articleUpdateEmojiFailure());
            }
        }).catch((error) => {
            dispatch(articleUpdateEmojiFailure());
        });
    }
}

export function articleListRequest(userData, teamid) {

    return (dispatch) => {

        dispatch(articleList());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid,
            count: ARTICLE_FIRST_LIST_LIMIT
        };

        return axios.post(API_ARTICLE_LIST_POST, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(articleListSuccess(json.data.data));
            } else {
                dispatch(articleListFailure());
            }
        }).catch((error) => {
            dispatch(articleListFailure());
        });
    }
}


export function newArticleListByIdRequest(userData, articleid, teamid) {

    return (dispatch) => {

        dispatch(newArticleList());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            articleid: articleid,
            teamid: teamid,
        };

        //new 는 count 무시됨.
        return axios.post(API_ARTICLE_NEW_LIST_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(newArticleListSuccess(json.data.data));
            } else {
                dispatch(newArticleListFailure());
            }
        }).catch((error) => {
            dispatch(newArticleListFailure());
        });
    }
}

export function oldArticleListByIdRequest(userData, articleid, teamid) {

    return (dispatch) => {

        dispatch(oldArticleList());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            articleid: articleid,
            teamid: teamid,
            count: ARTICLE_OLD_LIST_LIMIT
        };

        return axios.post(API_ARTICLE_OLD_LIST_POST, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(oldArticleListSuccess(json.data.data));
            } else {
                dispatch(oldArticleListFailure());
            }
        }).catch((error) => {
            dispatch(oldArticleListFailure());
        });
    }
}

export function deleteArticleRequest(userData, id) {

    return (dispatch) => {

        dispatch(deleteArticle());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            articleid: id
        };

        return axios.post(API_ARTICLE_DELETE_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(deleteArticleSuccess(json.data.data));
            } else {
                dispatch(deleteArticleFailure());
            }
        }).catch((error) => {
            dispatch(deleteArticleFailure());
        });
    }
}

export function likeArticleRequest(userData, articleId) {

    return (dispatch) => {

        dispatch(likeArticle());

        var config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let userInfo = {
            userid: userData.user._id,
            email: userData.user.email,
            username: userData.user.username
        };

        let data = {
            articleId: articleId,
            userInfo: JSON.stringify(userInfo)
        };

        return axios.post(API_ARTICLE_LIKE, data, config)
        .then((json) => {

            if (json.data.status === 'success') {
                dispatch(likeArticleSuccess());
            } else {
                dispatch(likeArticleFailure());
            }
        }).catch((error) => {
            dispatch(likeArticleFailure());
        });
    }
}

export function fileUploadRequest(userData, files, cb) {
    return (dispatch) => {

        dispatch(fileupload());

        let CancelToken = axios.CancelToken;
        let source = CancelToken.source();

        let config = {
            headers: {'authorization': 'bearer ' + userData.token},
            onUploadProgress: (progressEvent) => {
              let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
              cb && cb(percentCompleted, source);
            },
            cancelToken: source.token
        };


        let form = new FormData();
        form.append('author', JSON.stringify(userData.user));
        if (files) {
            for (let i = 0; i < files.length; i++) {
                form.append('files', files[i]);
            }
        }

        return axios.post(API_FILEUPLOAD_POST, form, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(fileuploadSuccess(json.data.data));
            } else {
                dispatch(fileuploadFailure());
            }
        }).catch((error) => {
            if (axios.isCancel(error)) {
                console.log('## CANCEL UPLOAD');
            }
            dispatch(fileuploadFailure());
        });
    };
}

export function articleSearchRequest(userData, teamid, keyword) {

     return (dispatch) => {

        dispatch(articleSearch());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            teamid: teamid,
            keyword: keyword,
        };

        return axios.post(`${API_ARTICLE_SEARCH_POST}`, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(articleSearchSuccess(json.data.data));

            } else {
                dispatch(articleSearchFailure());
            }
        }).catch((error) => {
            dispatch(articleSearchFailure());
        });
    }
}

export function articleFindRequest(userData, articleid) {

    return (dispatch) => {

       dispatch(articleFind());

       let config = {
         headers: {'authorization': 'bearer ' + userData.token}
       };

       let data = {
           articleid: articleid
       };
       return axios.post(`${API_ARTICLE_FIND_POST}`, data, config).then((json) => {
           if (json.data.status === 'success') {
               dispatch(articleFindSuccess(json.data.data));

           } else {
               dispatch(articleFindFailure());
           }
       }).catch((error) => {
           dispatch(articleFindFailure());
       });
   }
}

export function articleVoteFinishRequest(userData, articleid) {
     return (dispatch) => {

       dispatch(articleVoteFinish());

       let config = {
         headers: {'authorization': 'bearer ' + userData.token}
       };

       let data = {
           articleid: articleid
       };

       return axios.post(`${API_ARTICLE_VOTE_FINISH_POST}`, data, config).then((json) => {
           if (json.data.status === 'success') {
               dispatch(articleVoteFinishSuccess(articleid));

           } else {
               dispatch(articleVoteFinishFailure());
           }
       }).catch((error) => {
           dispatch(articleVoteFinishFailure());
       });
   }
}


export function articleUrlMetaRequest(userData, url) {
     return (dispatch) => {

       dispatch(articleUrlMeta());

       let config = {
         headers: {'authorization': 'bearer ' + userData.token}
       };

       let data = {
           url: url
       }
       return axios.post(`${API_ARTICLE_URL_META}`, data, config).then((json) => {
           if (json.data.status === 'success') {
               dispatch(articleUrlMetaSuccess(json.data));

           } else {
               dispatch(articleUrlMetaFailure());
           }
       }).catch((error) => {
           dispatch(articleUrlMetaFailure());
       });
   }
}


export function clearStoredArticle() {
    return (dispatch) => {
        dispatch(articleListClear());
    }
}

export function articleEditMode(isEdit, article) {
    return (dispatch) => {
        dispatch(articleEditRequest());
        dispatch(articleEditResponse(isEdit, article));
    }
}

function articlePost() {
    return {
        type: ARTICLE_POST
    };
}


function articlePostSuccess() {
    return {
        type: ARTICLE_POST_SUCCESS
    };
}


function articlePostFailure(json) {
    return {
        type: ARTICLE_POST_FAILURE
    };
}


function articleList() {
    return {
        type: ARTICLE_LIST
    };
}


function articleListSuccess(list) {
    return {
        type: ARTICLE_LIST_SUCCESS,
        data: list
    };
}


function articleListFailure() {
    return {
        type: ARTICLE_LIST_FAILURE
    };
}

export function articleListClear() {
    return {
        type: ARTICLE_LIST_CLEAR
    };
}


function newArticleList() {
    return {
        type: ARTICLE_NEW_POST
    };
}


function newArticleListSuccess(list) {
    return {
        type: ARTICLE_NEW_POST_SUCCESS,
        data: list
    };
}


function newArticleListFailure() {
    return {
        type: ARTICLE_NEW_POST_FAILURE
    };
}

function oldArticleList() {
    return {
        type: ARTICLE_OLD_POST
    };
}


function oldArticleListSuccess(list) {
    return {
        type: ARTICLE_OLD_POST_SUCCESS,
        data: list
    };
}


function oldArticleListFailure() {
    return {
        type: ARTICLE_OLD_POST_FAILURE
    };
}


function deleteArticle() {
    return {
        type: ARTICLE_DELETE
    };
}


function deleteArticleSuccess(deleteArticle) {
    return {
        type: ARTICLE_DELETE_SUCCESS,
        data: deleteArticle
    };
}


function deleteArticleFailure() {
    return {
        type: ARTICLE_DELETE_FAILURE
    };
}

function likeArticle() {
    return {
        type: ARTICLE_LIKE
    };
}


function likeArticleSuccess() {
    return {
        type: ARTICLE_LIKE_SUCCESS
    };
}


function likeArticleFailure() {
    return {
        type: ARTICLE_LIKE_FAILURE
    };
}


function fileupload() {
    return {
        type: ARTICLE_FILEUPLOAD
    };
}


function fileuploadSuccess(uploadfilesInfo) {
    return {
        type: ARTICLE_FILEUPLOAD_SUCCESS,
        data: uploadfilesInfo
    };
}


function fileuploadFailure() {
    return {
        type: ARTICLE_FILEUPLOAD_FAILURE
    };
}

function articleUpdate() {
    return {
        type: ARTICLE_UPDATE_POST
    };
}


function articleUpdateSuccess(data) {
    return {
        type: ARTICLE_UPDATE_POST_SUCCESS,
        data: data
    };
}


function articleUpdateFailure() {
    return {
        type: ARTICLE_UPDATE_POST_FAILURE
    };
}


function articleUpdateEmoji() {
    return {
        type: ARTICLE_UPDATE_EMOJI
    };
}


function articleUpdateEmojiSuccess(data) {
    return {
        type: ARTICLE_UPDATE_EMOJI_SUCCESS,
        data: data
    };
}


function articleUpdateEmojiFailure() {
    return {
        type: ARTICLE_UPDATE_EMOJI_FAILURE
    };
}



function articleEditRequest() {
    return {
        type: ARTICLE_EDIT_REQUEST
    };
}

function articleEditResponse(isEdit, article) {
    return {
        type: ARTICLE_EDIT_RESPONSE,
        data: {
            isEdit: isEdit,
            article: article
        }
    };
}


function articleSearch() {
    return {
        type: ARTICLE_SEARCH
    };
}


function articleSearchSuccess(data) {
    return {
        type: ARTICLE_SEARCH_SUCCESS,
        data: data
    };
}


function articleSearchFailure() {
    return {
        type: ARTICLE_SEARCH_FAILURE
    };
}

function articleFind() {
    return {
        type: ARTICLE_FIND
    };
}

function articleFindSuccess(data) {
    return {
        type: ARTICLE_FIND_SUCCESS,
        data: data
    };
}

function articleFindFailure() {
    return {
        type: ARTICLE_FIND_FAILURE
    };
}


function articleVoteFinish() {
    return {
        type: ARTICLE_VOTE_FINISH
    };
}

function articleVoteFinishSuccess(articleid) {
    return {
        type: ARTICLE_VOTE_FINISH_SUCCESS,
        data: articleid
    };
}

function articleVoteFinishFailure() {
    return {
        type: ARTICLE_VOTE_FINISH_FAILURE
    };
}


function articleUrlMeta() {
    return {
        type: ARTICLE_URL_META
    };
}

function articleUrlMetaSuccess(data) {
    return {
        type: ARTICLE_URL_META_SUCCESS,
        data: data
    };
}

function articleUrlMetaFailure() {
    return {
        type: ARTICLE_URL_META_FAILURE
    };
}