export const AUTH_SIGNUP                = "AUTH_SIGNUP";
export const AUTH_SIGNUP_SUCCESS        = "AUTH_SIGNUP_SUCCESS";
export const AUTH_SIGNUP_FAILURE        = "AUTH_SIGNUP_FAILURE";
export const AUTH_SIGNIN                = "AUTH_SIGNIN";
export const AUTH_SIGNIN_SUCCESS        = "AUTH_SIGNIN_SUCCESS";
export const AUTH_SIGNIN_FAILURE        = "AUTH_SIGNIN_FAILURE";
export const AUTH_SIGNOUT               = "AUTH_SIGNOUT";

export const AUTH_SELECT_TEAM           = "AUTH_SELECT_TEAM";
export const AUTH_SELECT_TEAM_SUCCESS   = "AUTH_SELECT_TEAM_SUCCESS";

export const AUTH_UNSELECT_TEAM         = "AUTH_UNSELECT_TEAM";
export const AUTH_UNSELECT_TEAM_SUCCESS = "AUTH_UNSELECT_TEAM_SUCCESS";

export const AUTH_TEAM_ADD              = "AUTH_TEAM_ADD";
export const AUTH_TEAM_ADD_SUCCESS      = "AUTH_TEAM_ADD_SUCCESS";
export const AUTH_TEAM_ADD_FAILURE      = "AUTH_TEAM_ADD_FAILURE";

export const AUTH_SEARCH_BAR            = "AUTH_SEARCH_BAR";

export const ARTICLE_POST               = "ARTICLE_POST";
export const ARTICLE_POST_SUCCESS       = "ARTICLE_POST_SUCCESS";
export const ARTICLE_POST_FAILURE       = "ARTICLE_POST_FAILURE";

export const ARTICLE_UPDATE_POST        = "ARTICLE_UPDATE_POST";
export const ARTICLE_UPDATE_POST_SUCCESS= "ARTICLE_UPDATE_POST_SUCCESS";
export const ARTICLE_UPDATE_POST_FAILURE= "ARTICLE_UPDATE_POST_FAILURE";

export const ARTICLE_UPDATE_EMOJI         = "ARTICLE_UPDATE_EMOJI_POST";
export const ARTICLE_UPDATE_EMOJI_SUCCESS = "ARTICLE_UPDATE_EMOJIPOST_SUCCESS";
export const ARTICLE_UPDATE_EMOJI_FAILURE = "ARTICLE_UPDATE_EMOJIPOST_FAILURE";

export const ARTICLE_LIST               = "ARTICLE_LIST";
export const ARTICLE_LIST_SUCCESS       = "ARTICLE_LIST_SUCCESS";
export const ARTICLE_LIST_FAILURE       = "ARTICLE_LIST_FAILURE";
export const ARTICLE_LIST_CLEAR         = "ARTICLE_LIST_CLEAR";
export const ARTICLE_NEW_POST            = "ARTICLE_NEW_POST";
export const ARTICLE_NEW_POST_SUCCESS    = "ARTICLE_NEW_POST_SUCCESS";
export const ARTICLE_NEW_POST_FAILURE    = "ARTICLE_NEW_POST_FAILURE";
export const ARTICLE_OLD_POST            = "ARTICLE_OLD_POST";
export const ARTICLE_OLD_POST_SUCCESS    = "ARTICLE_OLD_POST_SUCCESS";
export const ARTICLE_OLD_POST_FAILURE    = "ARTICLE_OLD_POST_FAILURE";

export const ARTICLE_DELETE              = "ARTICLE_DELETE";
export const ARTICLE_DELETE_SUCCESS      = "ARTICLE_DELETE_SUCCESS";
export const ARTICLE_DELETE_FAILURE      = "ARTICLE_DELETE_FAILURE";

export const ARTICLE_FIND                = "ARTICLE_FIND";
export const ARTICLE_FIND_SUCCESS        = "ARTICLE_FIND_SUCCESS";
export const ARTICLE_FIND_FAILURE        = "ARTICLE_FIND_FAILURE";

export const ARTICLE_LIKE                = "ARTICLE_LIKE";
export const ARTICLE_LIKE_SUCCESS        = "ARTICLE_LIKE_SUCCESS";
export const ARTICLE_LIKE_FAILURE        = "ARTICLE_LIKE_FAILURE";

export const ARTICLE_FILEUPLOAD          = "ARTICLE_FILEUPLOAD";
export const ARTICLE_FILEUPLOAD_SUCCESS  = "ARTICLE_FILEUPLOAD_SUCCESS";
export const ARTICLE_FILEUPLOAD_FAILURE  = "ARTICLE_FILEUPLOAD_FAILURE";

export const ARTICLE_SEARCH             = "ARTICLE_SEARCH";
export const ARTICLE_SEARCH_SUCCESS     = "ARTICLE_SEARCH_SUCCESS";
export const ARTICLE_SEARCH_FAILURE     = "ARTICLE_SEARCH_FAILURE";

export const ARTICLE_VOTE_FINISH        = "ARTICLE_VOTE_FINISH";
export const ARTICLE_VOTE_FINISH_SUCCESS = "ARTICLE_VOTE_FINISH_SUCCESS";
export const ARTICLE_VOTE_FINISH_FAILURE = "ARTICLE_VOTE_FINISH_FAILURE";

export const ARTICLE_URL_META            = "ARTICLE_URL_META";
export const ARTICLE_URL_META_SUCCESS    = "ARTICLE_URL_META_SUCCESS";
export const ARTICLE_URL_META_FAILURE    = "ARTICLE_URL_META_FAILURE";

export const COMMENT_POST                = "COMMENT_POST";
export const COMMENT_POST_SUCCESS        = "COMMENT_POST_SUCCESS";
export const COMMENT_POST_FAILURE        = "COMMENT_POST_FAILURE";

export const COMMENT_UPDATE              = "COMMENT_UPDATE";
export const COMMENT_UPDATE_SUCCESS      = "COMMENT_UPDATE_SUCCESS";
export const COMMENT_UPDATE_FAILURE      = "COMMENT_UPDATE_FAILURE";

export const COMMENT_LIST                = "COMMENT_LIST";
export const COMMENT_LIST_SUCCESS        = "COMMENT_LIST_SUCCESS";
export const COMMENT_LIST_FAILURE        = "COMMENT_LIST_FAILURE";

export const COMMENT_NEW_LIST            = "COMMENT_NEW_LIST";
export const COMMENT_NEW_LIST_SUCCESS    = "COMMENT_NEW_LIST_SUCCESS";
export const COMMENT_NEW_LIST_FAILURE    = "COMMENT_NEW_LIST_FAILURE";

export const COMMENT_OLD_LIST            = "COMMENT_OLD_LIST";
export const COMMENT_OLD_LIST_SUCCESS    = "COMMENT_OLD_LIST_SUCCESS";
export const COMMENT_OLD_LIST_FAILURE    = "COMMENT_OLD_LIST_FAILURE";

export const COMMENT_DELETE              = "COMMENT_DELETE";
export const COMMENT_DELETE_SUCCESS      = "COMMENT_DELETE_SUCCESS";
export const COMMENT_DELETE_FAILURE      = "COMMENT_DELETE_FAILURE";

export const COMMENT_FILEUPLOAD          = "COMMENT_FILEUPLOAD";
export const COMMENT_FILEUPLOAD_SUCCESS  = "COMMENT_FILEUPLOAD_SUCCESS";
export const COMMENT_FILEUPLOAD_FAILURE  = "COMMENT_FILEUPLOAD_FAILURE";

export const COMMENT_LIST_CLEAR          = "COMMENT_LIST_CLEAR";

export const ARTICLE_EDIT_REQUEST        = "ARTICLE_EDIT_REQUEST";
export const ARTICLE_EDIT_RESPONSE       = "ARTICLE_EDIT_RESPONSE";

export const ARTICLE_MODAL_REQUEST        = "ARTICLE_MODAL_REQUEST";

export const ARTICLE_FIRST_LIST_LIMIT    = 10;
export const ARTICLE_OLD_LIST_LIMIT      = 5;

export const TEAM_CREATE                = "TEAM_CREATE";
export const TEAM_CREATE_SUCCESS        = "TEAM_CREATE_SUCCESS";
export const TEAM_CREATE_FAILURE        = "TEAM_CREATE_FAILURE";

export const TEAM_LIST                  = "TEAM_LIST";
export const TEAM_LIST_SUCCESS          = "TEAM_LIST_SUCCESS";
export const TEAM_LIST_FAILURE          = "TEAM_LIST_FAILURE";

export const TEAM_LIST_LIMIT            = 1000;

export const TEAM_JOIN_REQ              = "TEAM_JOIN_REQ";
export const TEAM_JOIN_REQ_SUCCESS      = "TEAM_JOIN_REQ_SUCCESS";
export const TEAM_JOIN_REQ_FAILURE      = "TEAM_JOIN_REQ_FAILURE";

export const TEAM_FIND_TEAMS            = "TEAM_FIND_TEAMS";
export const TEAM_FIND_TEAMS_SUCCESS    = "TEAM_FIND_TEAMS_SUCCESS";
export const TEAM_FIND_TEAMS_FAILURE    = "TEAM_FIND_TEAMS_FAILURE";

export const TEAM_ADMISSION_USER            = "TEAM_ADMISSION_USER";
export const TEAM_ADMISSION_USER_SUCCESS    = "TEAM_ADMISSION_USER_SUCCESS";
export const TEAM_ADMISSION_USER_FAILURE    = "TEAM_ADMISSION_USER_FAILURE";

export const TEAM_FILEUPLOAD            = "TEAM_FILEUPLOAD";
export const TEAM_FILEUPLOAD_SUCCESS    = "TEAM_FILEUPLOAD_SUCCESS";
export const TEAM_FILEUPLOAD_FAILURE    = "TEAM_FILEUPLOAD_FAILURE";

export const TEAM_CHANGE_PROFILE 		 = "TEAM_CHANGE_PROFILE";
export const TEAM_CHANGE_PROFILE_SUCCESS = "TEAM_CHANGE_PROFILE_SUCCESS";
export const TEAM_CHANGE_PROFILE_FAILURE = "TEAM_CHANGE_PROFILE_FAILURE"; 

export const USER_INFO                  = "USER_INFO";
export const USER_INFO_SUCCESS          = "USER_INFO_SUCCESS";
export const USER_INFO_FAILURE          = "USER_INFO_FAILURE";

export const USER_CHANGE_PROFILE_IMG         = "USER_CHANGE_PROFILE_IMG";
export const USER_CHANGE_PROFILE_IMG_SUCCESS = "USER_CHANGE_PROFILE_IMG_SUCCESS";
export const USER_CHANGE_PROFILE_IMG_FAILURE = "USER_CHANGE_PROFILE_IMG_FAILURE";

export const USER_CHANGE_INFO         	= "USER_CHANGE_INFO";
export const USER_CHANGE_INFO_SUCCESS 	= "USER_CHANGE_INFO_SUCCESS";
export const USER_CHANGE_INFO_FAILURE 	= "USER_CHANGE_INFO_FAILURE";

export const USER_FILEUPLOAD            = "USER_FILEUPLOAD";
export const USER_FILEUPLOAD_SUCCESS    = "USER_FILEUPLOAD_SUCCESS";
export const USER_FILEUPLOAD_FAILURE    = "USER_FILEUPLOAD_FAILURE";

export const TEAM_SEARCH                = "TEAM_SEARCH";
export const TEAM_SEARCH_SUCCESS        = "TEAM_SEARCH_SUCCESS";
export const TEAM_SEARCH_FAILURE        = "TEAM_SEARCH_FAILURE";

export const TEAM_SELECT_INFO           = "TEAM_SELECT_INFO";
export const TEAM_SELECT_INFO_SUCCESS   = "TEAM_SELECT_INFO_SUCCESS";
export const TEAM_SELECT_INFO_FAILURE   = "TEAM_SELECT_INFO_FAILURE";

export const TEAM_FILE_LIST             = "TEAM_FILE_LIST";
export const TEAM_FILE_LIST_SUCCESS     = "TEAM_FILE_LIST_SUCCESS";
export const TEAM_FILE_LIST_FAILURE     = "TEAM_FILE_LIST_FAILURE";
