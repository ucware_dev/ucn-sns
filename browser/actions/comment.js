import {
    COMMENT_POST,
    COMMENT_POST_SUCCESS,
    COMMENT_POST_FAILURE,
    COMMENT_UPDATE,
    COMMENT_UPDATE_SUCCESS,
    COMMENT_UPDATE_FAILURE,
    COMMENT_LIST,
    COMMENT_LIST_SUCCESS,
    COMMENT_LIST_FAILURE,
    COMMENT_NEW_LIST,
    COMMENT_NEW_LIST_SUCCESS,
    COMMENT_NEW_LIST_FAILURE,
    COMMENT_OLD_LIST,
    COMMENT_OLD_LIST_SUCCESS,
    COMMENT_OLD_LIST_FAILURE,
    COMMENT_DELETE,
    COMMENT_DELETE_SUCCESS,
    COMMENT_DELETE_FAILURE,
    COMMENT_FILEUPLOAD,
    COMMENT_FILEUPLOAD_SUCCESS,
    COMMENT_FILEUPLOAD_FAILURE,
    COMMENT_LIST_CLEAR } from './actionTypes';

import axios from 'axios';
import FormData from 'form-data';

const API_COMMENT_POST  = '/api/v1/comment/post';
const API_COMMENT_UPDATE_POST = '/api/v1/comment/update';
const API_COMMENT_LIST_POST = '/api/v1/comment/list';
const API_COMMENT_LIST_NEW_POST = '/api/v1/comment/newlist';
const API_COMMENT_LIST_OLD_POST = '/api/v1/comment/oldlist';
const API_COMMENT_DELETE_POST  = '/api/v1/comment/delete';
const API_FILEUPLOAD_POST = '/api/v1/article/fileupload';

//특정 IE 버전에서 get 명령이 서버로 안나간다. 모든 ajax는 post 로 처리.

export function commentListClear() {
    return {
        type: COMMENT_LIST_CLEAR
    };
}

export function commentPostRequest(userData, articleId, teamid, comment, imgInfo) {

    return (dispatch) => {

        dispatch(commentPost());

        let config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            articleid: articleId,
            teamid: teamid,
            authorid: userData.user._id.toString(),
            content: comment,
            imgInfo: JSON.stringify(imgInfo)
        };

        return axios.post(API_COMMENT_POST, data, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(commentPostSuccess(json.data.data));
            } else {
                dispatch(commentPostFailure());
            }
        }).catch((error) => {
            dispatch(commentPostFailure());
        });
    }
}

export function commentUpdateRequest(userData, articleId, commentId, teamid, comment, imgInfo) {

     return (dispatch) => {

        dispatch(commentUpdate());

        var config = {
          headers: {'authorization': 'bearer ' + userData.token}
        };

        let data = {
            commentid: commentId,
            teamid: teamid,
            content: comment,
            imgInfo: imgInfo ? JSON.stringify(imgInfo) : null
        };

        return axios.post(API_COMMENT_UPDATE_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                dispatch(commentUpdateSuccess(data, articleId, commentId));
            } else {
                dispatch(commentUpdateFailure());
            }
        }).catch((error) => {
            dispatch(commentUpdateFailure());
        });
    }
}

export function commentListRequest(userData, articleid) {

    return (dispatch) => {
        dispatch(commentList());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            articleid: articleid,
            limit: 2
        };
        
        return axios.post(API_COMMENT_LIST_POST, data, config).then((json) => {
                if (json.data.status === 'success') {
                    dispatch(commentListSuccess(json.data.data, articleid));
                } else {
                    dispatch(commentListFailure());
                }
            }).catch((error) => {
                dispatch(commentListFailure());
            });
    }
}

export function commentNewListRequest(userData, articleid, commentid) {

    return (dispatch) => {
        dispatch(commentNewList());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            articleid: articleid,
            commentid: commentid,
            limit: 0
        };

        return axios.post(API_COMMENT_LIST_NEW_POST, data, config).then((json) => {

                if (json.data.status === 'success') {
                    dispatch(commentNewListSuccess(json.data.data, articleid));
                } else {
                    dispatch(commentNewListFailure());
                }
            }).catch((error) => {
                dispatch(commentNewListFailure());
            });
    }
}

export function commentOldListRequest(userData, articleid, commentid, count) {
    
    return (dispatch) => {
        dispatch(commentOldList());

        let config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            articleid: articleid,
            commentid: commentid,
            count: count
        };

        return axios.post(API_COMMENT_LIST_OLD_POST, data, config).then((json) => {
                if (json.data.status === 'success') {
                    dispatch(commentOldListSuccess(json.data.data, articleid));
                } else {
                    dispatch(commentOldListFailure());
                }
            }).catch((error) => {
                dispatch(commentOldListFailure());
            });
    }
}

export function deleteCommentRequest(userData, articleid, commentid) {
    
    return (dispatch) => {

        dispatch(deleteComment());

        var config = {
            headers: { 'authorization': 'bearer ' + userData.token }
        };

        let data = {
            articleid: articleid,
            commentid: commentid
        };

        return axios.post(API_COMMENT_DELETE_POST, data, config).then((json) => {

            if (json.data.status === 'success') {
                let commentCount = json.data.data.commentCount;
                dispatch(deleteCommentSuccess(articleid, commentid, commentCount));
            } else {
                dispatch(deleteCommentFailure());
            }
        }).catch((error) => {
            dispatch(deleteCommentFailure());
        });
    }
}

export function cmmtFileUploadRequest(userData, files, cb) {

    return (dispatch) => {
        
        dispatch(fileupload());
        
        let CancelToken = axios.CancelToken;
        let source = CancelToken.source();

        let config = {
            headers: {'authorization': 'bearer ' + userData.token},  
            
            onUploadProgress: (progressEvent) => {
              let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
              cb && cb(percentCompleted, source);
            },
            cancelToken: source.token
        };


        let form = new FormData();
        form.append('author', JSON.stringify(userData.user));
        if (files) {
            for (let i = 0; i < files.length; i++) {
                form.append('files', files[i]);
            }
        } else {
            dispatch(fileuploadFailure());
        }

        return axios.post(API_FILEUPLOAD_POST, form, config).then((json) => {
            if (json.data.status === 'success') {
                dispatch(fileuploadSuccess(json.data.data));
            } else {
                dispatch(fileuploadFailure());
            }
        }).catch((error) => {
            if (axios.isCancel(error)) {
                console.log('## CANCEL UPLOAD');
            }
            dispatch(fileuploadFailure());
        });
    };
}

export function clearStoredComment() {
    return (dispatch) => {
        dispatch(commentListClear());
    }
}


function commentPost() {
    return {
        type: COMMENT_POST
    };
}


function commentPostSuccess(data) {
    return {
        type: COMMENT_POST_SUCCESS,
        data: data
    };
}


function commentPostFailure() {
    return {
        type: COMMENT_POST_FAILURE
    };
}


function commentUpdate() {
    return {
        type: COMMENT_UPDATE
    };
}


function commentUpdateSuccess(data, articleid, commentid) {
    return {
        type: COMMENT_UPDATE_SUCCESS,
        data: {
            newComment: data,
            articleid: articleid,
            commentid: commentid
        }
    };
}


function commentUpdateFailure() {
    return {
        type: COMMENT_UPDATE_FAILURE
    };
}

function commentList() {
    return {
        type: COMMENT_LIST
    };
}


function commentListSuccess(commentArr, articleid) {
    return {
        type: COMMENT_LIST_SUCCESS,
        data: {
            commentList: commentArr,
            articleid: articleid
        }
    };
}


function commentListFailure() {
    return {
        type: COMMENT_LIST_FAILURE
    };
}

function commentNewList() {
    return {
        type: COMMENT_NEW_LIST
    };
}


function commentNewListSuccess(commentList, articleid) {
    return {
        type: COMMENT_NEW_LIST_SUCCESS,
        data: {
            commentList: commentList,
            articleid: articleid
        }
    };
}


function commentNewListFailure() {
    return {
        type: COMMENT_NEW_LIST_FAILURE
    };
}


function commentOldList() {
    return {
        type: COMMENT_OLD_LIST
    };
}


function commentOldListSuccess(commentArr, articleid) {
    return {
        type: COMMENT_OLD_LIST_SUCCESS,
        data: {
            commentList: commentArr,
            articleid: articleid
        }
    };
}


function commentOldListFailure() {
    return {
        type: COMMENT_OLD_LIST_FAILURE
    };
}

function deleteComment() {
    return {
        type: COMMENT_DELETE
    };
}


function deleteCommentSuccess(articleid, deleteid, count) {
    return {
        type: COMMENT_DELETE_SUCCESS,
        data: {
            deleteid: deleteid,
            articleid: articleid,
            commentCount: count
        }
    };
}


function deleteCommentFailure() {
    return {
        type: COMMENT_DELETE_FAILURE
    };
}

function fileupload() {
    return {
        type: COMMENT_FILEUPLOAD
    };
}


function fileuploadSuccess(uploadfilesInfo) {
    return {
        type: COMMENT_FILEUPLOAD_SUCCESS,
        data: uploadfilesInfo
    };
}


function fileuploadFailure() {
    return {
        type: COMMENT_FILEUPLOAD_FAILURE
    };
}