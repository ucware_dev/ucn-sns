import {
    AUTH_SIGNOUT,
    ARTICLE_POST,
    ARTICLE_POST_SUCCESS,
    ARTICLE_POST_FAILURE,
    ARTICLE_LIST,
    ARTICLE_LIST_SUCCESS,
    ARTICLE_LIST_FAILURE,
    ARTICLE_LIST_CLEAR,
    ARTICLE_NEW_POST,
    ARTICLE_NEW_POST_SUCCESS,
    ARTICLE_NEW_POST_FAILURE,
    ARTICLE_OLD_POST,
    ARTICLE_OLD_POST_SUCCESS,
    ARTICLE_OLD_POST_FAILURE,
    ARTICLE_FIST_LIST_LIMIT,
    ARTICLE_OLD_LIST_LIMIT,
    ARTICLE_DELETE,
    ARTICLE_DELETE_SUCCESS,
    ARTICLE_DELETE_FAILURE,
    ARTICLE_LIKE,
    ARTICLE_LIKE_SUCCESS,
    ARTICLE_LIKE_FAILURE,
    ARTICLE_FILEUPLOAD,
    ARTICLE_FILEUPLOAD_SUCCESS,
    ARTICLE_FILEUPLOAD_FAILURE,
    ARTICLE_UPDATE_POST,
    ARTICLE_UPDATE_POST_SUCCESS,
    ARTICLE_UPDATE_POST_FAILURE,
    ARTICLE_UPDATE_EMOJI,
    ARTICLE_UPDATE_EMOJI_SUCCESS,
    ARTICLE_UPDATE_EMOJI_FAILURE,
    ARTICLE_EDIT_REQUEST,
    ARTICLE_EDIT_RESPONSE,
    ARTICLE_SEARCH,
    ARTICLE_SEARCH_SUCCESS,
    ARTICLE_SEARCH_FAILURE,
    ARTICLE_FIND,
    ARTICLE_FIND_SUCCESS,
    ARTICLE_FIND_FAILURE,
    ARTICLE_VOTE_FINISH,
    ARTICLE_VOTE_FINISH_SUCCESS,
    ARTICLE_VOTE_FINISH_FAILURE,
    ARTICLE_URL_META,
    ARTICLE_URL_META_SUCCESS,
    ARTICLE_URL_META_FAILURE  

} from '../actions/actionTypes';

import update from 'react-addons-update';

const initialState = {
        post: {
            status: 'INIT'
        },
        list: {
            status: 'INIT',
            data: null,
            isLast: true
        },
        like: {
            status: 'INIT'
        },
        fileupload: {
            status: 'INIT',
            data: null
        },
        update: {
            status: 'INIT',
        },
        editmode: {
            status: 'INIT',
            isEdit: false,
            article: null
        },
        updateEmoji: {
            status: 'INIT',
        },
        search: {
            status: 'INIT',
        },
        foundArticle: {
            status: 'INIT',
        },
        voteFinish: {
            status: 'INIT',
        },
        meta: {
            status: 'INIT',
            data: null
        }
};


const reducersMap = {
    AUTH_SIGNOUT: (state, data) => {
        return initialState;
    },
    ARTICLE_POST: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'WAITING'}
            }
        });
    },
    ARTICLE_POST_SUCCESS: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'SUCCESS'}
            }
        });
    },
    ARTICLE_POST_FAILURE: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'FAILURE'}
            }
        });
    },
    ARTICLE_LIST: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'WAITING'},
                data: {$set: null},
                isLast: {$set:  true}
            }
        });
    },
    ARTICLE_LIST_SUCCESS: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'SUCCESS'},
                data: {$set: data},
                isLast: {$set: data.length < ARTICLE_FIST_LIST_LIMIT}
            }
        });
    },
    ARTICLE_LIST_FAILURE: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_LIST_CLEAR: (state, data) => {
        return initialState;
    },
    ARTICLE_NEW_POST: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'WAITING'}
            }
        });
    },
    ARTICLE_NEW_POST_SUCCESS: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'SUCCESS'},
                data: {$unshift: data}
            }
        });
    },
    ARTICLE_NEW_POST_FAILURE: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'FAILURE'}
            }
        });
    },
    ARTICLE_OLD_POST: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'WAITING'}
            }
        });
    },
    ARTICLE_OLD_POST_SUCCESS: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'SUCCESS'},
                data: {$push: data},
                isLast: {$set: data.length < ARTICLE_OLD_LIST_LIMIT}
            }
        });
    },
    ARTICLE_OLD_POST_FAILURE: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'FAILURE'},
                isLast: {$set: true}
            }
        });
    },
    ARTICLE_DELETE: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'WAITING'},
            }
        });
    },
    ARTICLE_DELETE_SUCCESS: (state, data) => {
        let index = -1;
        if (state.list.data && data.deleteId) {
            state.list.data.forEach((v, i) => {
                 if (v._id.toString() === data.deleteId) {
                    index = i;
                    return;
                 }
            });
            if (index >= 0) {
                let r = update(state, {
                    list: {
                        status: {$set: 'SUCCESS'},
                        data: {$splice: [[index, 1]]}
                    }
                });
                return r;
            }
        }
        return update(state, {
            list: {
                status: {$set: 'SUCCESS'},
            }
        });
    },    
    ARTICLE_DELETE_FAILURE: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'FAILURE'}
            }
        });
    },
    ARTICLE_LIKE: (state, data) => {
        return update(state, {
            like: {
                status: {$set: 'WAITING'}
            }
        });
    },
    ARTICLE_LIKE_SUCCESS: (state, data) => {
        return update(state, {
            like: {
                status: {$set: 'SUCCESS'}
            }
        });
    },
    ARTICLE_LIKE_FAILURE: (state, data) => {
        return update(state, {
            like: {
                status: {$set: 'FAILURE'}
            }
        });
    },
    ARTICLE_FILEUPLOAD: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'WAITING'},
                data: {$set: null}
            },
            editmode: {
                status: {$set: 'REQ'},
            }
        });
    },
    ARTICLE_FILEUPLOAD_SUCCESS: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            },
            editmode: {
                status: {$set: 'REQ'},
            }
        });
    },
    ARTICLE_FILEUPLOAD_FAILURE: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'FAILURE'},
                data: {$set: null}
            },
            editmode: {
                status: {$set: 'REQ'},
            }
        });
    },
    ARTICLE_UPDATE_POST: (state, data) => {
        return update(state, {
            update: {
                status: {$set: 'WAITING'},
            }
        });
    },
    ARTICLE_UPDATE_POST_SUCCESS: (state, data) => {

        if (state.list.data && data.articleid) {
            let index = -1;
            state.list.data.forEach((v, i) => {
                 if (v._id.toString() === data.articleid.toString()) {
                    index = i;
                    return true;
                }
            });

            if (index >= 0) {
                state.list.data[index].content = data.content;
                state.list.data[index].files = data.files;
                state.list.data[index].vote = JSON.parse(data.vote);
            }
        }

        return update(state, {
            update: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    ARTICLE_UPDATE_POST_FAILURE: (state, data) => {
        return update(state, {
            update: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_UPDATE_EMOJI: (state, data) => {
        return update(state, {
            updateEmoji: {
                status: {$set: 'WAITING'},
            }
        });
    },
    ARTICLE_UPDATE_EMOJI_SUCCESS: (state, data) => {

        if (data) {
            let index = -1;
            state.list.data.some((v, i) => {
                if (v._id.toString() === data.id) {
                    index = i;
                    return true;
                }
                return false;
            });

            if (index >= 0) {
                let newEmojis = {
                    user: JSON.parse(data.user),
                    emojiCode: data.emojiCode
                };

                let found = false;
                let removeIdx = -1;
                state.list.data[index].emojis.forEach ((v, i) => {
                    if (v.user._id.toString() === newEmojis.user._id.toString()) {
                        found = true;

                        if (v.emojiCode === data.emojiCode) {
                            removeIdx = i;
                        }

                        v.emojiCode = data.emojiCode;
                    }
                });

                if (!found) {
                    state.list.data[index].emojis.push(newEmojis);
                }

                if (removeIdx >= 0) {
                    state.list.data[index].emojis.splice(removeIdx, 1);
                }
            }
        }

        return update(state, {
            updateEmoji: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    ARTICLE_UPDATE_EMOJI_FAILURE: (state, data) => {
        return update(state, {
            updateEmoji: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_EDIT_REQUEST: (state, data) => {
        return update(state, {
            editmode: {
                status: {$set: 'REQ'},
                article: {$set: null},
            }
        });
    },
    ARTICLE_EDIT_RESPONSE: (state, data) => {
        return update(state, {
            editmode: {
                status: {$set: 'RES'},
                isEdit: {$set: data.isEdit},
                article: {$set: data.article},
            }
        });
    },
    ARTICLE_SEARCH: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'WAITING'},
                data: {$set: null},
                isLast: {$set: true}
            },
            search: {
                status: { $set: 'WAITING' },
            }
        });
    },
    ARTICLE_SEARCH_SUCCESS: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'SUCCESS'},
                data: {$set: data},
                isLast: {$set: true}
            },
            search: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    ARTICLE_SEARCH_FAILURE: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'FAILURE'},
                data: {$set: null},
                isLast: {$set: true}
            },
            search: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_FIND: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'WAITING'},
                data: {$set: null},
                isLast: {$set: true}
            },
            foundArticle: {
                status: {$set: 'WAITING'},
            }
        });
    },
    ARTICLE_FIND_SUCCESS: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'SUCCESS'},
                data: {$set: [data]},
                isLast: {$set: true}
            },
            foundArticle: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    ARTICLE_FIND_FAILURE: (state, data) => {
        return update(state, {
            list : {
                status: {$set: 'FAILURE'},
                data: {$set: null},
                isLast: {$set: true}
            },
            foundArticle: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_VOTE_FINISH: (state, data) => {
        return update(state, {
            voteFinish: {
                status: {$set: 'WAITING'},
            }
        });
    },
    ARTICLE_VOTE_FINISH_SUCCESS: (state, data) => {

        let articleid = data;
        state.list.data.forEach((v, i) => {
            if (v._id.toString() === articleid) {
                v.vote.finishVote = true;
            }
        });

        return update(state, {
            voteFinish: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    ARTICLE_VOTE_FINISH_FAILURE: (state, data) => {
        return update(state, {
            voteFinish: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    ARTICLE_URL_META: (state, data) => {
        return update(state, {
            meta: {
                status: {$set: 'WAITING'},
                data: {$set: null},
            }
        });
    },
    ARTICLE_URL_META_SUCCESS: (state, data) => {
        return update(state, {
            meta: {
                status: {$set: 'SUCCESS'},
                data: {$set: data},
            }
        });
    },
    ARTICLE_URL_META_FAILURE: (state, data) => {
        return update(state, {
            meta: {
                status: {$set: 'FAILURE'},
                data: {$set: null},
            }
        });
    }
};

export default function article(state = initialState, action) {
    if (reducersMap.hasOwnProperty(action.type)) {
        return reducersMap[action.type](state, action.data);
    }
    return state;
}