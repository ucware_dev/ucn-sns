import {
    AUTH_SIGNOUT,
    COMMENT_POST,
    COMMENT_POST_SUCCESS,
    COMMENT_POST_FAILURE,
    COMMENT_UPDATE,
    COMMENT_UPDATE_SUCCESS,
    COMMENT_UPDATE_FAILURE,
    COMMENT_LIST,
    COMMENT_LIST_SUCCESS,
    COMMENT_LIST_FAILURE,
    COMMENT_NEW_LIST,
    COMMENT_NEW_LIST_SUCCESS,
    COMMENT_NEW_LIST_FAILURE,
    COMMENT_OLD_LIST,
    COMMENT_OLD_LIST_SUCCESS,
    COMMENT_OLD_LIST_FAILURE,
    COMMENT_DELETE,
    COMMENT_DELETE_SUCCESS,
    COMMENT_DELETE_FAILURE,
    COMMENT_FILEUPLOAD,
    COMMENT_FILEUPLOAD_SUCCESS,
    COMMENT_FILEUPLOAD_FAILURE,
    COMMENT_LIST_CLEAR } from '../actions/actionTypes';

import update from 'react-addons-update';

const initialState = {
    status: 'INIT',
    data: {},
    fileupload: {
        status: 'INIT',
        data: null
    },
    post: {
        status: 'INIT',
        data: null
    },
    delete: {
        status: 'INIT',
        commentCount: 0
    }
};

const reducersMap = {
    AUTH_SIGNOUT: (state, data) => {
        return initialState;
    },
    COMMENT_POST: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'SUCCESS'},
                data: {$set: null},
            }
        });
    },
    COMMENT_POST_SUCCESS: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            }
        });
    },
    COMMENT_POST_FAILURE: (state, data) => {
        return update(state, {
            post: {
                status: {$set: 'FAILURE'},
                data: {$set: null},
            }
        });
    },
    COMMENT_UPDATE: (state, data) => {
        return update(state, {
            status: {$set: 'WAITING'}
        });
    },
    COMMENT_UPDATE_SUCCESS: (state, data) => {
        if (state.data && data.commentid) {  
            let cmts = state.data[data.articleid];
            let index = -1;
            cmts.some((v, i) => {
                if (v._id.toString() === data.commentid) {
                    index = i;
                    return true;
                }
                return false;
            });

            if (index >= 0) {
                cmts[index].content = data.newComment.content;
                cmts[index].date = Date.now();
            }
        }

        return update(state, {
            status: {$set: 'SUCCESS'}
        });
    },
    COMMENT_UPDATE_FAILURE: (state, data) => {
        return update(state, {
            status: {$set: 'FAILURE'}
        });
    },
    COMMENT_LIST: (state, data) => {
        return update(state, {
            status: {$set: 'WAITING'},
        });
    },
    COMMENT_LIST_SUCCESS: (state, data) => {
            
        let newData = data.commentList;
        let key = data.articleid.toString();
        let oldData = state.data;
        oldData[key] = newData;

        return update(state, {
            status: { $set: 'SUCCESS' },
            data: { $set: oldData }
        });
    },
    COMMENT_LIST_FAILURE: (state, data) => {
        return update(state, {
            status: {$set: 'FAILURE'},
        });
    },
    COMMENT_NEW_LIST: (state, data) => {
        return update(state, {
            status: {$set: 'WAITING'}
        });
    },
    COMMENT_NEW_LIST_SUCCESS: (state, data) => {

        let newData = data.commentList;
        let key = data.articleid.toString();

        let oldData = state.data;
        if (!oldData[key]) {
            oldData[key] = newData;
            return update(state, {
                status: { $set: 'SUCCESS' },
                data: { $set: oldData }
            });
        }
        oldData[key] = newData.concat(oldData[key]);
        return update(state, {
            status: { $set: 'SUCCESS' },
            data: { $set: oldData }
        });
    },
    COMMENT_NEW_LIST_FAILURE: (state, data) => {
        return update(state, {
            status: {$set: 'FAILURE'}
        });
    },
    COMMENT_OLD_LIST: (state, data) => {
        return update(state, {
            status: {$set: 'WAITING'}
        });
    },
    COMMENT_OLD_LIST_SUCCESS: (state, data) => {

        let fetchList = data.commentList;
        let key = data.articleid.toString();

        let oldData = state.data;
        let mergeList = oldData[key].concat(fetchList);
        oldData[key] = mergeList;

        return update(state, {
            status: { $set: 'SUCCESS' },
            data: { $set: oldData }
        });
    },
    COMMENT_OLD_LIST_FAILURE: (state, data) => {
        return update(state, {
            status: {$set: 'FAILURE'}
        });
    },
    COMMENT_DELETE: (state, data) => {
        return update(state, {
            delete: {
                status: {$set: 'WAITING'},
            }
        });
    },
    COMMENT_DELETE_SUCCESS: (state, data) => {

        if (state.data[data.articleid] && data.deleteid) {

            let oldData = state.data[data.articleid];
            let index = -1;

            oldData.forEach((v, i) => {
                if (v._id.toString() === data.deleteid) {
                    index = i;
                    return true;
                }
            });
            let newCommentList = state.data;
            newCommentList[data.articleid] = oldData;
            if (index >= 0) {
                oldData.splice(index, 1);
                update(state, {
                    status: { $set: 'SUCCESS' },
                    data: {$set: newCommentList},
                });
            }
        }

        return update(state, {
            delete: {
                status: {$set: 'SUCCESS'},
                commentCount: {$set: data.commentCount}
            }
        });
    },
    COMMENT_DELETE_FAILURE: (state, data) => {
        return update(state, {
            delete: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    COMMENT_FILEUPLOAD: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'WAITING'},
                data: {$set: null}
            }
        });
    },
    COMMENT_FILEUPLOAD_SUCCESS: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            }
        });
    },
    COMMENT_FILEUPLOAD_FAILURE: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'FAILURE'},
                data: {$set: null}
            }
        });
    },
    COMMENT_LIST_CLEAR: (state, data) => {
        return initialState;
    }
};

export default function comment(state = initialState, action) {
    if (reducersMap.hasOwnProperty(action.type)) {
        
        return reducersMap[action.type](state, action.data);
    }
    return state;
}