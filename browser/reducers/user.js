import {
    AUTH_SIGNOUT,
    USER_CHANGE_PROFILE_IMG,
    USER_CHANGE_PROFILE_IMG_SUCCESS,
    USER_CHANGE_PROFILE_IMG_FAILURE,
    USER_FILEUPLOAD,
    USER_FILEUPLOAD_SUCCESS,
    USER_FILEUPLOAD_FAILURE,
    USER_CHANGE_INFO,
    USER_CHANGE_INFO_SUCCESS,
    USER_CHANGE_INFO_FAILURE } from '../actions/actionTypes';

import update from 'react-addons-update';

const initialState = {
    status: 'INIT', 
    profileImgChange: {
        status: 'INIT', 
    },
    fileupload: {
        status: 'INIT', 
        data: null
    },
    updateInfo: {
        status: 'INIT', 
    }
};

const reducersMap = {
    
    AUTH_SIGNOUT: (state, data) => {
        return initialState;
    },
    USER_CHANGE_PROFILE_IMG: (state, data) => {
        return update(state, {
            profileImgChange: {
                status: {$set: 'WAITING'}
            }
        });
    },
    USER_CHANGE_PROFILE_IMG_SUCCESS: (state, data) => {
        return update(state, {
            profileImgChange: {
                status: {$set: 'SUCCESS'}
            }
        });
    },
    USER_CHANGE_PROFILE_IMG_FAILURE: (state, data) => {
        return update(state, {
            profileImgChange: {
                status: {$set: 'FAILURE'}
            }
        });
    },
    USER_FILEUPLOAD: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'WAITING'},
                data: {$set: null}
            }
        });
    },
    USER_FILEUPLOAD_SUCCESS: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            }
        });
    },
    USER_FILEUPLOAD_FAILURE: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'FAILURE'},
                data: {$set: null}
            }
        });
    },
    USER_CHANGE_INFO: (state, data) => {
        return update(state, {
            updateInfo: {
                status: {$set: 'WAITING'}
            }
        });
    },
    USER_CHANGE_INFO_SUCCESS: (state, data) => {
        return update(state, {
            updateInfo: {
                status: {$set: 'SUCCESS'}
            }
        });
    },
    USER_CHANGE_INFO_FAILURE: (state, data) => {
        return update(state, {
            updateInfo: {
                status: {$set: 'FAILURE'}
            }
        });
    }
}

export default function user(state = initialState, action) {

    if (reducersMap.hasOwnProperty(action.type)) {
        
        return reducersMap[action.type](state, action.data);
    }
    return state;
}
