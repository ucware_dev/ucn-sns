import {
    AUTH_SIGNOUT,
    TEAM_CREATE,
    TEAM_CREATE_SUCCESS,
    TEAM_CREATE_FAILURE,
    TEAM_LIST,
    TEAM_LIST_SUCCESS,
    TEAM_LIST_FAILURE,
    TEAM_LIST_LIMIT,
    TEAM_JOIN_REQ,
    TEAM_JOIN_REQ_SUCCESS,
    TEAM_JOIN_REQ_FAILURE,
    TEAM_FIND_TEAMS,
    TEAM_FIND_TEAMS_SUCCESS,
    TEAM_FIND_TEAMS_FAILURE,
    TEAM_ADMISSION_USER,
    TEAM_ADMISSION_USER_SUCCESS,
    TEAM_ADMISSION_USER_FAILURE,
    TEAM_SEARCH,
    TEAM_SEARCH_SUCCESS,
    TEAM_SEARCH_FAILURE,
    TEAM_SELECT_INFO,
    TEAM_SELECT_INFO_SUCCESS,
    TEAM_SELECT_INFO_FAILURE,
    TEAM_FILE_LIST,
    TEAM_FILE_LIST_SUCCESS,
    TEAM_FILE_LIST_FAILURE,
    TEAM_FILEUPLOAD,
    TEAM_FILEUPLOAD_SUCCESS,
    TEAM_FILEUPLOAD_FAILURE,
    TEAM_CHANGE_PROFILE,
    TEAM_CHANGE_PROFILE_SUCCESS,
    TEAM_CHANGE_PROFILE_FAILURE } from '../actions/actionTypes';

import update from 'react-addons-update';

const initialState = {
    status: 'INIT',
    list : {
        status: 'INIT',
        data: [],
        isLast: true
    },
    joinRequest: {
        status: 'INIT',
    },
    findteams: {
        status: 'INIT',
        data: []
    },
    admission: {
        status: 'INIT',
    },
    search: {
        status: 'INIT',
    },
    selectTeam: {
        status: 'INIT',
        team: null
    },
    filelist: {
        status: 'INIT',
        data: null
    },
    fileupload: {
        status: 'INIT', 
        data: null
    },
    changeProfile: {
        status: 'INIT', 
    },
};

const reducersMap = {
    AUTH_SIGNOUT: (state, data) => {
        return initialState;
    },
    TEAM_CREATE: (state, data) => {
        return update(state, {
            status: {$set: 'WAITING'}
        });
    },
    TEAM_CREATE_SUCCESS: (state, data) => {
        return update(state, {
            status: {$set: 'SUCCESS'},
        });
    },
    TEAM_CREATE_FAILURE: (state, data) => {
        return update(state, {
            status: {$set: 'FAILURE'}
        });
    },
    TEAM_LIST: (state, data) => {
        return update(state, {
            list: {
                status: {$set: 'SUCCESS'},
            }
        });
    },
    TEAM_LIST_SUCCESS: (state, data) => {  
        if (!data.teamlist) {
            data.teamlist = [];
        }

        if (data.isFirst) {
            return update(state, {
                list: {
                    status: {$set: 'SUCCESS'},
                    data: {$set: data.teamlist},
                    isLast: {$set: data.teamlist.length < TEAM_LIST_LIMIT}
                }
            });
        } else {
            return update(state, {
                list: {
                    status: {$set: 'SUCCESS'},
                    data: {$push: data.teamlist},
                    isLast: {$set: data.teamlist.length < TEAM_LIST_LIMIT}
                }
            });
        }
    },
    TEAM_LIST_FAILURE: (state, data) => {   
        return update(state, {
            list: {
                status: {$set: 'FAILURE'},
            }
        });
    },
    TEAM_JOIN_REQ: (state, data) => {
        return update(state, {
            joinRequest: {
                status: { $set: 'WAITING' },
            }
        });
    },
    TEAM_JOIN_REQ_SUCCESS: (state, data) => {
        return update(state, {
            joinRequest: {
                status: { $set: 'SUCCESS' },
            }
        });
    },
    TEAM_JOIN_REQ_FAILURE: (state, data) => {
        return update(state, {
            joinRequest: {
                status: { $set: 'FAILURE' },
            }
        });
    },
    TEAM_FIND_TEAMS: (state, data) => {
        return update(state, {
            findteams: {
                status: { $set: 'WAITING' },
            }
        });
    },
    TEAM_FIND_TEAMS_SUCCESS: (state, data) => {
        return update(state, {
            findteams: {
                status: { $set: 'SUCCESS' },
                data: {$set: data},
            }
        });
    },
    TEAM_FIND_TEAMS_FAILURE: (state, data) => {
        return update(state, {
            findteams: {
                status: { $set: 'FAILURE' },
            }
        });
    },
    TEAM_ADMISSION_USER: (state, data) => {
        return update(state, {
            admission: {
                status: { $set: 'WAITING' },
            }
        });
    },
    TEAM_ADMISSION_USER_SUCCESS: (state, data) => {
        return update(state, {
            admission: {
                status: { $set: 'SUCCESS' },
            }
        });
    },
    TEAM_ADMISSION_USER_FAILURE: (state, data) => {
        return update(state, {
            admission: {
                status: { $set: 'FAILURE' },
            }
        });
    },
    TEAM_SEARCH: (state, data) => {
        return update(state, {
            search: {
                status: { $set: 'WAITING' },
            }
        });
    },
    TEAM_SEARCH_SUCCESS: (state, data) => {
        return update(state, {
            list : {
                data: {$set: action.data}
            },
            search: {
                status: { $set: 'SUCCESS' }
            }
        });
    },
    TEAM_SEARCH_FAILURE: (state, data) => {
        return update(state, {
            search: {
                status: { $set: 'FAILURE' },
            }
        });
    },
    TEAM_SELECT_INFO: (state, data) => {
        return update(state, {
            selectTeam: {
                status: { $set: 'WAITING' },
            }
        });
    },
    TEAM_SELECT_INFO_SUCCESS: (state, data) => {
        return update(state, {
            selectTeam: {
                status: { $set: 'SUCCESS' },
                team: {$set: data},
            }
        });
    },
    TEAM_SELECT_INFO_FAILURE: (state, data) => {
        return update(state, {
            selectTeam: {
                status: { $set: 'FAILURE' },
            }
        });
    },
    TEAM_FILE_LIST: (state, data) => {
        return update(state, {
            filelist: {
                status: {$set: 'WAITING'},
                data: {$set: null}
            }
        });
    },
    TEAM_FILE_LIST_SUCCESS: (state, data) => {
        return update(state, {
            filelist: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            }
        });
    },
    TEAM_FILE_LIST_FAILURE: (state, data) => {
        return update(state, {
            filelist: {
                status: {$set: 'FAILURE'},
                data: {$set: null}
            }
        });
    },
    TEAM_FILEUPLOAD: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'WAITING'},
                data: {$set: null}
            }
        });
    },
    TEAM_FILEUPLOAD_SUCCESS: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'SUCCESS'},
                data: {$set: data}
            }
        });
    },
    TEAM_FILEUPLOAD_FAILURE: (state, data) => {
        return update(state, {
            fileupload: {
                status: {$set: 'FAILURE'},
                data: {$set: null}
            }
        });
    },
    TEAM_CHANGE_PROFILE: (state, data) => {
        return update(state, {
            changeProfile: {
                status: {$set: 'WAITING'}
            }
        });
    },
    TEAM_CHANGE_PROFILE_SUCCESS: (state, data) => {
        return update(state, {
            changeProfile: {
                status: {$set: 'SUCCESS'}
            }
        });
    },
    TEAM_CHANGE_PROFILE_FAILURE: (state, data) => {
        return update(state, {
            changeProfile: {
                status: {$set: 'FAILURE'}
            }
        });
    },
}

export default function team(state = initialState, action) {

    if (reducersMap.hasOwnProperty(action.type)) {
        
        return reducersMap[action.type](state, action.data);
    }
    return state;
}