import {
        AUTH_SIGNUP,
        AUTH_SIGNUP_SUCCESS,
        AUTH_SIGNUP_FAILURE ,
        AUTH_SIGNIN,
        AUTH_SIGNIN_SUCCESS,
        AUTH_SIGNIN_FAILURE,
        AUTH_SIGNOUT,
        AUTH_SELECT_TEAM,
        AUTH_SELECT_TEAM_SUCCESS,
        AUTH_UNSELECT_TEAM,
        AUTH_UNSELECT_TEAM_SUCCESS,
        AUTH_TEAM_ADD,
        AUTH_TEAM_ADD_SUCCESS,
        AUTH_TEAM_ADD_FAILURE,
        AUTH_SEARCH_BAR,
        USER_INFO,
        USER_INFO_SUCCESS,
        USER_INFO_FAILURE } from '../actions/actionTypes';

import update from 'react-addons-update';

const initialState = {
        status: 'INIT',
        isSignin: false,
        data: null,
        currentTeam: null,
        searchbar: null,
};

const reducersMap = {

    AUTH_SIGNOUT : (state, data) => {
        return initialState;
    },


    AUTH_SIGNUP : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    AUTH_SIGNUP_SUCCESS : (state, data) => {
        return update(state, {
            status: { $set: 'SUCCESS' }
        });
    },
    AUTH_SIGNUP_FAILURE : (state, data) => {
        return update(state, {
            status: { $set: 'FAILURE' }
        });
    },

    AUTH_SIGNIN : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    AUTH_SIGNIN_SUCCESS : (state, data) => {
        return update(state, {
            status: { $set: 'SUCCESS' },
            isSignin: { $set: true },
            data: { $set: data }
        });
    },
    AUTH_SIGNIN_FAILURE : (state, data) => {
        return update(state, {
            status: { $set: 'FAILURE' }
        });
    },

    AUTH_SELECT_TEAM : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    AUTH_SELECT_TEAM_SUCCESS : (state, data) => {
        return update(state, {
            status: { $set: 'SUCCESS' },
            currentTeam: { $set: data }
        });
    },

    AUTH_UNSELECT_TEAM : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    AUTH_UNSELECT_TEAM_SUCCESS : (state, data) => {
        return update(state, {
            status: { $set: 'SUCCESS' },
            currentTeam: { $set: null }
        });
    },
    
    AUTH_TEAM_ADD : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    AUTH_TEAM_ADD_SUCCESS : (state, data) => {

        let userData = state.data;
        userData.user.teams.push(data);

        return update(state, {
            status: { $set: 'SUCCESS' },
            data: {$set: userData}
        });
    },
    AUTH_TEAM_ADD_FAILURE : (state, data) => {
        return update(state, {
            status: { $set: 'FAILURE' }
        });
    },

    USER_INFO : (state, data) => {
        return update(state, {
            status: { $set: 'WAITING' }
        });
    },
    USER_INFO_SUCCESS : (state, data) => {
        
        let userData = state.data;
        userData.user = data;

        return update(state, {
            status: { $set: 'SUCCESS' },
            data: { $set: userData }
        });
    },
    USER_INFO_FAILURE : (state, data) => {
        return update(state, {
            status: { $set: 'FAILURE' }
        });
    },

    AUTH_SEARCH_BAR : (state, data) => {
        return update(state, {
            searchbar: { $set: data }
        });
    },
};

export default function auth(state = initialState, action) {

    if (reducersMap.hasOwnProperty(action.type)) {
        return reducersMap[action.type](state, action.data);
    }
    return state;
}