import auth from './auth';
import article from './article';
import comment from './comment';
import team from './team';
import user from './user';

import { combineReducers } from 'redux';

export default combineReducers({
    auth,
    article,
    comment,
    team,
    user
})
