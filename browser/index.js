'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {persistStore, autoRehydrate} from 'redux-persist';
import { asyncSessionStorage, syncSessionStorage } from 'redux-persist/storages'
import axios from 'axios';
import { Home ,
    Signin, 
    Signup, MyPage, 
    TeamList, 
    TeamJoinRequest,
    CreateTeam, 
    ArticleMain, 
    Article, 
    MemberList, 
    TeamSetting, 
    ProfileSetting,
    TeamFiles,
    NotFound } from './pages';

import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers'
import { Provider} from 'react-redux';
import thunk from 'redux-thunk';
import { signinout } from './actions/auth';
import { articleListClear } from './actions/article';
import { commentListClear } from './actions/comment';
var _ = require('underscore');

import loc from './lang/ucLocale';

//IE Promise Support
import Promise from 'promise-polyfill';
if (!window.Promise) {
  window.Promise = Promise;
}

window.uc = {};

window.uc.JLog = function(c, o) {
    console.log(c + ' : ' + JSON.stringify(o));
};
window.uc.Log = function(c, o) {
    console.log(c + ' : ' + o);
};

window.uc.loc = loc;
window.uc._ = _;


window.uc.isMobile = function() {
    if (navigator.userAgent.match(/Android/i)) return true;
    if (navigator.userAgent.match(/BlackBerry/i)) return true;
    if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) return true;
    if (navigator.userAgent.match(/Opera Mini/i)) return true;
    if (navigator.userAgent.match((/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i))) return true;
    return false;

};

const store = createStore(reducers, applyMiddleware(thunk), autoRehydrate());
let storage = {storage: asyncSessionStorage};
persistStore(store, storage, ()=> {
    //console.log('### RESOTRE : ' + store.getState().auth.isSignin);

    const rootElement = document.getElementById('root');    
    ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Home} >
                <IndexRoute component={Signin} />
                    <Route path="signup" component={Signup} />
                    <Route path="signin" component={Signin} />
                    <Route path="mypage" component={MyPage} />
                    <Route path="teams" component={TeamList} />                    
                    <Route path="joinrequest" component={TeamJoinRequest} />
                    <Route path="createteam" component={CreateTeam} />
                    <Route path="signin" component={TeamList} />
                    <Route path="profile" component={ProfileSetting} />
                    <Route path="article" component={ArticleMain} >
                        <IndexRoute component={Article} />
                        <Route path="memberlist" component={MemberList} />
                        <Route path="setting" component={TeamSetting} />
                        <Route path="teamfiles" component={TeamFiles} />
                        <Route path=":articleid" component={Article} />
                    </Route>
            </Route>
            <Route path='*' component={NotFound} />
        </Router>
    </Provider>, rootElement);

});

function connectSocket() {

    let info = {};
    let state = store.getState();
    if (state.auth.data) {
        info.team = 'global';
        if (state.auth.data.user.team) {
            info.team = state.auth.data.user.team;
        }
        info.email = state.auth.data.user.email,
        info.username = state.auth.data.user.username

        global.socket.emit('updateClientInfo', info);
    }
}

function checkSessionInfo() {
    const SESSION_INFO_API = '/api/v1/account/session';

    let clear = ()=> {

        store.dispatch(articleListClear());
        store.dispatch(commentListClear());

        setTimeout(() => {
            store.dispatch(signinout());
        }, 500);
    };

    axios.post(SESSION_INFO_API).then((json) => {
        if (json.data.session === null) {
            clear();
        }
    }).catch((error) => {
        clear();
    });
}
