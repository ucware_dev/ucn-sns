import ko from './ko';
import en from './en';
import config from '../../server.config';

const ucLocale = {
    default: config.lang
};

ucLocale.setLocale = function(lo) {
    this.default = lo;
}

ucLocale.i = function(w) {

    let locale = en;
    if (this.default === 'ko') {
        locale = ko;
    }
    return locale[w];
}

export default ucLocale