const ko = {
    
    brand: 'UMCS Social Media',
    
    header_join: '회원가입',
    header_signin: '로그인',
    header_signout: '로그아웃',
    header_search_group: '팀, 동호회를 검색하세요.',
    header_search_article: '게시글을 검색해 보세요.',

    auth_join_title: '회원가입',
    auth_join_email: '이메일',
    auth_join_name: '이름',
    auth_join_passwd: '비밀번호',
    auth_join_passwd_confirm: '비밀번호 확인',
    auth_join_passwd_guide: '숫자, 특수문자 포함 4자 이상 입력하세요',
    auth_join_btn: '회원가입',

    auth_login_title: '로그인',
    auth_login_email: '이메일',
    auth_login_passwd: '비밀번호',
    auth_login_btn: '로그인',

    lobby_my_group: '나의 팀',
    lobby_group_list: '전체 팀 목록',
    lobby_create_group: '팀 만들기',
    lobby_join_request: '가입 요청',

    group_list: '전체 팀',
    group_my_list: '나의 팀 목록',
    group_join: '가입하기',

    admission_title: '가입 요청',
    admission_confirm: '승인',

    group_detail_members: '멤버',
    group_detail_leader: '리더',

    create_group_title: '팀, 동호회 만들기',
    create_group_pl_name: '이름을 입력하세요',
    create_group_name: '이름',
    create_group_pl_desc: '소개글을 입력하세요',
    create_group_cancel: '취소',
    create_group_confirm: '확인',

    article_write_title: '글쓰기',
    article_write_edit: '수정',
    article_write_placeholder: '멤버들에게 소식을 전하세요.',
    article_write_post: '게시',

    article_face: '표정',
    article_face_write: '표정짓기',
    article_comment: '댓글',
    article_comment_write: '댓글쓰기',
    article_more: '...더보기',

    article_edit: '수정',
    article_delete: '삭제',

    article_time_now: '지금막',
    article_time_am: '오전',
    article_time_pm: '오후',
    article_min_ago: '분 전',
    article_mins_ago: '분 전',
    article_hour_ago: '시간 전',
    article_hours_ago: '시간 전',
    article_yesterday: '어제',

    article_time_year: '년',
    article_time_month: '월',
    article_time_day: '일',
    article_time_hr: '시',
    article_time_min: '분',

    comment_write_send: '보내기',
    comment_write_placeholder: '댓글을 남겨주세요',
    comment_last: '마지막 댓글 입니다',
    comment_more: '더보기',

    comment_edit_delete: '삭제',
    comment_edit_cancel: '취소',
    comment_edit_ok: '확인',
}

export default ko