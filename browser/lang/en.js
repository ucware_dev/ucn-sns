var en = {
    
    brand: 'UMCS Social Media',
    
    header_join: 'Join',
    header_signin: 'Sign in',
    header_signout: 'Sign out',
    header_search_group: 'Search',
    header_search_article: 'Search',

    auth_join_title: 'Join',
    auth_join_email: 'E-mail',
    auth_join_name: 'Username',
    auth_join_passwd: 'Password',
    auth_join_passwd_confirm: 'Confirm Password',
    auth_join_passwd_guide: 'Please enter at least 4 characters including numbers and special characters.',
    auth_join_btn: 'Join',

    auth_login_title: 'Sign in',
    auth_login_email: 'E-mail',
    auth_login_passwd: 'Password',
    auth_login_btn: 'Sign in',

    lobby_my_group: 'My Group',
    lobby_group_list: 'Discover Group...',
    lobby_create_group: 'Create group',
    lobby_join_request: 'Request to join',

    group_list: 'Discover Group',
    group_my_list: 'My Group',
    group_join: 'Join',

    admission_title: 'Request to join',
    admission_confirm: 'Confirm',

    group_detail_members: 'Members',
    group_detail_leader: 'Leader',

    create_group_title: 'Create New Group',
    create_group_pl_name: 'Name your group',
    create_group_name: 'Name',
    create_group_pl_desc: 'Please enter an introduction',
    create_group_cancel: 'Cancel',
    create_group_confirm: 'Create',

    article_write_title: 'Write Post',
    article_write_edit: 'Edit',
    article_write_placeholder: 'Write something…',
    article_write_post: 'Post',

    article_face: 'Like',
    article_face_write: 'Like',
    article_comment: 'Comment',
    article_comment_write: 'Comment',
    article_more: '...More',

    article_edit: 'Edit',
    article_delete: 'Delete',

    article_time_now: 'Just now',
    article_time_am: 'AM',
    article_time_pm: 'PM',
    article_min_ago: 'min ago',
    article_mins_ago: 'mins ago',
    article_hour_ago: 'hr ago',
    article_hours_ago: 'hrs ago',
    article_yesterday: 'yesterday',

    article_time_year: ' -',
    article_time_month: ' -',
    article_time_day: ' ',
    article_time_hr: ' :',
    article_time_min: '',

    comment_write_send: 'Post',
    comment_write_placeholder: 'Write a comment.',
    comment_last: 'Here is last ccomment',
    comment_more: 'More',

    comment_edit_delete: 'Delete',
    comment_edit_cancel: 'Cancel',
    comment_edit_ok: 'Confirm',
}

export default en